# INTEL CELERON
| PRODUCT NAME                                                           | STATUS          | RELEASE | COR | BASE FREQ  | CACHE            | TDP               |
| ---------------------------------------------------------------------- | --------------- | ------- | --- | ---------- | ---------------- | ----------------- |
| Intel(R) Celeron(R) Processor 4305U                                        | Launched        | Q2 '19  | 2   | 2.20 GHz   | 2 MB SmartCache  | 15 W              |
# INTEL CORE
| PRODUCT NAME                                                           | STATUS          | RELEASE | COR | TURBO MAX  | BASE FREQ  | CACHE                   |
| ---------------------------------------------------------------------- | --------------- | ------- | --- | ---------- | ---------- | ----------------------- |
| Intel(R) Core[TM] i5-1035G4 Processor                                       | Launched        | Q3'19   | 4   | 3.70 GHz   | 1.10 GHz   | 6 MB SmartCache         |
| Intel(R) Core[TM] i5-1035G7 Processor                                       | Launched        | Q3'19   | 4   | 3.70 GHz   | 1.20 GHz   | 6 MB SmartCache         |
| Intel(R) Core[TM] i7-1065G7 Processor                                       | Launched        | Q3'19   | 4   | 3.90 GHz   | 1.30 GHz   | 8 MB SmartCache         |
| Intel(R) Core[TM] i5-1035G1 Processor                                       | Launched        | Q3'19   | 4   | 3.60 GHz   | 1.00 GHz   | 6 MB SmartCache         |
| Intel(R) Core[TM] i3-1005G1 Processor                                       | Launched        | Q3'19   | 2   | 3.40 GHz   | 1.20 GHz   | 4 MB SmartCache         |
| Intel(R) Core[TM] i5-1030G7 Processor                                       | Announced       | Q3'19   | 4   | 3.50 GHz   | 800 MHz    | 6 MB SmartCache         |
| Intel(R) Core[TM] i7-1060G7 Processor                                       | Announced       | Q3'19   | 4   | 3.80 GHz   | 1.00 GHz   | 8 MB SmartCache         |
| Intel(R) Core[TM] i5-1030G4 Processor                                       | Announced       | Q3'19   | 4   | 3.50 GHz   | 700 MHz    | 6 MB SmartCache         |
| Intel(R) Core[TM] i3-1000G1 Processor                                       | Announced       | Q3'19   | 2   | 3.20 GHz   | 1.10 GHz   | 4 MB SmartCache         |
| Intel(R) Core[TM] i3-1000G4 Processor                                       | Announced       | Q3'19   | 2   | 3.20 GHz   | 1.10 GHz   | 4 MB SmartCache         |
| Intel(R) Core[TM] i5-8257U Processor                                        | Launched        | Q3'19   | 4   | 3.90 GHz   | 1.40 GHz   | 6 MB SmartCache         |
| Intel(R) Core[TM] i7-8557U Processor                                        | Launched        | Q3'19   | 4   | 4.50 GHz   | 1.70 GHz   | 8 MB SmartCache         |
| Intel(R) Core[TM] i3-9100HL Processor                                       | Launched        | Q2'19   | 4   | 2.90 GHz   | 1.60 GHz   | 6 MB                    |
| Intel(R) Core[TM] i7-9850HE Processor                                       | Launched        | Q2'19   | 6   | 4.40 GHz   | 2.70 GHz   | 9 MB                    |
| Intel(R) Core[TM] i7-9850HL Processor                                       | Launched        | Q2'19   | 6   | 4.10 GHz   | 1.90 GHz   | 9 MB                    |
| Intel(R) Core[TM] i5-9500TE Processor                                       | Launched        | Q2'19   | 6   | 3.60 GHz   | 2.20 GHz   | 9 MB                    |
| Intel(R) Core[TM] i7-9700TE Processor                                       | Launched        | Q2'19   | 8   | 3.80 GHz   | 1.80 GHz   | 12 MB                   |
| Intel(R) Core[TM] i3-9100E Processor                                        | Launched        | Q2'19   | 4   | 3.70 GHz   | 3.10 GHz   | 6 MB                    |
| Intel(R) Core[TM] i3-9100TE Processor                                       | Launched        | Q2'19   | 4   | 3.20 GHz   | 2.20 GHz   | 6 MB                    |
| Intel(R) Core[TM] i7-9700E Processor                                        | Launched        | Q2'19   | 8   | 4.40 GHz   | 2.60 GHz   | 12 MB                   |
| Intel(R) Core[TM] i5-9500E Processor                                        | Launched        | Q2'19   | 6   | 4.20 GHz   | 3.00 GHz   | 9 MB                    |
| Intel(R) Core[TM] i5-8279U Processor                                        | Launched        | Q2'19   | 4   | 4.10 GHz   | 2.40 GHz   | 6 MB SmartCache         |
| Intel(R) Core[TM] i7-8569U Processor                                        | Launched        | Q2'19   | 4   | 4.70 GHz   | 2.80 GHz   | 8 MB SmartCache         |
| Intel(R) Core[TM] i3-9300T Processor                                        | Launched        | Q2'19   | 4   | 3.80 GHz   | 3.20 GHz   | 8 MB SmartCache         |
| Intel(R) Core[TM] i5-9500 Processor                                         | Launched        | Q2'19   | 6   | 4.40 GHz   | 3.00 GHz   | 9 MB SmartCache         |
| Intel(R) Core[TM] i3-9300 Processor                                         | Launched        | Q2'19   | 4   | 4.30 GHz   | 3.70 GHz   | 8 MB SmartCache         |
| Intel(R) Core[TM] i5-9600 Processor                                         | Launched        | Q2'19   | 6   | 4.60 GHz   | 3.10 GHz   | 9 MB SmartCache         |
| Intel(R) Core[TM] i3-9100 Processor                                         | Launched        | Q2'19   | 4   | 4.20 GHz   | 3.60 GHz   | 6 MB SmartCache         |
| Intel(R) Core[TM] i3-9100T Processor                                        | Launched        | Q2'19   | 4   | 3.70 GHz   | 3.10 GHz   | 6 MB SmartCache         |
| Intel(R) Core[TM] i5-9400T Processor                                        | Launched        | Q2'19   | 6   | 3.40 GHz   | 1.80 GHz   | 9 MB SmartCache         |
| Intel(R) Core[TM] i3-9350K Processor                                        | Launched        | Q2'19   | 4   | 4.60 GHz   | 4.00 GHz   | 8 MB SmartCache         |
| Intel(R) Core[TM] i3-9100F Processor                                        | Launched        | Q2'19   | 4   | 4.20 GHz   | 3.60 GHz   | 6 MB SmartCache         |
| Intel(R) Core[TM] i5-9500F Processor                                        | Launched        | Q2'19   | 6   | 4.40 GHz   | 3.00 GHz   | 9 MB SmartCache         |
| Intel(R) Core[TM] i5-9600T Processor                                        | Launched        | Q2'19   | 6   | 3.90 GHz   | 2.30 GHz   | 9 MB SmartCache         |
| Intel(R) Core[TM] i5-9500T Processor                                        | Launched        | Q2'19   | 6   | 3.70 GHz   | 2.20 GHz   | 9 MB SmartCache         |
| Intel(R) Core[TM] i3-9320 Processor                                         | Launched        | Q2'19   | 4   | 4.40 GHz   | 3.70 GHz   | 8 MB SmartCache         |
| Intel(R) Core[TM] i9-9900T Processor                                        | Launched        | Q2'19   | 8   | 4.40 GHz   | 2.10 GHz   | 16 MB SmartCache        |
| Intel(R) Core[TM] i7-9700T Processor                                        | Launched        | Q2'19   | 8   | 4.30 GHz   | 2.00 GHz   | 12 MB SmartCache        |
| Intel(R) Core[TM] i9-9900 Processor                                         | Launched        | Q2'19   | 8   | 5.00 GHz   | 3.10 GHz   | 16 MB SmartCache        |
| Intel(R) Core[TM] i7-9700 Processor                                         | Launched        | Q2'19   | 8   | 4.70 GHz   | 3.00 GHz   | 12 MB SmartCache        |
| Intel(R) Core[TM] i7-9700F Processor                                        | Launched        | Q2'19   | 8   | 4.70 GHz   | 3.00 GHz   | 12 MB SmartCache        |
| Intel(R) Core[TM] i7-9750H Processor                                        | Launched        | Q2'19   | 6   | 4.50 GHz   | 2.60 GHz   | 12 MB SmartCache        |
| Intel(R) Core[TM] i7-9750HF Processor                                       | Launched        | Q2'19   | 6   | 4.50 GHz   | 2.60 GHz   | 12 MB SmartCache        |
| Intel(R) Core[TM] i7-9850H Processor                                        | Launched        | Q2'19   | 6   | 4.60 GHz   | 2.60 GHz   | 12 MB                   |
| Intel(R) Core[TM] i9-9880H Processor                                        | Launched        | Q2'19   | 8   | 4.80 GHz   | 2.30 GHz   | 16 MB SmartCache        |
| Intel(R) Core[TM] i9-9980HK Processor                                       | Launched        | Q2'19   | 8   | 5.00 GHz   | 2.40 GHz   | 16 MB SmartCache        |
| Intel(R) Core[TM] i5-9300H Processor                                        | Launched        | Q2'19   | 4   | 4.10 GHz   | 2.40 GHz   | 8 MB SmartCache         |
| Intel(R) Core[TM] i5-9400H Processor                                        | Launched        | Q2'19   | 4   | 4.30 GHz   | 2.50 GHz   | 8 MB SmartCache         |
| Intel(R) Core[TM] i5-8365U Processor                                        | Launched        | Q2 '19  | 4   | 4.10 GHz   | 1.60 GHz   | 6 MB SmartCache         |
| Intel(R) Core[TM] i7-8665U Processor                                        | Launched        | Q2 '19  | 4   | 4.80 GHz   | 1.90 GHz   | 8 MB SmartCache         |
| Intel(R) Core[TM] i5-9400 Processor                                         | Launched        | Q1'19   | 6   | 4.10 GHz   | 2.90 GHz   | 9 MB SmartCache         |
| Intel(R) Core[TM] i5-9400F Processor                                        | Launched        | Q1'19   | 6   | 4.10 GHz   | 2.90 GHz   | 9 MB SmartCache         |
| Intel(R) Core[TM] i5-9600KF Processor                                       | Launched        | Q1'19   | 6   | 4.60 GHz   | 3.70 GHz   | 9 MB SmartCache         |
| Intel(R) Core[TM] i7-9700KF Processor                                       | Launched        | Q1'19   | 8   | 4.90 GHz   | 3.60 GHz   | 12 MB SmartCache        |
| Intel(R) Core[TM] i9-9900KF Processor                                       | Launched        | Q1'19   | 8   | 5.00 GHz   | 3.60 GHz   | 16 MB SmartCache        |
| Intel(R) Core[TM] i3-9350KF Processor                                       | Launched        | Q1'19   | 4   | 4.60 GHz   | 4.00 GHz   | 8 MB SmartCache         |
| Intel(R) Core[TM] i5-9600K Processor                                        | Launched        | Q4'18   | 6   | 4.60 GHz   | 3.70 GHz   | 9 MB SmartCache         |
| Intel(R) Core[TM] i9-9820X X-series Processor                               | Launched        | Q4'18   | 10  | 4.10 GHz   | 3.30 GHz   | 16.5 MB SmartCache      |
| Intel(R) Core[TM] i7-9700K Processor                                        | Launched        | Q4'18   | 8   | 4.90 GHz   | 3.60 GHz   | 12 MB SmartCache        |
| Intel(R) Core[TM] i7-9800X X-series Processor                               | Launched        | Q4'18   | 8   | 4.40 GHz   | 3.80 GHz   | 16.5 MB SmartCache      |
| Intel(R) Core[TM] i9-9900X X-series Processor                               | Launched        | Q4'18   | 10  | 4.40 GHz   | 3.50 GHz   | 19.25 MB SmartCache     |
| Intel(R) Core[TM] i9-9920X X-series Processor                               | Launched        | Q4'18   | 12  | 4.40 GHz   | 3.50 GHz   | 19.25 MB SmartCache     |
| Intel(R) Core[TM] i9-9900K Processor                                        | Launched        | Q4'18   | 8   | 5.00 GHz   | 3.60 GHz   | 16 MB SmartCache        |
| Intel(R) Core[TM] i9-9960X X-series Processor                               | Launched        | Q4'18   | 16  | 4.40 GHz   | 3.10 GHz   | 22 MB SmartCache        |
| Intel(R) Core[TM] i9-9980XE Extreme Edition Processor                       | Launched        | Q4'18   | 18  | 4.40 GHz   | 3.00 GHz   | 24.75 MB SmartCache     |
| Intel(R) Core[TM] i9-9940X X-series Processor                               | Launched        | Q4'18   | 14  | 4.40 GHz   | 3.30 GHz   | 19.25 MB SmartCache     |
| Intel(R) Core[TM] i3-8100B Processor                                        | Launched        | Q3 '18  | 4   |            | 3.60 GHz   | 6 MB SmartCache         |
| Intel(R) Core[TM] i7-8565U Processor                                        | Launched        | Q3'18   | 4   | 4.60 GHz   | 1.80 GHz   | 8 MB SmartCache         |
| Intel(R) Core[TM] i5-8265U Processor                                        | Launched        | Q3'18   | 4   | 3.90 GHz   | 1.60 GHz   | 6 MB SmartCache         |
| Intel(R) Core[TM] i3-8145U Processor                                        | Launched        | Q3'18   | 2   | 3.90 GHz   | 2.10 GHz   | 4 MB SmartCache         |
| Intel(R) Core[TM] i5-8200Y Processor                                        | Launched        | Q3'18   | 2   | 3.90 GHz   | 1.30 GHz   | 4 MB SmartCache         |
| Intel(R) Core[TM] m3-8100Y Processor                                        | Launched        | Q3'18   | 2   | 3.40 GHz   | 1.10 GHz   | 4 MB SmartCache         |
| Intel(R) Core[TM] i7-8706G Processor with Radeon[TM] Pro WX Vega M GL graphics  | Launched        | Q3'18   | 4   | 4.10 GHz   | 3.10 GHz   | 8 MB                    |
| Intel(R) Core[TM] i5-8305G Processor with Radeon[TM] Pro WX Vega M GL graphics  | Launched        | Q3'18   | 4   | 3.80 GHz   | 2.80 GHz   | 6 MB                    |
| Intel(R) Core[TM] i3-8100H Processor                                        | Launched        | Q3'18   | 4   |            | 3.00 GHz   | 6 MB SmartCache         |
| Intel(R) Core[TM] i7-8086K Processor                                        | Launched        | Q2'18   | 6   | 5.00 GHz   | 4.00 GHz   | 12 MB SmartCache        |
| Intel(R) Core[TM] i3-8300 Processor                                         | Launched        | Q2'18   | 4   |            | 3.70 GHz   | 8 MB SmartCache         |
| Intel(R) Core[TM] i7-8700T Processor                                        | Launched        | Q2'18   | 6   | 4.00 GHz   | 2.40 GHz   | 12 MB SmartCache        |
| Intel(R) Core[TM] i5-8600T Processor                                        | Launched        | Q2'18   | 6   | 3.70 GHz   | 2.30 GHz   | 9 MB SmartCache         |
| Intel(R) Core[TM] i5-8400H Processor                                        | Launched        | Q2'18   | 4   | 4.20 GHz   | 2.50 GHz   | 8 MB SmartCache         |
| Intel(R) Core[TM] i3-8100T Processor                                        | Launched        | Q2'18   | 4   |            | 3.10 GHz   | 6 MB SmartCache         |
| Intel(R) Core[TM] i9-8950HK Processor                                       | Launched        | Q2'18   | 6   | 4.80 GHz   | 2.90 GHz   | 12 MB SmartCache        |
| Intel(R) Core[TM] i5-8400T Processor                                        | Launched        | Q2'18   | 6   | 3.30 GHz   | 1.70 GHz   | 9 MB SmartCache         |
| Intel(R) Core[TM] i3-8300T Processor                                        | Launched        | Q2'18   | 4   |            | 3.20 GHz   | 8 MB SmartCache         |
| Intel(R) Core[TM] i5-8600 Processor                                         | Launched        | Q2'18   | 6   | 4.30 GHz   | 3.10 GHz   | 9 MB SmartCache         |
| Intel(R) Core[TM] i5-8500 Processor                                         | Launched        | Q2'18   | 6   | 4.10 GHz   | 3.00 GHz   | 9 MB SmartCache         |
| Intel(R) Core[TM] i7-8750H Processor                                        | Launched        | Q2'18   | 6   | 4.10 GHz   | 2.20 GHz   | 9 MB SmartCache         |
| Intel(R) Core[TM] i7-8850H Processor                                        | Launched        | Q2'18   | 6   | 4.30 GHz   | 2.60 GHz   | 9 MB SmartCache         |
| Intel(R) Core[TM] i5-8500T Processor                                        | Launched        | Q2'18   | 6   | 3.50 GHz   | 2.10 GHz   | 9 MB SmartCache         |
| Intel(R) Core[TM] i5-8259U Processor                                        | Launched        | Q2'18   | 4   | 3.80 GHz   | 2.30 GHz   | 6 MB SmartCache         |
| Intel(R) Core[TM] i3-8109U Processor                                        | Launched        | Q2'18   | 2   | 3.60 GHz   | 3.00 GHz   | 4 MB SmartCache         |
| Intel(R) Core[TM] i5-8269U Processor                                        | Launched        | Q2'18   | 4   | 4.20 GHz   | 2.60 GHz   | 6 MB SmartCache         |
| Intel(R) Core[TM] i7-8559U Processor                                        | Launched        | Q2'18   | 4   | 4.50 GHz   | 2.70 GHz   | 8 MB SmartCache         |
| Intel(R) Core[TM] i7-8700B Processor                                        | Launched        | Q2'18   | 6   | 4.60 GHz   | 3.20 GHz   | 12 MB SmartCache        |
| Intel(R) Core[TM] i5-8400B Processor                                        | Launched        | Q2'18   | 6   | 4.00 GHz   | 2.80 GHz   | 9 MB SmartCache         |
| Intel(R) Core[TM] i5-8500B Processor                                        | Launched        | Q2'18   | 6   | 4.10 GHz   | 3.00 GHz   | 9 MB SmartCache         |
| Intel(R) Core[TM] i7+8700 Processory                                        | Launched        | Q2'18   | 6   | 4.60 GHz   | 3.20 GHz   | 12 MB SmartCache        |
| Intel(R) Core[TM] i5+8500 Processor                                         | Launched        | Q2'18   | 6   | 4.10 GHz   | 3.00 GHz   | 9 MB SmartCache         |
| Intel(R) Core[TM] i5+8400 Processor                                         | Launched        | Q2'18   | 6   | 4.00 GHz   | 2.80 GHz   | 9 MB SmartCache         |
| Intel(R) Core[TM] i3-7020U Processor                                        | Launched        | Q2'18   | 2   |            | 2.30 GHz   | 3 MB SmartCache         |
| Intel(R) Core[TM] i3-8130U Processor                                        | Launched        | Q1'18   | 2   | 3.40 GHz   | 2.20 GHz   | 4 MB SmartCache         |
| Intel(R) Core[TM] i7-8709G Processor with Radeon[TM] RX Vega M GH graphics     | Announced       | Q1'18   | 4   | 4.10 GHz   | 3.10 GHz   | 8 MB                    |
| Intel(R) Core[TM] i5-8305G Processor with Radeon[TM] RX Vega M GL graphics     | Announced       | Q1'18   | 4   | 3.80 GHz   | 2.80 GHz   | 6 MB                    |
| Intel(R) Core[TM] i7-8809G Processor with Radeon[TM] RX Vega M GH graphics     | Announced       | Q1'18   | 4   | 4.20 GHz   | 3.10 GHz   | 8 MB                    |
| Intel(R) Core[TM] i7-8705G Processor with Radeon[TM] RX Vega M GL graphics     | Announced       | Q1'18   | 4   | 4.10 GHz   | 3.10 GHz   | 8 MB                    |
| Intel(R) Core[TM] i7-8706G Processor with Radeon[TM] RX Vega M GL graphics     | Launched        | Q1'18   | 4   | 4.10 GHz   | 3.10 GHz   | 8 MB                    |
| Intel(R) Core[TM] i7-8700K Processor                                        | Launched        | Q4'17   | 6   | 4.70 GHz   | 3.70 GHz   | 12 MB SmartCache        |
| Intel(R) Core[TM] i5-8600K Processor                                        | Launched        | Q4'17   | 6   | 4.30 GHz   | 3.60 GHz   | 9 MB SmartCache         |
| Intel(R) Core[TM] i7-8700 Processor                                         | Launched        | Q4'17   | 6   | 4.60 GHz   | 3.20 GHz   | 12 MB SmartCache        |
| Intel(R) Core[TM] i5-8400 Processor                                         | Launched        | Q4'17   | 6   | 4.00 GHz   | 2.80 GHz   | 9 MB SmartCache         |
| Intel(R) Core[TM] i3-8100 Processor                                         | Launched        | Q4'17   | 4   |            | 3.60 GHz   | 6 MB SmartCache         |
| Intel(R) Core[TM] i3-8350K Processor                                        | Launched        | Q4'17   | 4   |            | 4.00 GHz   | 8 MB SmartCache         |
| Intel(R) Core[TM] i9-7960X X-series Processor                               | Launched        | Q3'17   | 16  | 4.20 GHz   | 2.80 GHz   | 22 MB                   |
| Intel(R) Core[TM] i9-7980XE Extreme Edition Processor                       | Launched        | Q3'17   | 18  | 4.20 GHz   | 2.60 GHz   | 24.75 MB                |
| Intel(R) Core[TM] i9-7940X X-series Processor                               | Launched        | Q3'17   | 14  | 4.30 GHz   | 3.10 GHz   | 19.25 MB                |
| Intel(R) Core[TM] i7-8550U Processor                                        | Launched        | Q3'17   | 4   | 4.00 GHz   | 1.80 GHz   | 8 MB SmartCache         |
| Intel(R) Core[TM] i5-8250U Processor                                        | Launched        | Q3'17   | 4   | 3.40 GHz   | 1.60 GHz   | 6 MB SmartCache         |
| Intel(R) Core[TM] i7-8650U Processor                                        | Launched        | Q3'17   | 4   | 4.20 GHz   | 1.90 GHz   | 8 MB SmartCache         |
| Intel(R) Core[TM] i5-8350U Processor                                        | Launched        | Q3'17   | 4   | 3.60 GHz   | 1.70 GHz   | 6 MB SmartCache         |
| Intel(R) Core[TM] i9-7920X X-series Processor                               | Launched        | Q3'17   | 12  | 4.30 GHz   | 2.90 GHz   | 16.5 MB L3              |
| Intel(R) Core[TM] i3-7130U Processor                                        | Launched        | Q2'17   | 2   |            | 2.70 GHz   | 3 MB SmartCache         |
| Intel(R) Core[TM] i7-7740X X-series Processor                               | Discontinued    | Q2'17   | 4   | 4.50 GHz   | 4.30 GHz   | 8 MB                    |
| Intel(R) Core[TM] i5-7640X X-series Processor                               | Discontinued    | Q2'17   | 4   | 4.20 GHz   | 4.00 GHz   | 6 MB                    |
| Intel(R) Core[TM] i9-7900X X-series Processor                               | Launched        | Q2'17   | 10  | 4.30 GHz   | 3.30 GHz   | 13.75 MB L3             |
| Intel(R) Core[TM] i7-7820X X-series Processor                               | Launched        | Q2'17   | 8   | 4.30 GHz   | 3.60 GHz   | 11 MB L3                |
| Intel(R) Core[TM] i7-7800X X-series Processor                               | Launched        | Q2'17   | 6   | 4.00 GHz   | 3.50 GHz   | 8.25 MB L3              |
| Intel(R) Core[TM] m3-7Y32 Processor                                         | Launched        | Q2'17   | 2   | 3.00 GHz   | 1.10 GHz   | 4 MB SmartCache         |
| Intel(R) Core[TM] i3-7101E Processor                                        | Launched        | Q1'17   | 2   |            | 3.90 GHz   | 3 MB SmartCache         |
| Intel(R) Core[TM] i5-7400 Processor                                         | Launched        | Q1'17   | 4   | 3.50 GHz   | 3.00 GHz   | 6 MB SmartCache         |
| Intel(R) Core[TM] i5-7500T Processor                                        | Launched        | Q1'17   | 4   | 3.30 GHz   | 2.70 GHz   | 6 MB SmartCache         |
| Intel(R) Core[TM] i3-7100E Processor                                        | Launched        | Q1'17   | 2   |            | 2.90 GHz   | 3 MB                    |
| Intel(R) Core[TM] i5-7600T Processor                                        | Launched        | Q1'17   | 4   | 3.70 GHz   | 2.80 GHz   | 6 MB SmartCache         |
| Intel(R) Core[TM] i7-7700T Processor                                        | Launched        | Q1'17   | 4   | 3.80 GHz   | 2.90 GHz   | 8 MB SmartCache         |
| Intel(R) Core[TM] i5-7400T Processor                                        | Launched        | Q1'17   | 4   | 3.00 GHz   | 2.40 GHz   | 6 MB SmartCache         |
| Intel(R) Core[TM] i7-7820HK Processor                                       | Launched        | Q1'17   | 4   | 3.90 GHz   | 2.90 GHz   | 8 MB                    |
| Intel(R) Core[TM] i3-7100 Processor                                         | Launched        | Q1'17   | 2   |            | 3.90 GHz   | 3 MB SmartCache         |
| Intel(R) Core[TM] i5-7600 Processor                                         | Launched        | Q1'17   | 4   | 4.10 GHz   | 3.50 GHz   | 6 MB SmartCache         |
| Intel(R) Core[TM] i7-7700HQ Processor                                       | Launched        | Q1'17   | 4   | 3.80 GHz   | 2.80 GHz   | 6 MB                    |
| Intel(R) Core[TM] i5-7500 Processor                                         | Launched        | Q1'17   | 4   | 3.80 GHz   | 3.40 GHz   | 6 MB SmartCache         |
| Intel(R) Core[TM] i7-7600U Processor                                        | Launched        | Q1'17   | 2   | 3.90 GHz   | 2.80 GHz   | 4 MB SmartCache         |
| Intel(R) Core[TM] i5-7300HQ Processor                                       | Launched        | Q1'17   | 4   | 3.50 GHz   | 2.50 GHz   | 6 MB                    |
| Intel(R) Core[TM] i3-7350K Processor                                        | Launched        | Q1'17   | 2   |            | 4.20 GHz   | 4 MB SmartCache         |
| Intel(R) Core[TM] i3-7300T Processor                                        | Launched        | Q1'17   | 2   |            | 3.50 GHz   | 4 MB SmartCache         |
| Intel(R) Core[TM] i3-7102E Processor                                        | Launched        | Q1'17   | 2   |            | 2.10 GHz   | 3 MB                    |
| Intel(R) Core[TM] i7-7660U Processor                                        | Launched        | Q1'17   | 2   | 4.00 GHz   | 2.50 GHz   | 4 MB                    |
| Intel(R) Core[TM] i3-7167U Processor                                        | Launched        | Q1'17   | 2   |            | 2.80 GHz   | 3 MB                    |
| Intel(R) Core[TM] i3-7101TE Processor                                       | Launched        | Q1'17   | 2   |            | 3.40 GHz   | 3 MB SmartCache         |
| Intel(R) Core[TM] i5-7267U Processor                                        | Launched        | Q1'17   | 2   | 3.50 GHz   | 3.10 GHz   | 4 MB                    |
| Intel(R) Core[TM] i3-7300 Processor                                         | Launched        | Q1'17   | 2   |            | 4.00 GHz   | 4 MB SmartCache         |
| Intel(R) Core[TM] i5-7600K Processor                                        | Launched        | Q1'17   | 4   | 4.20 GHz   | 3.80 GHz   | 6 MB SmartCache         |
| Intel(R) Core[TM] i3-7100H Processor                                        | Launched        | Q1'17   | 2   |            | 3.00 GHz   | 3 MB                    |
| Intel(R) Core[TM] i5-7260U Processor                                        | Launched        | Q1'17   | 2   | 3.40 GHz   | 2.20 GHz   | 4 MB                    |
| Intel(R) Core[TM] i5-7440EQ Processor                                       | Launched        | Q1'17   | 4   | 3.60 GHz   | 2.90 GHz   | 6 MB                    |
| Intel(R) Core[TM] i7-7820EQ Processor                                       | Launched        | Q1'17   | 4   | 3.70 GHz   | 3.00 GHz   | 8 MB                    |
| Intel(R) Core[TM] i5-7440HQ Processor                                       | Launched        | Q1'17   | 4   | 3.80 GHz   | 2.80 GHz   | 6 MB                    |
| Intel(R) Core[TM] i7-7560U Processor                                        | Launched        | Q1'17   | 2   | 3.80 GHz   | 2.40 GHz   | 4 MB                    |
| Intel(R) Core[TM] i7-7700 Processor                                         | Launched        | Q1'17   | 4   | 4.20 GHz   | 3.60 GHz   | 8 MB SmartCache         |
| Intel(R) Core[TM] i5-7287U Processor                                        | Launched        | Q1'17   | 2   | 3.70 GHz   | 3.30 GHz   | 4 MB                    |
| Intel(R) Core[TM] i5-7442EQ Processor                                       | Launched        | Q1'17   | 4   | 2.90 GHz   | 2.10 GHz   | 6 MB                    |
| Intel(R) Core[TM] i5-7300U Processor                                        | Launched        | Q1'17   | 2   | 3.50 GHz   | 2.60 GHz   | 3 MB SmartCache         |
| Intel(R) Core[TM] i7-7567U Processor                                        | Launched        | Q1'17   | 2   | 4.00 GHz   | 3.50 GHz   | 4 MB                    |
| Intel(R) Core[TM] i3-7320 Processor                                         | Launched        | Q1'17   | 2   |            | 4.10 GHz   | 4 MB SmartCache         |
| Intel(R) Core[TM] i5-7Y57 Processor                                         | Launched        | Q1'17   | 2   | 3.30 GHz   | 1.20 GHz   | 4 MB SmartCache         |
| Intel(R) Core[TM] i3-7100T Processor                                        | Launched        | Q1'17   | 2   |            | 3.40 GHz   | 3 MB SmartCache         |
| Intel(R) Core[TM] i7-7700K Processor                                        | Launched        | Q1'17   | 4   | 4.50 GHz   | 4.20 GHz   | 8 MB SmartCache         |
| Intel(R) Core[TM] i7-7920HQ Processor                                       | Launched        | Q1'17   | 4   | 4.10 GHz   | 3.10 GHz   | 8 MB                    |
| Intel(R) Core[TM] i5-7360U Processor                                        | Launched        | Q1'17   | 2   | 3.60 GHz   | 2.30 GHz   | 4 MB                    |
| Intel(R) Core[TM] i7-7820HQ Processor                                       | Launched        | Q1'17   | 4   | 3.90 GHz   | 2.90 GHz   | 8 MB                    |
| Intel(R) Core[TM] i3-6006U Processor                                        | Launched        | Q4'16   | 2   |            | 2.00 GHz   | 3 MB SmartCache         |
| Intel(R) Core[TM] i3-6157U Processor                                        | Launched        | Q3'16   | 2   |            | 2.40 GHz   | 3 MB SmartCache         |
| Intel(R) Core[TM] i7-7Y75 Processor                                         | Launched        | Q3'16   | 2   | 3.60 GHz   | 1.30 GHz   | 4 MB SmartCache         |
| Intel(R) Core[TM] i3-7100U Processor                                        | Launched        | Q3'16   | 2   |            | 2.40 GHz   | 3 MB SmartCache         |
| Intel(R) Core[TM] i5-7200U Processor                                        | Launched        | Q3'16   | 2   | 3.10 GHz   | 2.50 GHz   | 3 MB SmartCache         |
| Intel(R) Core[TM] m3-7Y30 Processor                                         | Launched        | Q3'16   | 2   | 2.60 GHz   | 1.00 GHz   | 4 MB SmartCache         |
| Intel(R) Core[TM] i7-7500U Processor                                        | Launched        | Q3'16   | 2   | 3.50 GHz   | 2.70 GHz   | 4 MB SmartCache         |
| Intel(R) Core[TM] i5-7Y54 Processor                                         | Launched        | Q3'16   | 2   | 3.20 GHz   | 1.20 GHz   | 4 MB SmartCache         |
| Intel(R) Core[TM] i5-6585R Processor                                        | Discontinued    | Q2'16   | 4   | 3.60 GHz   | 2.80 GHz   | 6 MB                    |
| Intel(R) Core[TM] i5-6685R Processor                                        | Discontinued    | Q2'16   | 4   | 3.80 GHz   | 3.20 GHz   | 6 MB                    |
| Intel(R) Core[TM] i7-6785R Processor                                        | Discontinued    | Q2'16   | 4   | 3.90 GHz   | 3.30 GHz   | 8 MB                    |
| Intel(R) Core[TM] i7-6660U Processor                                        | Launched        | Q1'16   | 2   | 3.40 GHz   | 2.40 GHz   | 4 MB SmartCache         |
| Intel(R) Core[TM] i5-6350HQ Processor                                       | Launched        | Q1'16   | 4   | 3.20 GHz   | 2.30 GHz   | 6 MB SmartCache         |
| Intel(R) Core[TM] i7-6970HQ Processor                                       | Launched        | Q1'16   | 4   | 3.70 GHz   | 2.80 GHz   | 8 MB SmartCache         |
| Intel(R) Core[TM] i7-6870HQ Processor                                       | Launched        | Q1'16   | 4   | 3.60 GHz   | 2.70 GHz   | 8 MB SmartCache         |
| Intel(R) Core[TM] i7-6770HQ Processor                                       | Launched        | Q1'16   | 4   | 3.50 GHz   | 2.60 GHz   | 6 MB SmartCache         |
| Intel(R) Core[TM] i5-6402P Processor                                        | Discontinued    | Q4'15   | 4   | 3.40 GHz   | 2.80 GHz   | 6 MB                    |
| Intel(R) Core[TM] i3-6098P Processor                                        | Discontinued    | Q4'15   | 2   |            | 3.60 GHz   | 3 MB                    |
| Intel(R) Core[TM] i7-6700TE Processor                                       | Launched        | Q4'15   | 4   | 3.40 GHz   | 2.40 GHz   | 8 MB SmartCache         |
| Intel(R) Core[TM] i3-6100TE Processor                                       | Launched        | Q4'15   | 2   |            | 2.70 GHz   | 4 MB SmartCache         |
| Intel(R) Core[TM] i5-6500TE Processor                                       | Launched        | Q4'15   | 4   | 3.30 GHz   | 2.30 GHz   | 6 MB SmartCache         |
| Intel(R) Core[TM] i5-6440EQ Processor                                       | Launched        | Q4'15   | 4   | 3.40 GHz   | 2.70 GHz   | 6 MB SmartCache         |
| Intel(R) Core[TM] i7-6820EQ Processor                                       | Launched        | Q4'15   | 4   | 3.50 GHz   | 2.80 GHz   | 8 MB SmartCache         |
| Intel(R) Core[TM] i7-6822EQ Processor                                       | Launched        | Q4'15   | 4   | 2.80 GHz   | 2.00 GHz   | 8 MB SmartCache         |
| Intel(R) Core[TM] i5-6442EQ Processor                                       | Launched        | Q4'15   | 4   | 2.70 GHz   | 1.90 GHz   | 6 MB SmartCache         |
| Intel(R) Core[TM] i3-6100E Processor                                        | Launched        | Q4'15   | 2   |            | 2.70 GHz   | 3 MB SmartCache         |
| Intel(R) Core[TM] i3-6102E Processor                                        | Launched        | Q4'15   | 2   |            | 1.90 GHz   | 3 MB SmartCache         |
| Intel(R) Core[TM] m7-6Y75 Processor                                         | Launched        | Q3'15   | 2   | 3.10 GHz   | 1.20 GHz   | 4 MB SmartCache         |
| Intel(R) Core[TM] i5-6300U Processor                                        | Launched        | Q3'15   | 2   | 3.00 GHz   | 2.40 GHz   | 3 MB SmartCache         |
| Intel(R) Core[TM] i3-6100U Processor                                        | Launched        | Q3'15   | 2   |            | 2.30 GHz   | 3 MB SmartCache         |
| Intel(R) Core[TM] m5-6Y54 Processor                                         | Launched        | Q3'15   | 2   | 2.70 GHz   | 1.10 GHz   | 4 MB SmartCache         |
| Intel(R) Core[TM] i7-6600U Processor                                        | Launched        | Q3'15   | 2   | 3.40 GHz   | 2.60 GHz   | 4 MB SmartCache         |
| Intel(R) Core[TM] i5-6200U Processor                                        | Launched        | Q3'15   | 2   | 2.80 GHz   | 2.30 GHz   | 3 MB SmartCache         |
| Intel(R) Core[TM] i7-6500U Processor                                        | Launched        | Q3'15   | 2   | 3.10 GHz   | 2.50 GHz   | 4 MB SmartCache         |
| Intel(R) Core[TM] m5-6Y57 Processor                                         | Launched        | Q3'15   | 2   | 2.80 GHz   | 1.10 GHz   | 4 MB SmartCache         |
| Intel(R) Core[TM] m3-6Y30 Processor                                         | Launched        | Q3'15   | 2   | 2.20 GHz   | 900 MHz    | 4 MB SmartCache         |
| Intel(R) Core[TM] i7-6700HQ Processor                                       | Launched        | Q3'15   | 4   | 3.50 GHz   | 2.60 GHz   | 6 MB SmartCache         |
| Intel(R) Core[TM] i7-6820HK Processor                                       | Launched        | Q3'15   | 4   | 3.60 GHz   | 2.70 GHz   | 8 MB SmartCache         |
| Intel(R) Core[TM] i7-6820HQ Processor                                       | Launched        | Q3'15   | 4   | 3.60 GHz   | 2.70 GHz   | 8 MB SmartCache         |
| Intel(R) Core[TM] i7-6920HQ Processor                                       | Launched        | Q3'15   | 4   | 3.80 GHz   | 2.90 GHz   | 8 MB SmartCache         |
| Intel(R) Core[TM] i5-6300HQ Processor                                       | Launched        | Q3'15   | 4   | 3.20 GHz   | 2.30 GHz   | 6 MB SmartCache         |
| Intel(R) Core[TM] i5-6440HQ Processor                                       | Launched        | Q3'15   | 4   | 3.50 GHz   | 2.60 GHz   | 6 MB SmartCache         |
| Intel(R) Core[TM] i3-6100H Processor                                        | Launched        | Q3'15   | 2   |            | 2.70 GHz   | 3 MB SmartCache         |
| Intel(R) Core[TM] i7-6560U Processor                                        | Launched        | Q3'15   | 2   | 3.20 GHz   | 2.20 GHz   | 4 MB SmartCache         |
| Intel(R) Core[TM] i3-6167U Processor                                        | Launched        | Q3'15   | 2   |            | 2.70 GHz   | 3 MB SmartCache         |
| Intel(R) Core[TM] i5-6287U Processor                                        | Launched        | Q3'15   | 2   | 3.50 GHz   | 3.10 GHz   | 4 MB SmartCache         |
| Intel(R) Core[TM] i5-6360U Processor                                        | Launched        | Q3'15   | 2   | 3.10 GHz   | 2.00 GHz   | 4 MB SmartCache         |
| Intel(R) Core[TM] i5-6267U Processor                                        | Launched        | Q3'15   | 2   | 3.30 GHz   | 2.90 GHz   | 4 MB SmartCache         |
| Intel(R) Core[TM] i5-6260U Processor                                        | Launched        | Q3'15   | 2   | 2.90 GHz   | 1.80 GHz   | 4 MB SmartCache         |
| Intel(R) Core[TM] i7-6650U Processor                                        | Launched        | Q3'15   | 2   | 3.40 GHz   | 2.20 GHz   | 4 MB SmartCache         |
| Intel(R) Core[TM] i5-6600T Processor                                        | Launched        | Q3'15   | 4   | 3.50 GHz   | 2.70 GHz   | 6 MB SmartCache         |
| Intel(R) Core[TM] i7-6700T Processor                                        | Launched        | Q3'15   | 4   | 3.60 GHz   | 2.80 GHz   | 8 MB SmartCache         |
| Intel(R) Core[TM] i5-6500T Processor                                        | Launched        | Q3'15   | 4   | 3.10 GHz   | 2.50 GHz   | 6 MB SmartCache         |
| Intel(R) Core[TM] i5-6500 Processor                                         | Launched        | Q3'15   | 4   | 3.60 GHz   | 3.20 GHz   | 6 MB SmartCache         |
| Intel(R) Core[TM] i7-6700 Processor                                         | Launched        | Q3'15   | 4   | 4.00 GHz   | 3.40 GHz   | 8 MB SmartCache         |
| Intel(R) Core[TM] i5-6400 Processor                                         | Launched        | Q3'15   | 4   | 3.30 GHz   | 2.70 GHz   | 6 MB SmartCache         |
| Intel(R) Core[TM] i5-6400T Processor                                        | Launched        | Q3'15   | 4   | 2.80 GHz   | 2.20 GHz   | 6 MB SmartCache         |
| Intel(R) Core[TM] i5-6600 Processor                                         | Launched        | Q3'15   | 4   | 3.90 GHz   | 3.30 GHz   | 6 MB SmartCache         |
| Intel(R) Core[TM] i3-6300T Processor                                        | Launched        | Q3'15   | 2   |            | 3.30 GHz   | 4 MB SmartCache         |
| Intel(R) Core[TM] i3-6100 Processor                                         | Launched        | Q3'15   | 2   |            | 3.70 GHz   | 3 MB SmartCache         |
| Intel(R) Core[TM] i3-6300 Processor                                         | Launched        | Q3'15   | 2   |            | 3.80 GHz   | 4 MB SmartCache         |
| Intel(R) Core[TM] i3-6320 Processor                                         | Launched        | Q3'15   | 2   |            | 3.90 GHz   | 4 MB SmartCache         |
| Intel(R) Core[TM] i3-6100T Processor                                        | Launched        | Q3'15   | 2   |            | 3.20 GHz   | 3 MB SmartCache         |
| Intel(R) Core[TM] i5-6600K Processor                                        | Discontinued    | Q3'15   | 4   | 3.90 GHz   | 3.50 GHz   | 6 MB SmartCache         |
| Intel(R) Core[TM] i7-6700K Processor                                        | Discontinued    | Q3'15   | 4   | 4.20 GHz   | 4.00 GHz   | 8 MB SmartCache         |
| Intel(R) Core[TM] i7-6567U Processor                                        | Launched        | Q3'15   | 2   | 3.60 GHz   | 3.30 GHz   | 4 MB SmartCache         |
| Intel(R) Core[TM] i7-5775R Processor                                        | Discontinued    | Q2'15   | 4   | 3.80 GHz   | 3.30 GHz   | 6 MB                    |
| Intel(R) Core[TM] i7-5850HQ Processor                                       | Launched        | Q2'15   | 4   | 3.60 GHz   | 2.70 GHz   | 6 MB                    |
| Intel(R) Core[TM] i7-5950HQ Processor                                       | Launched        | Q2'15   | 4   | 3.80 GHz   | 2.90 GHz   | 6 MB                    |
| Intel(R) Core[TM] i5-5350H Processor                                        | Launched        | Q2'15   | 2   | 3.50 GHz   | 3.10 GHz   | 4 MB                    |
| Intel(R) Core[TM] i7-5700EQ Processor                                       | Launched        | Q2'15   | 4   | 3.40 GHz   | 2.60 GHz   | 6 MB SmartCache         |
| Intel(R) Core[TM] i7-5775C Processor                                        | Launched        | Q2'15   | 4   | 3.70 GHz   | 3.30 GHz   | 6 MB                    |
| Intel(R) Core[TM] i7-5850EQ Processor                                       | Launched        | Q2'15   | 4   | 3.40 GHz   | 2.70 GHz   | 6 MB SmartCache         |
| Intel(R) Core[TM] i5-5575R Processor                                        | Discontinued    | Q2'15   | 4   | 3.30 GHz   | 2.80 GHz   | 4 MB                    |
| Intel(R) Core[TM] i5-5675R Processor                                        | Discontinued    | Q2'15   | 4   | 3.60 GHz   | 3.10 GHz   | 4 MB                    |
| Intel(R) Core[TM] i5-5675C Processor                                        | Launched        | Q2'15   | 4   | 3.60 GHz   | 3.10 GHz   | 4 MB                    |
| Intel(R) Core[TM] i7-5700HQ Processor                                       | Launched        | Q2'15   | 4   | 3.50 GHz   | 2.70 GHz   | 6 MB                    |
| Intel(R) Core[TM] i7-5750HQ Processor                                       | Launched        | Q2'15   | 4   | 3.40 GHz   | 2.50 GHz   | 6 MB                    |
| Intel(R) Core[TM] i3-4170 Processor                                         | Discontinued    | Q1'15   | 2   |            | 3.70 GHz   | 3 MB                    |
| Intel(R) Core[TM] i3-4370T Processor                                        | Discontinued    | Q1'15   | 2   |            | 3.30 GHz   | 4 MB                    |
| Intel(R) Core[TM] i3-4170T Processor                                        | Discontinued    | Q1'15   | 2   |            | 3.20 GHz   | 3 MB                    |
| Intel(R) Core[TM] i3-5015U Processor                                        | Launched        | Q1'15   | 2   |            | 2.10 GHz   | 3 MB                    |
| Intel(R) Core[TM] i3-5020U Processor                                        | Launched        | Q1'15   | 2   |            | 2.20 GHz   | 3 MB                    |
| Intel(R) Core[TM] i7-4720HQ Processor                                       | Launched        | Q1'15   | 4   | 3.60 GHz   | 2.60 GHz   | 6 MB                    |
| Intel(R) Core[TM] i7-4722HQ Processor                                       | Launched        | Q1'15   | 4   | 3.40 GHz   | 2.40 GHz   | 6 MB                    |
| Intel(R) Core[TM] i3-5005U Processor                                        | Launched        | Q1'15   | 2   |            | 2.00 GHz   | 3 MB                    |
| Intel(R) Core[TM] i3-5010U Processor                                        | Launched        | Q1'15   | 2   |            | 2.10 GHz   | 3 MB                    |
| Intel(R) Core[TM] i5-5200U Processor                                        | Launched        | Q1'15   | 2   | 2.70 GHz   | 2.20 GHz   | 3 MB                    |
| Intel(R) Core[TM] i5-5300U Processor                                        | Launched        | Q1'15   | 2   | 2.90 GHz   | 2.30 GHz   | 3 MB                    |
| Intel(R) Core[TM] i5-5287U Processor                                        | Launched        | Q1'15   | 2   | 3.30 GHz   | 2.90 GHz   | 3 MB                    |
| Intel(R) Core[TM] i7-5500U Processor                                        | Launched        | Q1'15   | 2   | 3.00 GHz   | 2.40 GHz   | 4 MB                    |
| Intel(R) Core[TM] i7-5600U Processor                                        | Launched        | Q1'15   | 2   | 3.20 GHz   | 2.60 GHz   | 4 MB                    |
| Intel(R) Core[TM] i5-5350U Processor                                        | Launched        | Q1'15   | 2   | 2.90 GHz   | 1.80 GHz   | 3 MB                    |
| Intel(R) Core[TM] i7-5550U Processor                                        | Launched        | Q1'15   | 2   | 3.00 GHz   | 2.00 GHz   | 4 MB SmartCache         |
| Intel(R) Core[TM] i7-5557U Processor                                        | Launched        | Q1'15   | 2   | 3.40 GHz   | 3.10 GHz   | 4 MB                    |
| Intel(R) Core[TM] i7-5650U Processor                                        | Launched        | Q1'15   | 2   | 3.10 GHz   | 2.20 GHz   | 4 MB                    |
| Intel(R) Core[TM] i3-5157U Processor                                        | Launched        | Q1'15   | 2   |            | 2.50 GHz   | 3 MB                    |
| Intel(R) Core[TM] i5-5250U Processor                                        | Launched        | Q1'15   | 2   | 2.70 GHz   | 1.60 GHz   | 3 MB                    |
| Intel(R) Core[TM] i5-5257U Processor                                        | Launched        | Q1'15   | 2   | 3.10 GHz   | 2.70 GHz   | 3 MB                    |
| Intel(R) Core[TM] M-5Y71 Processor                                          | Launched        | Q4'14   | 2   | 2.90 GHz   | 1.20 GHz   | 4 MB                    |
| Intel(R) Core[TM] M-5Y31 Processor                                          | Launched        | Q4'14   | 2   | 2.40 GHz   | 900 MHz    | 4 MB                    |
| Intel(R) Core[TM] M-5Y51 Processor                                          | Launched        | Q4'14   | 2   | 2.60 GHz   | 1.10 GHz   | 4 MB                    |
| Intel(R) Core[TM] M-5Y10c Processor                                         | Launched        | Q4'14   | 2   | 2.00 GHz   | 800 MHz    | 4 MB                    |
| Intel(R) Core[TM] M-5Y10 Processor                                          | Launched        | Q3'14   | 2   | 2.00 GHz   | 800 MHz    | 4 MB                    |
| Intel(R) Core[TM] M-5Y10a Processor                                         | Launched        | Q3'14   | 2   | 2.00 GHz   | 800 MHz    | 4 MB                    |
| Intel(R) Core[TM] M-5Y70 Processor                                          | Launched        | Q3'14   | 2   | 2.60 GHz   | 1.10 GHz   | 4 MB                    |
| Intel(R) Core[TM] i7-5960X Processor Extreme Edition                        | Discontinued    | Q3'14   | 8   | 3.50 GHz   | 3.00 GHz   | 20 MB SmartCache        |
| Intel(R) Core[TM] i7-5930K Processor                                        | Discontinued    | Q3'14   | 6   | 3.70 GHz   | 3.50 GHz   | 15 MB SmartCache        |
| Intel(R) Core[TM] i7-5820K Processor                                        | Discontinued    | Q3'14   | 6   | 3.60 GHz   | 3.30 GHz   | 15 MB SmartCache        |
| Intel(R) Core[TM] i3-4160 Processor                                         | Discontinued    | Q3'14   | 2   |            | 3.60 GHz   | 3 MB SmartCache         |
| Intel(R) Core[TM] i3-4160T Processor                                        | Discontinued    | Q3'14   | 2   |            | 3.10 GHz   | 3 MB SmartCache         |
| Intel(R) Core[TM] i3-4360T Processor                                        | Discontinued    | Q3'14   | 2   |            | 3.20 GHz   | 4 MB SmartCache         |
| Intel(R) Core[TM] i3-4370 Processor                                         | Discontinued    | Q3'14   | 2   |            | 3.80 GHz   | 4 MB SmartCache         |
| Intel(R) Core[TM] i5-4210H Processor                                        | Launched        | Q3'14   | 2   | 3.50 GHz   | 2.90 GHz   | 3 MB SmartCache         |
| Intel(R) Core[TM] i7-4980HQ Processor                                       | Launched        | Q3'14   | 4   | 4.00 GHz   | 2.80 GHz   | 6 MB                    |
| Intel(R) Core[TM] i7-4870HQ Processor                                       | Launched        | Q3'14   | 4   | 3.70 GHz   | 2.50 GHz   | 6 MB                    |
| Intel(R) Core[TM] i7-4770HQ Processor                                       | Launched        | Q3'14   | 4   | 3.40 GHz   | 2.20 GHz   | 6 MB                    |
| Intel(R) Core[TM] i7-4578U Processor                                        | Launched        | Q3'14   | 2   | 3.50 GHz   | 3.00 GHz   | 4 MB                    |
| Intel(R) Core[TM] i5-4308U Processor                                        | Launched        | Q3'14   | 2   | 3.30 GHz   | 2.80 GHz   | 3 MB                    |
| Intel(R) Core[TM] i5-4278U Processor                                        | Launched        | Q3'14   | 2   | 3.10 GHz   | 2.60 GHz   | 3 MB                    |
| Intel(R) Core[TM] i5-4690K Processor                                        | Discontinued    | Q2'14   | 4   | 3.90 GHz   | 3.50 GHz   | 6 MB SmartCache         |
| Intel(R) Core[TM] i7-4790K Processor                                        | Discontinued    | Q2'14   | 4   | 4.40 GHz   | 4.00 GHz   | 8 MB SmartCache         |
| Intel(R) Core[TM] i3-4150T Processor                                        | Discontinued    | Q2'14   | 2   |            | 3.00 GHz   | 3 MB SmartCache         |
| Intel(R) Core[TM] i3-4350 Processor                                         | Discontinued    | Q2'14   | 2   |            | 3.60 GHz   | 4 MB SmartCache         |
| Intel(R) Core[TM] i3-4350T Processor                                        | Launched        | Q2'14   | 2   |            | 3.10 GHz   | 4 MB SmartCache         |
| Intel(R) Core[TM] i3-4150 Processor                                         | Discontinued    | Q2'14   | 2   |            | 3.50 GHz   | 3 MB SmartCache         |
| Intel(R) Core[TM] i3-4360 Processor                                         | Launched        | Q2'14   | 2   |            | 3.70 GHz   | 4 MB SmartCache         |
| Intel(R) Core[TM] i5-4460T Processor                                        | Discontinued    | Q2'14   | 4   | 2.70 GHz   | 1.90 GHz   | 6 MB SmartCache         |
| Intel(R) Core[TM] i5-4590T Processor                                        | Launched        | Q2'14   | 4   | 3.00 GHz   | 2.00 GHz   | 6 MB SmartCache         |
| Intel(R) Core[TM] i7-4790S Processor                                        | Launched        | Q2'14   | 4   | 4.00 GHz   | 3.20 GHz   | 8 MB SmartCache         |
| Intel(R) Core[TM] i5-4460S Processor                                        | Discontinued    | Q2'14   | 4   | 3.40 GHz   | 2.90 GHz   | 6 MB SmartCache         |
| Intel(R) Core[TM] i7-4790T Processor                                        | Discontinued    | Q2'14   | 4   | 3.90 GHz   | 2.70 GHz   | 8 MB SmartCache         |
| Intel(R) Core[TM] i5-4690 Processor                                         | Discontinued    | Q2'14   | 4   | 3.90 GHz   | 3.50 GHz   | 6 MB SmartCache         |
| Intel(R) Core[TM] i5-4690S Processor                                        | Discontinued    | Q2'14   | 4   | 3.90 GHz   | 3.20 GHz   | 6 MB SmartCache         |
| Intel(R) Core[TM] i5-4690T Processor                                        | Discontinued    | Q2'14   | 4   | 3.50 GHz   | 2.50 GHz   | 6 MB SmartCache         |
| Intel(R) Core[TM] i7-4790 Processor                                         | Discontinued    | Q2'14   | 4   | 4.00 GHz   | 3.60 GHz   | 8 MB SmartCache         |
| Intel(R) Core[TM] i7-4785T Processor                                        | Discontinued    | Q2'14   | 4   | 3.20 GHz   | 2.20 GHz   | 8 MB SmartCache         |
| Intel(R) Core[TM] i5-4590 Processor                                         | Discontinued    | Q2'14   | 4   | 3.70 GHz   | 3.30 GHz   | 6 MB SmartCache         |
| Intel(R) Core[TM] i5-4590S Processor                                        | Launched        | Q2'14   | 4   | 3.70 GHz   | 3.00 GHz   | 6 MB SmartCache         |
| Intel(R) Core[TM] i5-4460 Processor                                         | Discontinued    | Q2'14   | 4   | 3.40 GHz   | 3.20 GHz   | 6 MB SmartCache         |
| Intel(R) Core[TM] i5-4260U Processor                                        | Discontinued    | Q2'14   | 2   | 2.70 GHz   | 1.40 GHz   | 3 MB SmartCache         |
| Intel(R) Core[TM] i7-4760HQ Processor                                       | Launched        | Q2'14   | 4   | 3.30 GHz   | 2.10 GHz   | 6 MB SmartCache         |
| Intel(R) Core[TM] i3-4110M Processor                                        | Launched        | Q2'14   | 2   |            | 2.60 GHz   | 3 MB SmartCache         |
| Intel(R) Core[TM] i7-4710MQ Processor                                       | Launched        | Q2'14   | 4   | 3.50 GHz   | 2.50 GHz   | 6 MB SmartCache         |
| Intel(R) Core[TM] i7-4712HQ Processor                                       | Launched        | Q2'14   | 4   | 3.30 GHz   | 2.30 GHz   | 6 MB SmartCache         |
| Intel(R) Core[TM] i7-4712MQ Processor                                       | Launched        | Q2'14   | 4   | 3.30 GHz   | 2.30 GHz   | 6 MB SmartCache         |
| Intel(R) Core[TM] i3-4110E Processor                                        | Launched        | Q2'14   | 2   |            | 2.60 GHz   | 3 MB SmartCache         |
| Intel(R) Core[TM] i3-4112E Processor                                        | Launched        | Q2'14   | 2   |            | 1.80 GHz   | 3 MB SmartCache         |
| Intel(R) Core[TM] i5-4410E Processor                                        | Launched        | Q2'14   | 2   |            | 2.90 GHz   | 3 MB SmartCache         |
| Intel(R) Core[TM] i5-4422E Processor                                        | Launched        | Q2'14   | 2   | 2.90 GHz   | 1.80 GHz   | 3 MB SmartCache         |
| Intel(R) Core[TM] i7-4710HQ Processor                                       | Launched        | Q2'14   | 4   | 3.50 GHz   | 2.50 GHz   | 6 MB SmartCache         |
| Intel(R) Core[TM] i3-4030U Processor                                        | Launched        | Q2'14   | 2   |            | 1.90 GHz   | 3 MB SmartCache         |
| Intel(R) Core[TM] i3-4025U Processor                                        | Launched        | Q2'14   | 2   |            | 1.90 GHz   | 3 MB SmartCache         |
| Intel(R) Core[TM] i5-4220Y Processor                                        | Launched        | Q2'14   | 2   | 2.00 GHz   | 1.60 GHz   | 3 MB                    |
| Intel(R) Core[TM] i3-4030Y Processor                                        | Launched        | Q2'14   | 2   |            | 1.60 GHz   | 3 MB SmartCache         |
| Intel(R) Core[TM] i5-4210M Processor                                        | Launched        | Q2'14   | 2   | 3.20 GHz   | 2.60 GHz   | 3 MB SmartCache         |
| Intel(R) Core[TM] i7-4510U Processor                                        | Launched        | Q2'14   | 2   | 3.10 GHz   | 2.00 GHz   | 4 MB SmartCache         |
| Intel(R) Core[TM] i5-4210U Processor                                        | Launched        | Q2'14   | 2   | 2.70 GHz   | 1.70 GHz   | 3 MB SmartCache         |
| Intel(R) Core[TM] i3-4120U Processor                                        | Launched        | Q2'14   | 2   |            | 2.00 GHz   | 3 MB                    |
| Intel(R) Core[TM] i7-4700EC Processor                                       | Launched        | Q1'14   | 4   |            | 2.70 GHz   | 8 MB SmartCache         |
| Intel(R) Core[TM] i7-4702EC Processor                                       | Launched        | Q1'14   | 4   |            | 2.00 GHz   | 8 MB SmartCache         |
| Intel(R) Core[TM] i5-4402EC Processor                                       | Launched        | Q1'14   | 2   |            | 2.50 GHz   | 4 MB SmartCache         |
| Intel(R) Core[TM] i5-4360U Processor                                        | Launched        | Q1'14   | 2   | 3.00 GHz   | 1.50 GHz   | 3 MB SmartCache         |
| Intel(R) Core[TM] i7-4860HQ Processor                                       | Launched        | Q1'14   | 4   | 3.60 GHz   | 2.40 GHz   | 6 MB SmartCache         |
| Intel(R) Core[TM] i7-4810MQ Processor                                       | Launched        | Q1'14   | 4   | 3.80 GHz   | 2.80 GHz   | 6 MB SmartCache         |
| Intel(R) Core[TM] i7-4910MQ Processor                                       | Launched        | Q1'14   | 4   | 3.90 GHz   | 2.90 GHz   | 8 MB SmartCache         |
| Intel(R) Core[TM] i7-4940MX Processor Extreme Edition                       | Launched        | Q1'14   | 4   | 4.00 GHz   | 3.10 GHz   | 8 MB SmartCache         |
| Intel(R) Core[TM] i5-4310M Processor                                        | Launched        | Q1'14   | 2   | 3.40 GHz   | 2.70 GHz   | 3 MB SmartCache         |
| Intel(R) Core[TM] i5-4310U Processor                                        | Launched        | Q1'14   | 2   | 3.00 GHz   | 2.00 GHz   | 3 MB SmartCache         |
| Intel(R) Core[TM] i5-4340M Processor                                        | Launched        | Q1'14   | 2   | 3.60 GHz   | 2.90 GHz   | 3 MB SmartCache         |
| Intel(R) Core[TM] i7-4610M Processor                                        | Launched        | Q1'14   | 2   | 3.70 GHz   | 3.00 GHz   | 4 MB SmartCache         |
| Intel(R) Core[TM] i5-4440S Processor                                        | Discontinued    | Q3'13   | 4   | 3.30 GHz   | 2.80 GHz   | 6 MB SmartCache         |
| Intel(R) Core[TM] i5-4200H Processor                                        | Launched        | Q4'13   | 2   | 3.40 GHz   | 2.80 GHz   | 3 MB SmartCache         |
| Intel(R) Core[TM] i5-4440 Processor                                         | Discontinued    | Q3'13   | 4   | 3.30 GHz   | 3.10 GHz   | 6 MB SmartCache         |
| Intel(R) Core[TM] i3-4000M Processor                                        | Launched        | Q4'13   | 2   |            | 2.40 GHz   | 3 MB SmartCache         |
| Intel(R) Core[TM] i3-4005U Processor                                        | Launched        | Q3'13   | 2   |            | 1.70 GHz   | 3 MB SmartCache         |
| Intel(R) Core[TM] i5-4300U Processor                                        | Launched        | Q3'13   | 2   | 2.90 GHz   | 1.90 GHz   | 3 MB SmartCache         |
| Intel(R) Core[TM] i7-4600U Processor                                        | Launched        | Q3'13   | 2   | 3.30 GHz   | 2.10 GHz   | 4 MB SmartCache         |
| Intel(R) Core[TM] i7-4960HQ Processor                                       | Launched        | Q4'13   | 4   | 3.80 GHz   | 2.60 GHz   | 6 MB SmartCache         |
| Intel(R) Core[TM] i3-4012Y Processor                                        | Launched        | Q3'13   | 2   |            | 1.50 GHz   | 3 MB SmartCache         |
| Intel(R) Core[TM] i7-4610Y Processor                                        | Launched        | Q3'13   | 2   | 2.90 GHz   | 1.70 GHz   | 4 MB SmartCache         |
| Intel(R) Core[TM] i3-4020Y Processor                                        | Launched        | Q3'13   | 2   |            | 1.50 GHz   | 3 MB SmartCache         |
| Intel(R) Core[TM] i5-4202Y Processor                                        | Launched        | Q3'13   | 2   | 2.00 GHz   | 1.60 GHz   | 3 MB SmartCache         |
| Intel(R) Core[TM] i5-4330M Processor                                        | Launched        | Q4'13   | 2   | 3.50 GHz   | 2.80 GHz   | 3 MB SmartCache         |
| Intel(R) Core[TM] i5-4400E Processor                                        | Launched        | Q3'13   | 2   | 3.30 GHz   | 2.70 GHz   | 3 MB SmartCache         |
| Intel(R) Core[TM] i5-4210Y Processor                                        | Launched        | Q3'13   | 2   | 1.90 GHz   | 1.50 GHz   | 3 MB SmartCache         |
| Intel(R) Core[TM] i3-4100M Processor                                        | Launched        | Q4'13   | 2   |            | 2.50 GHz   | 3 MB SmartCache         |
| Intel(R) Core[TM] i3-4100E Processor                                        | Launched        | Q3'13   | 2   |            | 2.40 GHz   | 3 MB SmartCache         |
| Intel(R) Core[TM] i5-4300Y Processor                                        | Launched        | Q3'13   | 2   | 2.30 GHz   | 1.60 GHz   | 3 MB SmartCache         |
| Intel(R) Core[TM] i5-4300M Processor                                        | Launched        | Q4'13   | 2   | 3.30 GHz   | 2.60 GHz   | 3 MB SmartCache         |
| Intel(R) Core[TM] i3-4102E Processor                                        | Launched        | Q3'13   | 2   |            | 1.60 GHz   | 3 MB SmartCache         |
| Intel(R) Core[TM] i5-4302Y Processor                                        | Launched        | Q3'13   | 2   | 2.30 GHz   | 1.60 GHz   | 3 MB SmartCache         |
| Intel(R) Core[TM] i5-4402E Processor                                        | Launched        | Q3'13   | 2   | 2.70 GHz   | 1.60 GHz   | 3 MB SmartCache         |
| Intel(R) Core[TM] i5-4200M Processor                                        | Launched        | Q4'13   | 2   | 3.10 GHz   | 2.50 GHz   | 3 MB SmartCache         |
| Intel(R) Core[TM] i7-4600M Processor                                        | Launched        | Q4'13   | 2   | 3.60 GHz   | 2.90 GHz   | 4 MB SmartCache         |
| Intel(R) Core[TM] i3-4130 Processor                                         | Discontinued    | Q3'13   | 2   |            | 3.40 GHz   | 3 MB SmartCache         |
| Intel(R) Core[TM] i3-4130T Processor                                        | Discontinued    | Q3'13   | 2   |            | 2.90 GHz   | 3 MB SmartCache         |
| Intel(R) Core[TM] i7-4771 Processor                                         | Discontinued    | Q3'13   | 4   | 3.90 GHz   | 3.50 GHz   | 8 MB SmartCache         |
| Intel(R) Core[TM] i3-4330 Processor                                         | Launched        | Q3'13   | 2   |            | 3.50 GHz   | 4 MB SmartCache         |
| Intel(R) Core[TM] i3-4330T Processor                                        | Discontinued    | Q3'13   | 2   |            | 3.00 GHz   | 4 MB SmartCache         |
| Intel(R) Core[TM] i3-4340 Processor                                         | Discontinued    | Q3'13   | 2   |            | 3.60 GHz   | 4 MB SmartCache         |
| Intel(R) Core[TM] i7-4950HQ Processor                                       | Launched        | Q3'13   | 4   | 3.60 GHz   | 2.40 GHz   | 6 MB SmartCache         |
| Intel(R) Core[TM] i7-4850HQ Processor                                       | Launched        | Q3'13   | 4   | 3.50 GHz   | 2.30 GHz   | 6 MB SmartCache         |
| Intel(R) Core[TM] i7-4750HQ Processor                                       | Launched        | Q3'13   | 4   | 3.20 GHz   | 2.00 GHz   | 6 MB SmartCache         |
| Intel(R) Core[TM] i5-4670R Processor                                        | Discontinued    | Q2'13   | 4   | 3.70 GHz   | 3.00 GHz   | 4 MB SmartCache         |
| Intel(R) Core[TM] i7-4770R Processor                                        | Discontinued    | Q2'13   | 4   | 3.90 GHz   | 3.20 GHz   | 6 MB SmartCache         |
| Intel(R) Core[TM] i5-4570R Processor                                        | Discontinued    | Q2'13   | 4   | 3.20 GHz   | 2.70 GHz   | 4 MB SmartCache         |
| Intel(R) Core[TM] i5-4250U Processor                                        | Launched        | Q3'13   | 2   | 2.60 GHz   | 1.30 GHz   | 3 MB SmartCache         |
| Intel(R) Core[TM] i7-4550U Processor                                        | Launched        | Q3'13   | 2   | 3.00 GHz   | 1.50 GHz   | 4 MB SmartCache         |
| Intel(R) Core[TM] i7-4650U Processor                                        | Launched        | Q3'13   | 2   | 3.30 GHz   | 1.70 GHz   | 4 MB SmartCache         |
| Intel(R) Core[TM] i5-4350U Processor                                        | Launched        | Q3'13   | 2   | 2.90 GHz   | 1.40 GHz   | 3 MB SmartCache         |
| Intel(R) Core[TM] i5-4200U Processor                                        | Launched        | Q3'13   | 2   | 2.60 GHz   | 1.60 GHz   | 3 MB SmartCache         |
| Intel(R) Core[TM] i7-4500U Processor                                        | Launched        | Q3'13   | 2   | 3.00 GHz   | 1.80 GHz   | 4 MB SmartCache         |
| Intel(R) Core[TM] i3-4010U Processor                                        | Launched        | Q3'13   | 2   |            | 1.70 GHz   | 3 MB SmartCache         |
| Intel(R) Core[TM] i3-4100U Processor                                        | Launched        | Q3'13   | 2   |            | 1.80 GHz   | 3 MB SmartCache         |
| Intel(R) Core[TM] i3-4010Y Processor                                        | Launched        | Q3'13   | 2   |            | 1.30 GHz   | 3 MB SmartCache         |
| Intel(R) Core[TM] i3-4158U Processor                                        | Launched        | Q3'13   | 2   |            | 2.00 GHz   | 3 MB SmartCache         |
| Intel(R) Core[TM] i5-4200Y Processor                                        | Launched        | Q3'13   | 2   | 1.90 GHz   | 1.40 GHz   | 3 MB SmartCache         |
| Intel(R) Core[TM] i5-4258U Processor                                        | Launched        | Q3'13   | 2   | 2.90 GHz   | 2.40 GHz   | 3 MB SmartCache         |
| Intel(R) Core[TM] i5-4288U Processor                                        | Launched        | Q3'13   | 2   | 3.10 GHz   | 2.60 GHz   | 3 MB SmartCache         |
| Intel(R) Core[TM] i7-4558U Processor                                        | Launched        | Q3'13   | 2   | 3.30 GHz   | 2.80 GHz   | 4 MB SmartCache         |
| Intel(R) Core[TM] i5-4570 Processor                                         | Discontinued    | Q2'13   | 4   | 3.60 GHz   | 3.20 GHz   | 6 MB SmartCache         |
| Intel(R) Core[TM] i7-4770S Processor                                        | Launched        | Q2'13   | 4   | 3.90 GHz   | 3.10 GHz   | 8 MB SmartCache         |
| Intel(R) Core[TM] i5-4570S Processor                                        | Launched        | Q2'13   | 4   | 3.60 GHz   | 2.90 GHz   | 6 MB SmartCache         |
| Intel(R) Core[TM] i7-4770T Processor                                        | Discontinued    | Q2'13   | 4   | 3.70 GHz   | 2.50 GHz   | 8 MB SmartCache         |
| Intel(R) Core[TM] i5-4570T Processor                                        | Discontinued    | Q2'13   | 2   | 3.60 GHz   | 2.90 GHz   | 4 MB SmartCache         |
| Intel(R) Core[TM] i5-4670 Processor                                         | Discontinued    | Q2'13   | 4   | 3.80 GHz   | 3.40 GHz   | 6 MB SmartCache         |
| Intel(R) Core[TM] i5-4430S Processor                                        | Discontinued    | Q2'13   | 4   | 3.20 GHz   | 2.70 GHz   | 6 MB SmartCache         |
| Intel(R) Core[TM] i5-4670K Processor                                        | Discontinued    | Q2'13   | 4   | 3.80 GHz   | 3.40 GHz   | 6 MB SmartCache         |
| Intel(R) Core[TM] i5-4670S Processor                                        | Discontinued    | Q2'13   | 4   | 3.80 GHz   | 3.10 GHz   | 6 MB SmartCache         |
| Intel(R) Core[TM] i7-4765T Processor                                        | Discontinued    | Q2'13   | 4   | 3.00 GHz   | 2.00 GHz   | 8 MB SmartCache         |
| Intel(R) Core[TM] i5-4670T Processor                                        | Discontinued    | Q2'13   | 4   | 3.30 GHz   | 2.30 GHz   | 6 MB SmartCache         |
| Intel(R) Core[TM] i5-4570TE Processor                                       | Launched        | Q2'13   | 2   | 3.30 GHz   | 2.70 GHz   | 4 MB SmartCache         |
| Intel(R) Core[TM] i7-4770 Processor                                         | Discontinued    | Q2'13   | 4   | 3.90 GHz   | 3.40 GHz   | 8 MB SmartCache         |
| Intel(R) Core[TM] i7-4770K Processor                                        | Discontinued    | Q2'13   | 4   | 3.90 GHz   | 3.50 GHz   | 8 MB SmartCache         |
| Intel(R) Core[TM] i7-4770TE Processor                                       | Launched        | Q2'13   | 4   | 3.30 GHz   | 2.30 GHz   | 8 MB SmartCache         |
| Intel(R) Core[TM] i7-4700HQ Processor                                       | Launched        | Q2'13   | 4   | 3.40 GHz   | 2.40 GHz   | 6 MB SmartCache         |
| Intel(R) Core[TM] i7-4702HQ Processor                                       | Launched        | Q2'13   | 4   | 3.20 GHz   | 2.20 GHz   | 6 MB SmartCache         |
| Intel(R) Core[TM] i7-4800MQ Processor                                       | Launched        | Q2'13   | 4   | 3.70 GHz   | 2.70 GHz   | 6 MB SmartCache         |
| Intel(R) Core[TM] i7-4702MQ Processor                                       | Launched        | Q2'13   | 4   | 3.20 GHz   | 2.20 GHz   | 6 MB SmartCache         |
| Intel(R) Core[TM] i7-4900MQ Processor                                       | Launched        | Q2'13   | 4   | 3.80 GHz   | 2.80 GHz   | 8 MB SmartCache         |
| Intel(R) Core[TM] i7-4930MX Processor Extreme Edition                       | Launched        | Q2'13   | 4   | 3.90 GHz   | 3.00 GHz   | 8 MB SmartCache         |
| Intel(R) Core[TM] i7-4700EQ Processor                                       | Launched        | Q2'13   | 4   | 3.40 GHz   | 2.40 GHz   | 6 MB SmartCache         |
| Intel(R) Core[TM] i7-4700MQ Processor                                       | Launched        | Q2'13   | 4   | 3.40 GHz   | 2.40 GHz   | 6 MB SmartCache         |
| Intel(R) Core[TM] i5-4430 Processor                                         | Discontinued    | Q2'13   | 4   | 3.20 GHz   | 3.00 GHz   | 6 MB SmartCache         |
# INTEL PENTIUM
| PRODUCT NAME                                                           | STATUS          | RELEASE | COR | TURBO MAX  | BASE FREQ  | CACHE                   |
| ---------------------------------------------------------------------- | --------------- | ------- | --- | ---------- | ---------- | ----------------------- |
| Intel(R) Pentium(R) Processor D1519                                        | Launched        | Q2'16   | 4   | 2.10 GHz   | 1.50 GHz   | 6 MB                    |
| Intel(R) Pentium(R) Processor D1517                                        | Launched        | Q4'15   | 4   | 2.20 GHz   | 1.60 GHz   | 6 MB                    |
| Intel(R) Pentium(R) Processor D1508                                        | Launched        | Q4'15   | 2   | 2.60 GHz   | 2.20 GHz   | 3 MB                    |
| Intel(R) Pentium(R) Processor D1509                                        | Launched        | Q4'15   | 2   |            | 1.50 GHz   | 3 MB                    |
| Intel(R) Pentium(R) Processor D1507                                        | Launched        | Q4'15   | 2   |            | 1.20 GHz   | 3 MB                    |
# INTEL XEON
| PRODUCT NAME                                                           | STATUS          | RELEASE | COR | TURBO MAX  | BASE FREQ  | CACHE                   |
| ---------------------------------------------------------------------- | --------------- | ------- | --- | ---------- | ---------- | ----------------------- |
| Intel(R) Xeon(R) E-2276ME Processor                                        | Launched        | Q2'19   | 6   | 4.50 GHz   | 2.80 GHz   | 12 MB                   |
| Intel(R) Xeon(R) E-2254ML Processor                                        | Launched        | Q2' 19  | 4   | 3.50 GHz   | 1.70 GHz   | 8 MB                    |
| Intel(R) Xeon(R) E-2276ML Processor                                        | Launched        | Q2'19   | 6   | 4.20 GHz   | 2.00 GHz   | 12 MB                   |
| Intel(R) Xeon(R) E-2254ME Processor                                        | Launched        | Q2'19   | 4   | 3.80 GHz   | 2.60 GHz   | 8 MB                    |
| Intel(R) Xeon(R) E-2278GEL Processor                                       | Launched        | Q2'19   | 8   | 3.90 GHz   | 2.00 GHz   | 16 MB                   |
| Intel(R) Xeon(R) E-2278GE Processor                                        | Launched        | Q2'19   | 8   | 4.70 GHz   | 3.30 GHz   | 16 MB                   |
| Intel(R) Xeon(R) E-2226GE Processor                                        | Launched        | Q2'19   | 6   | 4.60 GHz   | 3.40 GHz   | 12 MB                   |
| Intel(R) Xeon(R) E-2226G Processor                                         | Launched        | Q2'19   | 6   | 4.70 GHz   | 3.40 GHz   | 12 MB                   |
| Intel(R) Xeon(R) E-2234 Processor                                          | Launched        | Q2'19   | 4   | 4.80 GHz   | 3.60 GHz   | 8 MB                    |
| Intel(R) Xeon(R) E-2236 Processor                                          | Launched        | Q2'19   | 6   | 4.80 GHz   | 3.40 GHz   | 12 MB                   |
| Intel(R) Xeon(R) E-2244G Processor                                         | Launched        | Q2'19   | 4   | 4.80 GHz   | 3.80 GHz   | 8 MB                    |
| Intel(R) Xeon(R) E-2274G Processor                                         | Launched        | Q2'19   | 4   | 4.90 GHz   | 4.00 GHz   | 8 MB                    |
| Intel(R) Xeon(R) E-2246G Processor                                         | Launched        | Q2'19   | 6   | 4.80 GHz   | 3.60 GHz   | 12 MB                   |
| Intel(R) Xeon(R) E-2224 Processor                                          | Launched        | Q2'19   | 4   | 4.60 GHz   | 3.40 GHz   | 8 MB                    |
| Intel(R) Xeon(R) E-2224G Processor                                         | Launched        | Q2'19   | 4   | 4.70 GHz   | 3.50 GHz   | 8 MB                    |
| Intel(R) Xeon(R) E-2288G Processor                                         | Launched        | Q2'19   | 8   | 5.00 GHz   | 3.70 GHz   | 16 MB                   |
| Intel(R) Xeon(R) E-2278G Processor                                         | Launched        | Q2'19   | 8   | 5.00 GHz   | 3.40 GHz   | 16 MB                   |
| Intel(R) Xeon(R) E-2286G Processor                                         | Launched        | Q2'19   | 6   | 4.90 GHz   | 4.00 GHz   | 12 MB                   |
| Intel(R) Xeon(R) E-2276G Processor                                         | Launched        | Q2'19   | 6   | 4.90 GHz   | 3.80 GHz   | 12 MB                   |
| Intel(R) Xeon(R) E-2276M Processor                                         | Launched        | Q2 '19  | 6   | 4.70 GHz   | 2.80 GHz   | 12 MB SmartCache        |
| Intel(R) Xeon(R) E-2286M Processor                                         | Launched        | Q2 '19  | 8   | 5.00 GHz   | 2.40 GHz   | 16 MB SmartCache        |
| Intel(R) Xeon(R) Gold 6244 Processor                                       | Launched        | Q2'19   | 8   | 4.40 GHz   | 3.60 GHz   | 24.75 MB                |
| Intel(R) Xeon(R) Gold 6240 Processor                                       | Launched        | Q2'19   | 18  | 3.90 GHz   | 2.60 GHz   | 24.75 MB                |
| Intel(R) Xeon(R) Gold 5218 Processor                                       | Launched        | Q2'19   | 16  | 3.90 GHz   | 2.30 GHz   | 22 MB                   |
| Intel(R) Xeon(R) Platinum 8253 Processor                                   | Launched        | Q2'19   | 16  | 3.00 GHz   | 2.20 GHz   | 22 MB                   |
| Intel(R) Xeon(R) Gold 6230 Processor                                       | Launched        | Q2'19   | 20  | 3.90 GHz   | 2.10 GHz   | 27.5 MB                 |
| Intel(R) Xeon(R) Gold 5222 Processor                                       | Launched        | Q2'19   | 4   | 3.90 GHz   | 3.80 GHz   | 16.5 MB                 |
| Intel(R) Xeon(R) Gold 6238T Processor                                      | Launched        | Q2'19   | 22  | 3.70 GHz   | 1.90 GHz   | 30.25 MB                |
| Intel(R) Xeon(R) Gold 6248 Processor                                       | Launched        | Q2'19   | 20  | 3.90 GHz   | 2.50 GHz   | 27.5 MB                 |
| Intel(R) Xeon(R) Platinum 8256 Processor                                   | Launched        | Q2'19   | 4   | 3.90 GHz   | 3.80 GHz   | 16.5 MB                 |
| Intel(R) Xeon(R) Gold 6252 Processor                                       | Launched        | Q2'19   | 24  | 3.70 GHz   | 2.10 GHz   | 35.75 MB                |
| Intel(R) Xeon(R) Platinum 8260 Processor                                   | Launched        | Q2'19   | 24  | 3.90 GHz   | 2.40 GHz   | 35.75 MB                |
| Intel(R) Xeon(R) Gold 6242 Processor                                       | Launched        | Q2'19   | 16  | 3.90 GHz   | 2.80 GHz   | 22 MB                   |
| Intel(R) Xeon(R) Platinum 8276 Processor                                   | Launched        | Q2'19   | 28  | 4.00 GHz   | 2.20 GHz   | 38.5 MB                 |
| Intel(R) Xeon(R) Platinum 8276M Processor                                  | Launched        | Q2'19   | 28  | 4.00 GHz   | 2.20 GHz   | 38.5 MB                 |
| Intel(R) Xeon(R) Gold 6240Y Processor                                      | Launched        | Q2'19   | 18  | 3.90 GHz   | 2.60 GHz   | 24.75 MB                |
| Intel(R) Xeon(R) Platinum 8276L Processor                                  | Launched        | Q2'19   | 28  | 4.00 GHz   | 2.20 GHz   | 38.5 MB                 |
| Intel(R) Xeon(R) Silver 4209T Processor                                    | Launched        | Q2'19   | 8   | 3.20 GHz   | 2.20 GHz   | 11 MB                   |
| Intel(R) Xeon(R) Platinum 8280L Processor                                  | Launched        | Q2'19   | 28  | 4.00 GHz   | 2.70 GHz   | 38.5 MB                 |
| Intel(R) Xeon(R) Gold 5217 Processor                                       | Launched        | Q2'19   | 8   | 3.70 GHz   | 3.00 GHz   | 11 MB                   |
| Intel(R) Xeon(R) Silver 4210 Processor                                     | Launched        | Q2'19   | 10  | 3.20 GHz   | 2.20 GHz   | 13.75 MB                |
| Intel(R) Xeon(R) Platinum 8260L Processor                                  | Launched        | Q2'19   | 24  | 3.90 GHz   | 2.40 GHz   | 35.75 MB                |
| Intel(R) Xeon(R) Gold 6238M Processor                                      | Launched        | Q2'19   | 22  | 3.70 GHz   | 2.10 GHz   | 30.25 MB                |
| Intel(R) Xeon(R) Platinum 8280M Processor                                  | Launched        | Q2'19   | 28  | 4.00 GHz   | 2.70 GHz   | 38.5 MB                 |
| Intel(R) Xeon(R) Gold 5218N Processor                                      | Launched        | Q2'19   | 16  | 3.70 GHz   | 2.30 GHz   | 22 MB                   |
| Intel(R) Xeon(R) Gold 6230N Processor                                      | Launched        | Q2'19   | 20  | 3.90 GHz   | 2.30 GHz   | 27.5 MB                 |
| Intel(R) Xeon(R) Silver 4214 Processor                                     | Launched        | Q2'19   | 12  | 3.20 GHz   | 2.20 GHz   | 16.5 MB                 |
| Intel(R) Xeon(R) Platinum 8260M Processor                                  | Launched        | Q2'19   | 24  | 3.90 GHz   | 2.40 GHz   | 35.75 MB                |
| Intel(R) Xeon(R) Gold 6226 Processor                                       | Launched        | Q2'19   | 12  | 3.70 GHz   | 2.70 GHz   | 19.25 MB                |
| Intel(R) Xeon(R) Gold 6254 Processor                                       | Launched        | Q2'19   | 18  | 4.00 GHz   | 3.10 GHz   | 24.75 MB                |
| Intel(R) Xeon(R) Silver 4214Y Processor                                    | Launched        | Q2'19   | 12  | 3.20 GHz   | 2.20 GHz   | 16.5 MB                 |
| Intel(R) Xeon(R) Gold 5220 Processor                                       | Launched        | Q2'19   | 18  | 3.90 GHz   | 2.20 GHz   | 24.75 MB                |
| Intel(R) Xeon(R) Platinum 8280 Processor                                   | Launched        | Q2'19   | 28  | 4.00 GHz   | 2.70 GHz   | 38.5 MB                 |
| Intel(R) Xeon(R) Gold 6238L Processor                                      | Launched        | Q2'19   | 22  | 3.70 GHz   | 2.10 GHz   | 30.25 MB                |
| Intel(R) Xeon(R) Platinum 8260Y Processor                                  | Launched        | Q2'19   | 24  | 3.90 GHz   | 2.40 GHz   | 35.75 MB                |
| Intel(R) Xeon(R) Gold 5218B Processor                                      | Launched        | Q2'19   | 16  | 3.90 GHz   | 2.30 GHz   | 22 MB                   |
| Intel(R) Xeon(R) Silver 4215 Processor                                     | Launched        | Q2'19   | 8   | 3.50 GHz   | 2.50 GHz   | 11 MB                   |
| Intel(R) Xeon(R) Gold 6240L Processor                                      | Launched        | Q2'19   | 18  | 3.90 GHz   | 2.60 GHz   | 24.75 MB                |
| Intel(R) Xeon(R) Gold 6238 Processor                                       | Launched        | Q2'19   | 22  | 3.70 GHz   | 2.10 GHz   | 30.25 MB                |
| Intel(R) Xeon(R) Platinum 8268 Processor                                   | Launched        | Q2'19   | 24  | 3.90 GHz   | 2.90 GHz   | 35.75 MB                |
| Intel(R) Xeon(R) Silver 4208 Processor                                     | Launched        | Q2'19   | 8   | 3.20 GHz   | 2.10 GHz   | 11 MB                   |
| Intel(R) Xeon(R) Gold 6240M Processor                                      | Launched        | Q2'19   | 18  | 3.90 GHz   | 2.60 GHz   | 24.75 MB                |
| Intel(R) Xeon(R) Gold 5220S Processor                                      | Launched        | Q2'19   | 18  | 3.90 GHz   | 2.70 GHz   | 24.75 MB                |
| Intel(R) Xeon(R) Platinum 8270 Processor                                   | Launched        | Q2'19   | 26  | 4.00 GHz   | 2.70 GHz   | 35.75 MB                |
| Intel(R) Xeon(R) Gold 5215 Processor                                       | Launched        | Q2'19   | 10  | 3.40 GHz   | 2.50 GHz   | 13.75 MB                |
| Intel(R) Xeon(R) Gold 6222V Processor                                      | Launched        | Q2'19   | 20  | 3.60 GHz   | 1.80 GHz   | 27.5 MB                 |
| Intel(R) Xeon(R) Gold 6252N Processor                                      | Launched        | Q2'19   | 24  | 3.60 GHz   | 2.30 GHz   | 35.75 MB                |
| Intel(R) Xeon(R) Gold 5215M Processor                                      | Launched        | Q2'19   | 10  | 3.40 GHz   | 2.50 GHz   | 13.75 MB                |
| Intel(R) Xeon(R) Gold 6230T Processor                                      | Launched        | Q2'19   | 20  | 3.90 GHz   | 2.10 GHz   | 27.5 MB                 |
| Intel(R) Xeon(R) Gold 6246 Processor                                       | Launched        | Q2'19   | 12  | 4.20 GHz   | 3.30 GHz   | 24.75 MB                |
| Intel(R) Xeon(R) Gold 5215L Processor                                      | Launched        | Q2'19   | 10  | 3.40 GHz   | 2.50 GHz   | 13.75 MB                |
| Intel(R) Xeon(R) Bronze 3204 Processor                                     | Launched        | Q2'19   | 6   | 1.90 GHz   | 1.90 GHz   | 8.25 MB                 |
| Intel(R) Xeon(R) Gold 5218T Processor                                      | Launched        | Q2'19   | 16  | 3.80 GHz   | 2.10 GHz   | 22 MB                   |
| Intel(R) Xeon(R) Gold 5220T Processor                                      | Launched        | Q2'19   | 18  | 3.90 GHz   | 1.90 GHz   | 24.75 MB                |
| Intel(R) Xeon(R) Silver 4216 Processor                                     | Launched        | Q2'19   | 16  | 3.20 GHz   | 2.10 GHz   | 22 MB                   |
| Intel(R) Xeon(R) Gold 6234 Processor                                       | Launched        | Q2'19   | 8   | 4.00 GHz   | 3.30 GHz   | 24.75 MB                |
| Intel(R) Xeon(R) Gold 6262V Processor                                      | Launched        | Q2'19   | 24  | 3.60 GHz   | 1.90 GHz   | 33 MB                   |
| Intel(R) Xeon(R) D-1602 Processor                                          | Launched        | Q2'19   | 2   | 3.20 GHz   | 2.50 GHz   | 3 MB                    |
| Intel(R) Xeon(R) D-1653N Processor                                         | Launched        | Q2'19   | 8   | 3.20 GHz   | 2.80 GHz   | 12 MB                   |
| Intel(R) Xeon(R) D-1622 Processor                                          | Launched        | Q2'19   | 4   | 3.20 GHz   | 2.60 GHz   | 6 MB                    |
| Intel(R) Xeon(R) D-1623N Processor                                         | Launched        | Q2'19   | 4   | 3.20 GHz   | 2.40 GHz   | 6 MB                    |
| Intel(R) Xeon(R) D-1627 Processor                                          | Launched        | Q2'19   | 4   | 3.20 GHz   | 2.90 GHz   | 6 MB                    |
| Intel(R) Xeon(R) D-1637 Processor                                          | Launched        | Q2'19   | 6   | 3.20 GHz   | 2.90 GHz   | 9 MB                    |
| Intel(R) Xeon(R) D-1633N Processor                                         | Launched        | Q2'19   | 6   | 3.20 GHz   | 2.50 GHz   | 9 MB                    |
| Intel(R) Xeon(R) D-1649N Processor                                         | Launched        | Q2'19   | 8   | 3.00 GHz   | 2.30 GHz   | 12 MB                   |
| Intel(R) Xeon(R) W-3175X Processor                                         | Launched        | Q4'18   | 28  | 3.80 GHz   | 3.10 GHz   | 38.5 MB                 |
| Intel(R) Xeon(R) E-2124G Processor                                         | Launched        | Q3'18   | 4   | 4.50 GHz   | 3.40 GHz   | 8 MB SmartCache         |
| Intel(R) Xeon(R) E-2146G Processor                                         | Launched        | Q3'18   | 6   | 4.50 GHz   | 3.50 GHz   | 12 MB SmartCache        |
| Intel(R) Xeon(R) E-2176G Processor                                         | Launched        | Q3'18   | 6   | 4.70 GHz   | 3.70 GHz   | 12 MB SmartCache        |
| Intel(R) Xeon(R) E-2136 Processor                                          | Launched        | Q3'18   | 6   | 4.50 GHz   | 3.30 GHz   | 12 MB SmartCache        |
| Intel(R) Xeon(R) E-2134 Processor                                          | Launched        | Q3'18   | 4   | 4.50 GHz   | 3.50 GHz   | 8 MB SmartCache         |
| Intel(R) Xeon(R) E-2144G Processor                                         | Launched        | Q3'18   | 4   | 4.50 GHz   | 3.60 GHz   | 8 MB SmartCache         |
| Intel(R) Xeon(R) E-2174G Processor                                         | Launched        | Q3'18   | 4   | 4.70 GHz   | 3.80 GHz   | 8 MB SmartCache         |
| Intel(R) Xeon(R) E-2186G Processor                                         | Launched        | Q3'18   | 6   | 4.70 GHz   | 3.80 GHz   | 12 MB SmartCache        |
| Intel(R) Xeon(R) E-2126G Processor                                         | Launched        | Q3'18   | 6   | 4.50 GHz   | 3.30 GHz   | 12 MB SmartCache        |
| Intel(R) Xeon(R) E-2124 Processor                                          | Launched        | Q3'18   | 4   | 4.30 GHz   | 3.30 GHz   | 8 MB SmartCache         |
| Intel(R) Xeon(R) E-2176M Processor                                         | Launched        | Q2'18   | 6   | 4.40 GHz   | 2.70 GHz   | 12 MB SmartCache        |
| Intel(R) Xeon(R) E-2186M Processor                                         | Launched        | Q2'18   | 6   | 4.80 GHz   | 2.90 GHz   | 12 MB SmartCache        |
| Intel(R) Xeon(R) D-2141I Processor                                         | Launched        | Q1'18   | 8   | 3.00 GHz   | 2.20 GHz   | 11 MB                   |
| Intel(R) Xeon(R) D-2177NT Processor                                        | Launched        | Q1'18   | 14  | 3.00 GHz   | 1.90 GHz   | 19 MB                   |
| Intel(R) Xeon(R) D-2161I Processor                                         | Launched        | Q1'18   | 12  | 3.00 GHz   | 2.20 GHz   | 16.5 MB L2              |
| Intel(R) Xeon(R) D-2143IT Processor                                        | Launched        | Q1'18   | 8   | 3.00 GHz   | 2.20 GHz   | 11 MB                   |
| Intel(R) Xeon(R) D-2146NT Processor                                        | Launched        | Q1'18   | 8   | 3.00 GHz   | 2.30 GHz   | 11 MB                   |
| Intel(R) Xeon(R) D-2145NT Processor                                        | Launched        | Q1'18   | 8   | 3.00 GHz   | 1.90 GHz   | 11 MB                   |
| Intel(R) Xeon(R) D-2123IT Processor                                        | Launched        | Q1'18   | 4   | 3.00 GHz   | 2.20 GHz   | 8 MB                    |
| Intel(R) Xeon(R) D-2173IT Processor                                        | Launched        | Q1'18   | 14  | 3.00 GHz   | 1.70 GHz   | 19 MB                   |
| Intel(R) Xeon(R) D-2187NT Processor                                        | Launched        | Q1'18   | 16  | 3.00 GHz   | 2.00 GHz   | 22 MB                   |
| Intel(R) Xeon(R) D-2142IT Processor                                        | Launched        | Q1'18   | 8   | 3.00 GHz   | 1.90 GHz   | 11 MB                   |
| Intel(R) Xeon(R) D-2163IT Processor                                        | Launched        | Q1'18   | 12  | 3.00 GHz   | 2.10 GHz   | 17 MB                   |
| Intel(R) Xeon(R) D-2183IT Processor                                        | Launched        | Q1'18   | 16  | 3.00 GHz   | 2.20 GHz   | 22 MB                   |
| Intel(R) Xeon(R) D-2166NT Processor                                        | Launched        | Q1'18   | 12  | 3.00 GHz   | 2.00 GHz   | 17 MB                   |
| Intel(R) Xeon(R) W-2175 Processor                                          | Launched        | Q4'17   | 14  | 4.30 GHz   | 2.50 GHz   | 19 MB                   |
| Intel(R) Xeon(R) W-2133 Processor                                          | Launched        | Q3'17   | 6   | 3.90 GHz   | 3.60 GHz   | 8.25 MB                 |
| Intel(R) Xeon(R) W-2155 Processor                                          | Launched        | Q3'17   | 10  | 4.50 GHz   | 3.30 GHz   | 13.75 MB                |
| Intel(R) Xeon(R) W-2123 Processor                                          | Launched        | Q3'17   | 4   | 3.90 GHz   | 3.60 GHz   | 8.25 MB                 |
| Intel(R) Xeon(R) W-2145 Processor                                          | Launched        | Q3'17   | 8   | 4.50 GHz   | 3.70 GHz   | 11 MB                   |
| Intel(R) Xeon(R) W-2125 Processor                                          | Launched        | Q3'17   | 4   | 4.50 GHz   | 4.00 GHz   | 8.25 MB                 |
| Intel(R) Xeon(R) W-2135 Processor                                          | Launched        | Q3'17   | 6   | 4.50 GHz   | 3.70 GHz   | 8.25 MB                 |
| Intel(R) Xeon(R) W-2195 Processor                                          | Launched        | Q3'17   | 18  | 4.30 GHz   | 2.30 GHz   | 24.75 MB                |
| Intel(R) Xeon(R) Processor E3-1285 v6                                      | Launched        | Q3'17   | 4   | 4.50 GHz   | 4.10 GHz   | 8 MB SmartCache         |
| Intel(R) Xeon(R) Processor D-1533N                                         | Launched        | Q3'17   | 6   | 2.70 GHz   | 2.10 GHz   | 9 MB                    |
| Intel(R) Xeon(R) Processor D-1513N                                         | Launched        | Q3'17   | 4   | 2.20 GHz   | 1.60 GHz   | 6 MB                    |
| Intel(R) Xeon(R) Processor D-1543N                                         | Launched        | Q3'17   | 8   | 2.50 GHz   | 1.90 GHz   | 12 MB                   |
| Intel(R) Xeon(R) Processor D-1523N                                         | Launched        | Q3'17   | 4   | 2.60 GHz   | 2.00 GHz   | 6 MB                    |
| Intel(R) Xeon(R) Processor D-1553N                                         | Launched        | Q3'17   | 8   | 2.70 GHz   | 2.30 GHz   | 12 MB                   |
| Intel(R) Xeon(R) Gold 5122 Processor                                       | Launched        | Q3'17   | 4   | 3.70 GHz   | 3.60 GHz   | 16.5 MB L3              |
| Intel(R) Xeon(R) Gold 6142 Processor                                       | Launched        | Q3'17   | 16  | 3.70 GHz   | 2.60 GHz   | 22 MB L3                |
| Intel(R) Xeon(R) Platinum 8153 Processor                                   | Launched        | Q3'17   | 16  | 2.80 GHz   | 2.00 GHz   | 22 MB L3                |
| Intel(R) Xeon(R) Platinum 8156 Processor                                   | Launched        | Q3'17   | 4   | 3.70 GHz   | 3.60 GHz   | 16.5 MB L3              |
| Intel(R) Xeon(R) Gold 6148 Processor                                       | Launched        | Q3'17   | 20  | 3.70 GHz   | 2.40 GHz   | 27.5 MB L3              |
| Intel(R) Xeon(R) Gold 5120T Processor                                      | Launched        | Q3'17   | 14  | 3.20 GHz   | 2.20 GHz   | 19.25 MB L3             |
| Intel(R) Xeon(R) Platinum 8158 Processor                                   | Launched        | Q3'17   | 12  | 3.70 GHz   | 3.00 GHz   | 24.75 MB L3             |
| Intel(R) Xeon(R) Platinum 8176 Processor                                   | Launched        | Q3'17   | 28  | 3.80 GHz   | 2.10 GHz   | 38.5 MB L3              |
| Intel(R) Xeon(R) Gold 6136 Processor                                       | Launched        | Q3'17   | 12  | 3.70 GHz   | 3.00 GHz   | 24.75 MB L3             |
| Intel(R) Xeon(R) Gold 6150 Processor                                       | Launched        | Q3'17   | 18  | 3.70 GHz   | 2.70 GHz   | 24.75 MB L3             |
| Intel(R) Xeon(R) Platinum 8160 Processor                                   | Launched        | Q3'17   | 24  | 3.70 GHz   | 2.10 GHz   | 33 MB L3                |
| Intel(R) Xeon(R) Silver 4116 Processor                                     | Launched        | Q3'17   | 12  | 3.00 GHz   | 2.10 GHz   | 16.5 MB L3              |
| Intel(R) Xeon(R) Gold 6152 Processor                                       | Launched        | Q3'17   | 22  | 3.70 GHz   | 2.10 GHz   | 30.25 MB L3             |
| Intel(R) Xeon(R) Gold 6130 Processor                                       | Launched        | Q3'17   | 16  | 3.70 GHz   | 2.10 GHz   | 22 MB L3                |
| Intel(R) Xeon(R) Gold 6128 Processor                                       | Launched        | Q3'17   | 6   | 3.70 GHz   | 3.40 GHz   | 19.25 MB L3             |
| Intel(R) Xeon(R) Gold 5118 Processor                                       | Launched        | Q3'17   | 12  | 3.20 GHz   | 2.30 GHz   | 16.5 MB L3              |
| Intel(R) Xeon(R) Platinum 8164 Processor                                   | Launched        | Q3'17   | 26  | 3.70 GHz   | 2.00 GHz   | 35.75 MB L3             |
| Intel(R) Xeon(R) Gold 6134 Processor                                       | Launched        | Q3'17   | 8   | 3.70 GHz   | 3.20 GHz   | 24.75 MB L3             |
| Intel(R) Xeon(R) Gold 6126 Processor                                       | Launched        | Q3'17   | 12  | 3.70 GHz   | 2.60 GHz   | 19.25 MB L3             |
| Intel(R) Xeon(R) Gold 5120 Processor                                       | Launched        | Q3'17   | 14  | 3.20 GHz   | 2.20 GHz   | 19.25 MB L3             |
| Intel(R) Xeon(R) Platinum 8168 Processor                                   | Launched        | Q3'17   | 24  | 3.70 GHz   | 2.70 GHz   | 33 MB L3                |
| Intel(R) Xeon(R) Gold 5115 Processor                                       | Launched        | Q3'17   | 10  | 3.20 GHz   | 2.40 GHz   | 13.75 MB L3             |
| Intel(R) Xeon(R) Gold 6154 Processor                                       | Launched        | Q3'17   | 18  | 3.70 GHz   | 3.00 GHz   | 24.75 MB L3             |
| Intel(R) Xeon(R) Gold 6140 Processor                                       | Launched        | Q3'17   | 18  | 3.70 GHz   | 2.30 GHz   | 24.75 MB L3             |
| Intel(R) Xeon(R) Platinum 8170 Processor                                   | Launched        | Q3'17   | 26  | 3.70 GHz   | 2.10 GHz   | 35.75 MB L3             |
| Intel(R) Xeon(R) Platinum 8180 Processor                                   | Launched        | Q3'17   | 28  | 3.80 GHz   | 2.50 GHz   | 38.5 MB L3              |
| Intel(R) Xeon(R) Silver 4109T Processor                                    | Launched        | Q3'17   | 8   | 3.00 GHz   | 2.00 GHz   | 11 MB L3                |
| Intel(R) Xeon(R) Bronze 3106 Processor                                     | Launched        | Q3'17   | 8   |            | 1.70 GHz   | 11 MB L3                |
| Intel(R) Xeon(R) Silver 4114 Processor                                     | Launched        | Q3'17   | 10  | 3.00 GHz   | 2.20 GHz   | 13.75 MB L3             |
| Intel(R) Xeon(R) Gold 6132 Processor                                       | Launched        | Q3'17   | 14  | 3.70 GHz   | 2.60 GHz   | 19.25 MB L3             |
| Intel(R) Xeon(R) Silver 4112 Processor                                     | Launched        | Q3'17   | 4   | 3.00 GHz   | 2.60 GHz   | 8.25 MB L3              |
| Intel(R) Xeon(R) Gold 6138T Processor                                      | Launched        | Q3'17   | 20  | 3.70 GHz   | 2.00 GHz   | 27.5 MB L3              |
| Intel(R) Xeon(R) Platinum 8160T Processor                                  | Launched        | Q3'17   | 24  | 3.70 GHz   | 2.10 GHz   | 33 MB L3                |
| Intel(R) Xeon(R) Silver 4108 Processor                                     | Launched        | Q3'17   | 8   | 3.00 GHz   | 1.80 GHz   | 11 MB L3                |
| Intel(R) Xeon(R) Gold 6142F Processor                                      | Launched        | Q3'17   | 16  | 3.70 GHz   | 2.60 GHz   | 22 MB L3                |
| Intel(R) Xeon(R) Gold 6130T Processor                                      | Launched        | Q3'17   | 16  | 3.70 GHz   | 2.10 GHz   | 22 MB L3                |
| Intel(R) Xeon(R) Gold 6138F Processor                                      | Launched        | Q3'17   | 20  | 3.70 GHz   | 2.00 GHz   | 27.5 MB L3              |
| Intel(R) Xeon(R) Bronze 3104 Processor                                     | Launched        | Q3'17   | 6   |            | 1.70 GHz   | 8.25 MB L3              |
| Intel(R) Xeon(R) Platinum 8160F Processor                                  | Launched        | Q3'17   | 24  | 3.70 GHz   | 2.10 GHz   | 33 MB L3                |
| Intel(R) Xeon(R) Silver 4110 Processor                                     | Launched        | Q3'17   | 8   | 3.00 GHz   | 2.10 GHz   | 11 MB L3                |
| Intel(R) Xeon(R) Gold 6130F Processor                                      | Launched        | Q3'17   | 16  | 3.70 GHz   | 2.10 GHz   | 22 MB L3                |
| Intel(R) Xeon(R) Gold 6126T Processor                                      | Launched        | Q3'17   | 12  | 3.70 GHz   | 2.60 GHz   | 19.25 MB L3             |
| Intel(R) Xeon(R) Gold 6126F Processor                                      | Launched        | Q3'17   | 12  | 3.70 GHz   | 2.60 GHz   | 19.25 MB L3             |
| Intel(R) Xeon(R) Gold 6148F Processor                                      | Launched        | Q3'17   | 20  | 3.70 GHz   | 2.40 GHz   | 27.5 MB L3              |
| Intel(R) Xeon(R) Gold 6146 Processor                                       | Launched        | Q3'17   | 12  | 4.20 GHz   | 3.20 GHz   | 24.75 MB L3             |
| Intel(R) Xeon(R) Gold 6144 Processor                                       | Launched        | Q3'17   | 8   | 4.20 GHz   | 3.50 GHz   | 24.75 MB L3             |
| Intel(R) Xeon(R) Platinum 8176F Processor                                  | Launched        | Q3'17   | 28  | 3.80 GHz   | 2.10 GHz   | 38.5 MB L3              |
| Intel(R) Xeon(R) Silver 4116T Processor                                    | Launched        | Q3'17   | 12  | 3.00 GHz   | 2.10 GHz   | 16.5 MB L3              |
| Intel(R) Xeon(R) Silver 4114T Processor                                    | Launched        | Q3'17   | 10  | 3.00 GHz   | 2.20 GHz   | 13.75 MB L3             |
| Intel(R) Xeon(R) Gold 5119T Processor                                      | Launched        | Q3'17   | 14  | 3.20 GHz   | 1.90 GHz   | 19.25 MB L3             |
| Intel(R) Xeon(R) Processor E3-1501M v6                                     | Launched        | Q2'17   | 4   | 3.60 GHz   | 2.90 GHz   | 6 MB                    |
| Intel(R) Xeon(R) Processor E3-1501L v6                                     | Launched        | Q2'17   | 4   | 2.90 GHz   | 2.10 GHz   | 6 MB                    |
| Intel(R) Xeon(R) Processor E3-1230 v6                                      | Launched        | Q1'17   | 4   | 3.90 GHz   | 3.50 GHz   | 8 MB SmartCache         |
| Intel(R) Xeon(R) Processor E3-1280 v6                                      | Launched        | Q1'17   | 4   | 4.20 GHz   | 3.90 GHz   | 8 MB SmartCache         |
| Intel(R) Xeon(R) Processor E3-1225 v6                                      | Launched        | Q1'17   | 4   | 3.70 GHz   | 3.30 GHz   | 8 MB SmartCache         |
| Intel(R) Xeon(R) Processor E3-1240 v6                                      | Launched        | Q1'17   | 4   | 4.10 GHz   | 3.70 GHz   | 8 MB SmartCache         |
| Intel(R) Xeon(R) Processor E3-1275 v6                                      | Launched        | Q1'17   | 4   | 4.20 GHz   | 3.80 GHz   | 8 MB SmartCache         |
| Intel(R) Xeon(R) Processor E3-1220 v6                                      | Launched        | Q1'17   | 4   | 3.50 GHz   | 3.00 GHz   | 8 MB SmartCache         |
| Intel(R) Xeon(R) Processor E3-1270 v6                                      | Launched        | Q1'17   | 4   | 4.20 GHz   | 3.80 GHz   | 8 MB SmartCache         |
| Intel(R) Xeon(R) Processor E3-1245 v6                                      | Launched        | Q1'17   | 4   | 4.10 GHz   | 3.70 GHz   | 8 MB SmartCache         |
| Intel(R) Xeon(R) Processor E7-8894 v4                                      | Launched        | Q1'17   | 24  | 3.40 GHz   | 2.40 GHz   | 60 MB                   |
| Intel(R) Xeon(R) Processor E3-1535M v6                                     | Launched        | Q1'17   | 4   | 4.20 GHz   | 3.10 GHz   | 8 MB                    |
| Intel(R) Xeon(R) Processor E3-1505M v6                                     | Launched        | Q1'17   | 4   | 4.00 GHz   | 3.00 GHz   | 8 MB                    |
| Intel(R) Xeon(R) Processor E3-1505L v6                                     | Launched        | Q1'17   | 4   | 3.00 GHz   | 2.20 GHz   | 8 MB                    |
| Intel(R) Xeon(R) Processor E5-2699A v4                                     | Launched        | 04'16   | 22  | 3.60 GHz   | 2.40 GHz   | 55 MB                   |
| Intel(R) Xeon(R) Processor E5-2699R v4                                     | Launched        | 04'16   | 22  | 3.60 GHz   | 2.20 GHz   | 55 MB                   |
| Intel(R) Xeon(R) Processor E5-4627 v4                                      | Launched        | Q2'16   | 10  | 3.20 GHz   | 2.60 GHz   | 25 MB                   |
| Intel(R) Xeon(R) Processor E5-4610 v4                                      | Launched        | Q2'16   | 10  | 1.80 GHz   | 1.80 GHz   | 25 MB                   |
| Intel(R) Xeon(R) Processor E5-4620 v4                                      | Launched        | Q2'16   | 10  | 2.60 GHz   | 2.10 GHz   | 25 MB                   |
| Intel(R) Xeon(R) Processor E5-4628L v4                                     | Launched        | Q2'16   | 14  | 2.20 GHz   | 1.80 GHz   | 35 MB                   |
| Intel(R) Xeon(R) Processor E5-4660 v4                                      | Launched        | Q2'16   | 16  | 3.00 GHz   | 2.20 GHz   | 40 MB                   |
| Intel(R) Xeon(R) Processor E5-4640 v4                                      | Launched        | Q2'16   | 12  | 2.60 GHz   | 2.10 GHz   | 30 MB                   |
| Intel(R) Xeon(R) Processor E5-4669 v4                                      | Launched        | Q2'16   | 22  | 3.00 GHz   | 2.20 GHz   | 55 MB                   |
| Intel(R) Xeon(R) Processor E5-4667 v4                                      | Launched        | Q2'16   | 18  | 3.00 GHz   | 2.20 GHz   | 45 MB                   |
| Intel(R) Xeon(R) Processor E5-4655 v4                                      | Launched        | Q2'16   | 8   | 3.20 GHz   | 2.50 GHz   | 30 MB                   |
| Intel(R) Xeon(R) Processor E5-4650 v4                                      | Launched        | Q2'16   | 14  | 2.80 GHz   | 2.20 GHz   | 35 MB                   |
| Intel(R) Xeon(R) Processor E5-1660 v4                                      | Launched        | Q2'16   | 8   | 3.80 GHz   | 3.20 GHz   | 20 MB                   |
| Intel(R) Xeon(R) Processor E5-1630 v4                                      | Launched        | Q2'16   | 4   | 4.00 GHz   | 3.70 GHz   | 10 MB                   |
| Intel(R) Xeon(R) Processor E5-1620 v4                                      | Launched        | Q2'16   | 4   | 3.80 GHz   | 3.50 GHz   | 10 MB                   |
| Intel(R) Xeon(R) Processor E5-1680 v4                                      | Launched        | Q2'16   | 8   | 4.00 GHz   | 3.40 GHz   | 20 MB                   |
| Intel(R) Xeon(R) Processor E5-1650 v4                                      | Launched        | Q2'16   | 6   | 4.00 GHz   | 3.60 GHz   | 15 MB                   |
| Intel(R) Xeon(R) Processor E7-8890 v4                                      | Launched        | Q2'16   | 24  | 3.40 GHz   | 2.20 GHz   | 60 MB                   |
| Intel(R) Xeon(R) Processor E7-8893 v4                                      | Launched        | Q2'16   | 4   | 3.50 GHz   | 3.20 GHz   | 60 MB                   |
| Intel(R) Xeon(R) Processor E7-8880 v4                                      | Launched        | Q2'16   | 22  | 3.30 GHz   | 2.20 GHz   | 55 MB                   |
| Intel(R) Xeon(R) Processor E7-4830 v4                                      | Launched        | Q2'16   | 14  | 2.80 GHz   | 2.00 GHz   | 35 MB                   |
| Intel(R) Xeon(R) Processor E7-8860 v4                                      | Launched        | Q2'16   | 18  | 3.20 GHz   | 2.20 GHz   | 45 MB                   |
| Intel(R) Xeon(R) Processor E7-4809 v4                                      | Launched        | Q2'16   | 8   |            | 2.10 GHz   | 20 MB                   |
| Intel(R) Xeon(R) Processor E7-8870 v4                                      | Launched        | Q2'16   | 20  | 3.00 GHz   | 2.10 GHz   | 50 MB                   |
| Intel(R) Xeon(R) Processor E7-4820 v4                                      | Launched        | Q2'16   | 10  |            | 2.00 GHz   | 25 MB                   |
| Intel(R) Xeon(R) Processor E7-8891 v4                                      | Launched        | Q2'16   | 10  | 3.50 GHz   | 2.80 GHz   | 60 MB                   |
| Intel(R) Xeon(R) Processor E7-8867 v4                                      | Launched        | Q2'16   | 18  | 3.30 GHz   | 2.40 GHz   | 45 MB                   |
| Intel(R) Xeon(R) Processor E7-4850 v4                                      | Launched        | Q2'16   | 16  | 2.80 GHz   | 2.10 GHz   | 40 MB                   |
| Intel(R) Xeon(R) Processor E3-1565L v5                                     | Launched        | Q2'16   | 4   | 3.50 GHz   | 2.50 GHz   | 8 MB                    |
| Intel(R) Xeon(R) Processor E3-1585L v5                                     | Launched        | Q2'16   | 4   | 3.70 GHz   | 3.00 GHz   | 8 MB                    |
| Intel(R) Xeon(R) Processor E3-1585 v5                                      | Launched        | Q2'16   | 4   | 3.90 GHz   | 3.50 GHz   | 8 MB                    |
| Intel(R) Xeon(R) Processor E3-1558L v5                                     | Launched        | Q2'16   | 4   | 3.30 GHz   | 1.90 GHz   | 8 MB                    |
| Intel(R) Xeon(R) Processor E3-1578L v5                                     | Launched        | Q2'16   | 4   | 3.40 GHz   | 2.00 GHz   | 8 MB                    |
| Intel(R) Xeon(R) Processor D-1539                                          | Launched        | Q2'16   | 8   | 2.20 GHz   | 1.60 GHz   | 12 MB                   |
| Intel(R) Xeon(R) Processor D-1529                                          | Launched        | Q2'16   | 4   | 1.30 GHz   | 1.30 GHz   | 6 MB                    |
| Intel(R) Xeon(R) Processor D-1559                                          | Launched        | Q2'16   | 12  | 2.10 GHz   | 1.50 GHz   | 18 MB                   |
| Intel(R) Xeon(R) Processor E5-2687W v4                                     | Launched        | Q1'16   | 12  | 3.50 GHz   | 3.00 GHz   | 30 MB SmartCache        |
| Intel(R) Xeon(R) Processor E5-2695 v4                                      | Launched        | Q1'16   | 18  | 3.30 GHz   | 2.10 GHz   | 45 MB SmartCache        |
| Intel(R) Xeon(R) Processor E5-2690 v4                                      | Launched        | Q1'16   | 14  | 3.50 GHz   | 2.60 GHz   | 35 MB SmartCache        |
| Intel(R) Xeon(R) Processor E5-2699 v4                                      | Launched        | Q1'16   | 22  | 3.60 GHz   | 2.20 GHz   | 55 MB SmartCache        |
| Intel(R) Xeon(R) Processor E5-2650L v4                                     | Launched        | Q1'16   | 14  | 2.50 GHz   | 1.70 GHz   | 35 MB SmartCache        |
| Intel(R) Xeon(R) Processor E5-2658 v4                                      | Launched        | Q1'16   | 14  | 2.80 GHz   | 2.30 GHz   | 35 MB SmartCache        |
| Intel(R) Xeon(R) Processor E5-2698 v4                                      | Launched        | Q1'16   | 20  | 3.60 GHz   | 2.20 GHz   | 50 MB SmartCache        |
| Intel(R) Xeon(R) Processor E5-2660 v4                                      | Launched        | Q1'16   | 14  | 3.20 GHz   | 2.00 GHz   | 35 MB                   |
| Intel(R) Xeon(R) Processor E5-2680 v4                                      | Launched        | Q1'16   | 14  | 3.30 GHz   | 2.40 GHz   | 35 MB SmartCache        |
| Intel(R) Xeon(R) Processor E5-2697 v4                                      | Launched        | Q1'16   | 18  | 3.60 GHz   | 2.30 GHz   | 45 MB SmartCache        |
| Intel(R) Xeon(R) Processor E5-2683 v4                                      | Launched        | Q1'16   | 16  | 3.00 GHz   | 2.10 GHz   | 40 MB SmartCache        |
| Intel(R) Xeon(R) Processor E5-2628L v4                                     | Launched        | Q1'16   | 12  | 2.40 GHz   | 1.90 GHz   | 30 MB SmartCache        |
| Intel(R) Xeon(R) Processor E5-2650 v4                                      | Launched        | Q1'16   | 12  | 2.90 GHz   | 2.20 GHz   | 30 MB SmartCache        |
| Intel(R) Xeon(R) Processor E5-2697A v4                                     | Launched        | Q1'16   | 16  | 3.60 GHz   | 2.60 GHz   | 40 MB SmartCache        |
| Intel(R) Xeon(R) Processor E5-2648L v4                                     | Launched        | Q1'16   | 14  | 2.50 GHz   | 1.80 GHz   | 35 MB SmartCache        |
| Intel(R) Xeon(R) Processor E5-2620 v4                                      | Launched        | Q1'16   | 8   | 3.00 GHz   | 2.10 GHz   | 20 MB SmartCache        |
| Intel(R) Xeon(R) Processor E5-2630L v4                                     | Launched        | Q1'16   | 10  | 2.90 GHz   | 1.80 GHz   | 25 MB SmartCache        |
| Intel(R) Xeon(R) Processor E5-2608L v4                                     | Launched        | Q1'16   | 8   | 1.70 GHz   | 1.60 GHz   | 20 MB SmartCache        |
| Intel(R) Xeon(R) Processor E5-2667 v4                                      | Launched        | Q1'16   | 8   | 3.60 GHz   | 3.20 GHz   | 25 MB SmartCache        |
| Intel(R) Xeon(R) Processor E5-2643 v4                                      | Launched        | Q1'16   | 6   | 3.70 GHz   | 3.40 GHz   | 20 MB SmartCache        |
| Intel(R) Xeon(R) Processor E5-2623 v4                                      | Launched        | Q1'16   | 4   | 3.20 GHz   | 2.60 GHz   | 10 MB SmartCache        |
| Intel(R) Xeon(R) Processor E5-2609 v4                                      | Launched        | Q1'16   | 8   |            | 1.70 GHz   | 20 MB SmartCache        |
| Intel(R) Xeon(R) Processor E5-2630 v4                                      | Launched        | Q1'16   | 10  | 3.10 GHz   | 2.20 GHz   | 25 MB SmartCache        |
| Intel(R) Xeon(R) Processor E5-2618L v4                                     | Launched        | Q1'16   | 10  | 3.20 GHz   | 2.20 GHz   | 25 MB SmartCache        |
| Intel(R) Xeon(R) Processor E5-2637 v4                                      | Launched        | Q1'16   | 4   | 3.70 GHz   | 3.50 GHz   | 15 MB SmartCache        |
| Intel(R) Xeon(R) Processor E5-2603 v4                                      | Launched        | Q1'16   | 6   |            | 1.70 GHz   | 15 MB SmartCache        |
| Intel(R) Xeon(R) Processor E5-2640 v4                                      | Launched        | Q1'16   | 10  | 3.40 GHz   | 2.40 GHz   | 25 MB SmartCache        |
| Intel(R) Xeon(R) Processor D-1557                                          | Launched        | Q1'16   | 12  | 2.10 GHz   | 1.50 GHz   | 18 MB                   |
| Intel(R) Xeon(R) Processor D-1567                                          | Launched        | Q1'16   | 12  | 2.70 GHz   | 2.10 GHz   | 18 MB                   |
| Intel(R) Xeon(R) Processor D-1577                                          | Launched        | Q1'16   | 16  | 2.10 GHz   | 1.30 GHz   | 24 MB                   |
| Intel(R) Xeon(R) Processor D-1571                                          | Launched        | Q1'16   | 16  | 2.10 GHz   | 1.30 GHz   | 24 MB                   |
| Intel(R) Xeon(R) Processor E3-1545M v5                                     | Launched        | Q1'16   | 4   | 3.80 GHz   | 2.90 GHz   | 8 MB SmartCache         |
| Intel(R) Xeon(R) Processor E3-1575M v5                                     | Launched        | Q1'16   | 4   | 3.90 GHz   | 3.00 GHz   | 8 MB SmartCache         |
| Intel(R) Xeon(R) Processor E3-1515M v5                                     | Launched        | Q1'16   | 4   | 3.70 GHz   | 2.80 GHz   | 8 MB SmartCache         |
| Intel(R) Xeon(R) Processor D-1528                                          | Launched        | Q4'15   | 6   | 2.50 GHz   | 1.90 GHz   | 9 MB                    |
| Intel(R) Xeon(R) Processor D-1541                                          | Launched        | Q4'15   | 8   | 2.70 GHz   | 2.10 GHz   | 12 MB                   |
| Intel(R) Xeon(R) Processor D-1518                                          | Launched        | Q4'15   | 4   | 2.20 GHz   | 2.20 GHz   | 6 MB                    |
| Intel(R) Xeon(R) Processor D-1521                                          | Launched        | Q4'15   | 4   | 2.70 GHz   | 2.40 GHz   | 6 MB                    |
| Intel(R) Xeon(R) Processor D-1531                                          | Launched        | Q4'15   | 6   | 2.70 GHz   | 2.20 GHz   | 9 MB                    |
| Intel(R) Xeon(R) Processor D-1548                                          | Launched        | Q4'15   | 8   | 2.60 GHz   | 2.00 GHz   | 12 MB                   |
| Intel(R) Xeon(R) Processor D-1527                                          | Launched        | Q4'15   | 4   | 2.70 GHz   | 2.20 GHz   | 6 MB                    |
| Intel(R) Xeon(R) Processor D-1537                                          | Launched        | Q4'15   | 8   | 2.30 GHz   | 1.70 GHz   | 12 MB                   |
| Intel(R) Xeon(R) Processor E3-1240L v5                                     | Launched        | Q4'15   | 4   | 3.20 GHz   | 2.10 GHz   | 8 MB SmartCache         |
| Intel(R) Xeon(R) Processor E3-1235L v5                                     | Launched        | Q4'15   | 4   | 3.00 GHz   | 2.00 GHz   | 8 MB SmartCache         |
| Intel(R) Xeon(R) Processor E3-1280 v5                                      | Launched        | Q4'15   | 4   | 4.00 GHz   | 3.70 GHz   | 8 MB SmartCache         |
| Intel(R) Xeon(R) Processor E3-1220 v5                                      | Launched        | Q4'15   | 4   | 3.50 GHz   | 3.00 GHz   | 8 MB SmartCache         |
| Intel(R) Xeon(R) Processor E3-1230 v5                                      | Launched        | Q4'15   | 4   | 3.80 GHz   | 3.40 GHz   | 8 MB SmartCache         |
| Intel(R) Xeon(R) Processor E3-1245 v5                                      | Launched        | Q4'15   | 4   | 3.90 GHz   | 3.50 GHz   | 8 MB SmartCache         |
| Intel(R) Xeon(R) Processor E3-1270 v5                                      | Launched        | Q4'15   | 4   | 4.00 GHz   | 3.60 GHz   | 8 MB SmartCache         |
| Intel(R) Xeon(R) Processor E3-1225 v5                                      | Launched        | Q4'15   | 4   | 3.70 GHz   | 3.30 GHz   | 8 MB SmartCache         |
| Intel(R) Xeon(R) Processor E3-1260L v5                                     | Launched        | Q4'15   | 4   | 3.90 GHz   | 2.90 GHz   | 8 MB SmartCache         |
| Intel(R) Xeon(R) Processor E3-1240 v5                                      | Launched        | Q4'15   | 4   | 3.90 GHz   | 3.50 GHz   | 8 MB SmartCache         |
| Intel(R) Xeon(R) Processor E3-1275 v5                                      | Launched        | Q4'15   | 4   | 4.00 GHz   | 3.60 GHz   | 8 MB SmartCache         |
| Intel(R) Xeon(R) Processor E3-1268L v5                                     | Launched        | Q4'15   | 4   | 3.40 GHz   | 2.40 GHz   | 8 MB SmartCache         |
| Intel(R) Xeon(R) Processor E3-1505L v5                                     | Launched        | Q4'15   | 4   | 2.80 GHz   | 2.00 GHz   | 8 MB SmartCache         |
| Intel(R) Xeon(R) Processor E3-1505M v5                                     | Launched        | Q3'15   | 4   | 3.70 GHz   | 2.80 GHz   | 8 MB SmartCache         |
| Intel(R) Xeon(R) Processor E3-1535M v5                                     | Launched        | Q3'15   | 4   | 3.80 GHz   | 2.90 GHz   | 8 MB SmartCache         |
| Intel(R) Xeon(R) Processor E3-1265L v4                                     | Launched        | Q2'15   | 4   | 3.30 GHz   | 2.30 GHz   | 6 MB                    |
| Intel(R) Xeon(R) Processor E3-1285L v4                                     | Launched        | Q2'15   | 4   | 3.80 GHz   | 3.40 GHz   | 6 MB                    |
| Intel(R) Xeon(R) Processor E3-1258L v4                                     | Launched        | Q2'15   | 4   | 3.20 GHz   | 1.80 GHz   | 6 MB SmartCache         |
| Intel(R) Xeon(R) Processor E3-1278L v4                                     | Launched        | Q2'15   | 4   | 3.30 GHz   | 2.00 GHz   | 6 MB SmartCache         |
| Intel(R) Xeon(R) Processor E3-1285 v4                                      | Launched        | Q2'15   | 4   | 3.80 GHz   | 3.50 GHz   | 6 MB                    |
| Intel(R) Xeon(R) Processor E5-4640 v3                                      | Launched        | Q2'15   | 12  | 2.60 GHz   | 1.90 GHz   | 30 MB                   |
| Intel(R) Xeon(R) Processor E5-4648 v3                                      | Launched        | Q2'15   | 12  | 2.20 GHz   | 1.70 GHz   | 30 MB                   |
| Intel(R) Xeon(R) Processor E5-4650 v3                                      | Launched        | Q2'15   | 12  | 2.80 GHz   | 2.10 GHz   | 30 MB                   |
| Intel(R) Xeon(R) Processor E5-4655 v3                                      | Launched        | Q2'15   | 6   | 3.20 GHz   | 2.90 GHz   | 30 MB                   |
| Intel(R) Xeon(R) Processor E5-4660 v3                                      | Launched        | Q2'15   | 14  | 2.90 GHz   | 2.10 GHz   | 35 MB                   |
| Intel(R) Xeon(R) Processor E5-4667 v3                                      | Launched        | Q2'15   | 16  | 2.90 GHz   | 2.00 GHz   | 40 MB                   |
| Intel(R) Xeon(R) Processor E5-4669 v3                                      | Launched        | Q2'15   | 18  | 2.90 GHz   | 2.10 GHz   | 45 MB                   |
| Intel(R) Xeon(R) Processor E5-4610 v3                                      | Launched        | Q2'15   | 10  | 1.70 GHz   | 1.70 GHz   | 25 MB                   |
| Intel(R) Xeon(R) Processor E5-4620 v3                                      | Launched        | Q2'15   | 10  | 2.60 GHz   | 2.00 GHz   | 25 MB                   |
| Intel(R) Xeon(R) Processor E5-4627 v3                                      | Launched        | Q2'15   | 10  | 3.20 GHz   | 2.60 GHz   | 25 MB                   |
| Intel(R) Xeon(R) Processor E7-8860 v3                                      | Launched        | Q2'15   | 16  | 3.20 GHz   | 2.20 GHz   | 40 MB Last Level Cache  |
| Intel(R) Xeon(R) Processor E7-8867 v3                                      | Launched        | Q2'15   | 16  | 3.30 GHz   | 2.50 GHz   | 45 MB                   |
| Intel(R) Xeon(R) Processor E7-8870 v3                                      | Launched        | Q2'15   | 18  | 2.90 GHz   | 2.10 GHz   | 45 MB Last Level Cache  |
| Intel(R) Xeon(R) Processor E7-8880 v3                                      | Launched        | Q2'15   | 18  | 3.10 GHz   | 2.30 GHz   | 45 MB Last Level Cache  |
| Intel(R) Xeon(R) Processor E7-8880L v3                                     | Launched        | Q2'15   | 18  | 2.80 GHz   | 2.00 GHz   | 45 MB Last Level Cache  |
| Intel(R) Xeon(R) Processor E7-4809 v3                                      | Launched        | Q2'15   | 8   |            | 2.00 GHz   | 20 MB Last Level Cache  |
| Intel(R) Xeon(R) Processor E7-8890 v3                                      | Launched        | Q2'15   | 18  | 3.30 GHz   | 2.50 GHz   | 45 MB Last Level Cache  |
| Intel(R) Xeon(R) Processor E7-4820 v3                                      | Launched        | Q2'15   | 10  |            | 1.90 GHz   | 25 MB Last Level Cache  |
| Intel(R) Xeon(R) Processor E7-8891 v3                                      | Launched        | Q2'15   | 10  | 3.50 GHz   | 2.80 GHz   | 45 MB Last Level Cache  |
| Intel(R) Xeon(R) Processor E7-4830 v3                                      | Launched        | Q2'15   | 12  | 2.70 GHz   | 2.10 GHz   | 30 MB Last Level Cache  |
| Intel(R) Xeon(R) Processor E7-4850 v3                                      | Launched        | Q2'15   | 14  | 2.80 GHz   | 2.20 GHz   | 35 MB Last Level Cache  |
| Intel(R) Xeon(R) Processor E7-8893 v3                                      | Launched        | Q2'15   | 4   | 3.50 GHz   | 3.20 GHz   | 45 MB Last Level Cache  |
| Intel(R) Xeon(R) Processor D-1520                                          | Launched        | Q1'15   | 4   | 2.60 GHz   | 2.20 GHz   | 6 MB                    |
| Intel(R) Xeon(R) Processor D-1540                                          | Launched        | Q1'15   | 8   | 2.60 GHz   | 2.00 GHz   | 12 MB                   |
| Intel(R) Xeon(R) Processor E5-2658A v3                                     | Launched        | Q1'15   | 12  | 2.90 GHz   | 2.20 GHz   | 30 MB SmartCache        |
| Intel(R) Xeon(R) Processor E5-2695 v3                                      | Launched        | Q3'14   | 14  | 3.30 GHz   | 2.30 GHz   | 35 MB SmartCache        |
| Intel(R) Xeon(R) Processor E5-2697 v3                                      | Launched        | Q3'14   | 14  | 3.60 GHz   | 2.60 GHz   | 35 MB SmartCache        |
| Intel(R) Xeon(R) Processor E5-2698 v3                                      | Announced       | Q3'14   | 16  | 3.60 GHz   | 2.30 GHz   | 40 MB SmartCache        |
| Intel(R) Xeon(R) Processor E5-2699 v3                                      | Announced       | Q3'14   | 18  | 3.60 GHz   | 2.30 GHz   | 45 MB SmartCache        |
| Intel(R) Xeon(R) Processor E5-2683 v3                                      | Launched        | Q3'14   | 14  | 3.00 GHz   | 2.00 GHz   | 35 MB SmartCache        |
| Intel(R) Xeon(R) Processor E5-2628L v3                                     | Launched        | Q3'14   | 10  | 2.50 GHz   | 2.00 GHz   | 25 MB SmartCache        |
| Intel(R) Xeon(R) Processor E5-2690 v3                                      | Launched        | Q3'14   | 12  | 3.50 GHz   | 2.60 GHz   | 30 MB SmartCache        |
| Intel(R) Xeon(R) Processor E5-2650 v3                                      | Launched        | Q3'14   | 10  | 3.00 GHz   | 2.30 GHz   | 25 MB SmartCache        |
| Intel(R) Xeon(R) Processor E5-2660 v3                                      | Launched        | Q3'14   | 10  | 3.30 GHz   | 2.60 GHz   | 25 MB SmartCache        |
| Intel(R) Xeon(R) Processor E5-2658 v3                                      | Launched        | Q3'14   | 12  | 2.90 GHz   | 2.20 GHz   | 30 MB SmartCache        |
| Intel(R) Xeon(R) Processor E5-2609 v3                                      | Discontinued    | Q3'14   | 6   |            | 1.90 GHz   | 15 MB SmartCache        |
| Intel(R) Xeon(R) Processor E5-2680 v3                                      | Launched        | Q3'14   | 12  | 3.30 GHz   | 2.50 GHz   | 30 MB SmartCache        |
| Intel(R) Xeon(R) Processor E5-2670 v3                                      | Launched        | Q3'14   | 12  | 3.10 GHz   | 2.30 GHz   | 30 MB SmartCache        |
| Intel(R) Xeon(R) Processor E5-1630 v3                                      | Launched        | Q3'14   | 4   | 3.80 GHz   | 3.70 GHz   | 10 MB SmartCache        |
| Intel(R) Xeon(R) Processor E5-1650 v3                                      | Launched        | Q3'14   | 6   | 3.80 GHz   | 3.50 GHz   | 15 MB SmartCache        |
| Intel(R) Xeon(R) Processor E5-2687W v3                                     | Launched        | Q3'14   | 10  | 3.50 GHz   | 3.10 GHz   | 25 MB SmartCache        |
| Intel(R) Xeon(R) Processor E5-2643 v3                                      | Launched        | Q3'14   | 6   | 3.70 GHz   | 3.40 GHz   | 20 MB SmartCache        |
| Intel(R) Xeon(R) Processor E5-1660 v3                                      | Launched        | Q3'14   | 8   | 3.50 GHz   | 3.00 GHz   | 20 MB SmartCache        |
| Intel(R) Xeon(R) Processor E5-2648L v3                                     | Launched        | Q3'14   | 12  | 2.50 GHz   | 1.80 GHz   | 30 MB SmartCache        |
| Intel(R) Xeon(R) Processor E5-1680 v3                                      | Launched        | Q3'14   | 8   | 3.80 GHz   | 3.20 GHz   | 20 MB SmartCache        |
| Intel(R) Xeon(R) Processor E5-2618L v3                                     | Launched        | Q3'14   | 8   | 3.40 GHz   | 2.30 GHz   | 20 MB SmartCache        |
| Intel(R) Xeon(R) Processor E5-2650L v3                                     | Launched        | Q3'14   | 12  | 2.50 GHz   | 1.80 GHz   | 30 MB SmartCache        |
| Intel(R) Xeon(R) Processor E5-2620 v3                                      | Launched        | Q3'14   | 6   | 3.20 GHz   | 2.40 GHz   | 15 MB SmartCache        |
| Intel(R) Xeon(R) Processor E5-2623 v3                                      | Launched        | Q3'14   | 4   | 3.50 GHz   | 3.00 GHz   | 10 MB SmartCache        |
| Intel(R) Xeon(R) Processor E5-1620 v3                                      | Launched        | Q3'14   | 4   | 3.60 GHz   | 3.50 GHz   | 10 MB SmartCache        |
| Intel(R) Xeon(R) Processor E5-2630 v3                                      | Launched        | Q3'14   | 8   | 3.20 GHz   | 2.40 GHz   | 20 MB SmartCache        |
| Intel(R) Xeon(R) Processor E5-2630L v3                                     | Launched        | Q3'14   | 8   | 2.90 GHz   | 1.80 GHz   | 20 MB SmartCache        |
| Intel(R) Xeon(R) Processor E5-2637 v3                                      | Launched        | Q3'14   | 4   | 3.70 GHz   | 3.50 GHz   | 15 MB SmartCache        |
| Intel(R) Xeon(R) Processor E5-2640 v3                                      | Launched        | Q3'14   | 8   | 3.40 GHz   | 2.60 GHz   | 20 MB SmartCache        |
| Intel(R) Xeon(R) Processor E5-2603 v3                                      | Launched        | Q3'14   | 6   |            | 1.60 GHz   | 15 MB SmartCache        |
| Intel(R) Xeon(R) Processor E5-2667 v3                                      | Launched        | Q3'14   | 8   | 3.60 GHz   | 3.20 GHz   | 20 MB SmartCache        |
| Intel(R) Xeon(R) Processor E5-2608L v3                                     | Launched        | Q3'14   | 6   |            | 2.00 GHz   | 15 MB SmartCache        |
| Intel(R) Xeon(R) Processor E3-1275L v3                                     | Discontinued    | Q2'14   | 4   | 3.90 GHz   | 2.70 GHz   | 8 MB SmartCache         |
| Intel(R) Xeon(R) Processor E3-1241 v3                                      | Launched        | Q2'14   | 4   | 3.90 GHz   | 3.50 GHz   | 8 MB SmartCache         |
| Intel(R) Xeon(R) Processor E3-1231 v3                                      | Launched        | Q2'14   | 4   | 3.80 GHz   | 3.40 GHz   | 8 MB SmartCache         |
| Intel(R) Xeon(R) Processor E3-1240L v3                                     | Launched        | Q2'14   | 4   | 3.00 GHz   | 2.00 GHz   | 8 MB SmartCache         |
| Intel(R) Xeon(R) Processor E3-1286 v3                                      | Discontinued    | Q2'14   | 4   | 4.10 GHz   | 3.70 GHz   | 8 MB SmartCache         |
| Intel(R) Xeon(R) Processor E3-1286L v3                                     | Discontinued    | Q2'14   | 4   | 4.00 GHz   | 3.20 GHz   | 8 MB SmartCache         |
| Intel(R) Xeon(R) Processor E3-1276 v3                                      | Launched        | Q2'14   | 4   | 4.00 GHz   | 3.60 GHz   | 8 MB SmartCache         |
| Intel(R) Xeon(R) Processor E3-1281 v3                                      | Launched        | Q2'14   | 4   | 4.10 GHz   | 3.70 GHz   | 8 MB SmartCache         |
| Intel(R) Xeon(R) Processor E3-1246 v3                                      | Launched        | Q2'14   | 4   | 3.90 GHz   | 3.50 GHz   | 8 MB SmartCache         |
| Intel(R) Xeon(R) Processor E3-1271 v3                                      | Launched        | Q2'14   | 4   | 4.00 GHz   | 3.60 GHz   | 8 MB SmartCache         |
| Intel(R) Xeon(R) Processor E3-1226 v3                                      | Launched        | Q2'14   | 4   | 3.70 GHz   | 3.30 GHz   | 8 MB SmartCache         |
| Intel(R) Xeon(R) Processor E3-1220L v3                                     | Launched        | Q3'13   | 2   | 1.50 GHz   | 1.10 GHz   | 4 MB SmartCache         |
| Intel(R) Xeon(R) Processor E3-1220 v3                                      | Launched        | Q2'13   | 4   | 3.50 GHz   | 3.10 GHz   | 8 MB SmartCache         |
| Intel(R) Xeon(R) Processor E3-1230L v3                                     | Launched        | Q2'13   | 4   | 2.80 GHz   | 1.80 GHz   | 8 MB SmartCache         |
| Intel(R) Xeon(R) Processor E3-1230 v3                                      | Discontinued    | Q2'13   | 4   | 3.70 GHz   | 3.30 GHz   | 8 MB SmartCache         |
| Intel(R) Xeon(R) Processor E3-1240 v3                                      | Discontinued    | Q2'13   | 4   | 3.80 GHz   | 3.40 GHz   | 8 MB SmartCache         |
| Intel(R) Xeon(R) Processor E3-1270 v3                                      | Discontinued    | Q2'13   | 4   | 3.90 GHz   | 3.50 GHz   | 8 MB SmartCache         |
| Intel(R) Xeon(R) Processor E3-1280 v3                                      | Discontinued    | Q2'13   | 4   | 4.00 GHz   | 3.60 GHz   | 8 MB SmartCache         |
| Intel(R) Xeon(R) Processor E3-1268L v3                                     | Launched        | Q2'13   | 4   | 3.30 GHz   | 2.30 GHz   | 8 MB SmartCache         |
| Intel(R) Xeon(R) Processor E3-1225 v3                                      | Launched        | Q2'13   | 4   | 3.60 GHz   | 3.20 GHz   | 8 MB SmartCache         |
| Intel(R) Xeon(R) Processor E3-1245 v3                                      | Discontinued    | Q2'13   | 4   | 3.80 GHz   | 3.40 GHz   | 8 MB SmartCache         |
| Intel(R) Xeon(R) Processor E3-1265L v3                                     | Launched        | Q2'13   | 4   | 3.70 GHz   | 2.50 GHz   | 8 MB SmartCache         |
| Intel(R) Xeon(R) Processor E3-1275 v3                                      | Launched        | Q2'13   | 4   | 3.90 GHz   | 3.50 GHz   | 8 MB SmartCache         |
| Intel(R) Xeon(R) Processor E3-1285 v3                                      | Discontinued    | Q2'13   | 4   | 4.00 GHz   | 3.60 GHz   | 8 MB SmartCache         |
| Intel(R) Xeon(R) Processor E3-1285L v3                                     | Discontinued    | Q2'13   | 4   | 3.90 GHz   | 3.10 GHz   | 8 MB SmartCache         |