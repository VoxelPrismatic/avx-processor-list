# AVX Processor List
All Data from the Intel(R) list is taken from https://ark.intel.com

This repo only exists to make searching easier for users

# QUICK LINK
- [`INTEL`](https://gitlab.com/VoxelPrismatic/avx-processor-list/tree/master/INTEL)
- - [`AVX`](https://gitlab.com/VoxelPrismatic/avx-processor-list/blob/master/INTEL/AVX.md) `- Regular`
- - - [`CELERON `](https://gitlab.com/VoxelPrismatic/avx-processor-list/blob/master/INTEL/AVX.md#intel-celeron)
- - - [`CORE    `](https://gitlab.com/VoxelPrismatic/avx-processor-list/blob/master/INTEL/AVX.md#intel-core) `- i3, i5, i7, ...`
- - - [`PENTIUM `](https://gitlab.com/VoxelPrismatic/avx-processor-list/blob/master/INTEL/AVX.md#intel-pentium)
- - - [`XEON    `](https://gitlab.com/VoxelPrismatic/avx-processor-list/blob/master/INTEL/AVX.md#intel-xeon) `- Gold, Silver, Bronze, ...`
- - [`AVX2`](https://gitlab.com/VoxelPrismatic/avx-processor-list/blob/master/INTEL/AVX2.md) `- Version 2`
- - - [`CELERON `](https://gitlab.com/VoxelPrismatic/avx-processor-list/blob/master/INTEL/AVX2.md#intel-celeron)
- - - [`CORE    `](https://gitlab.com/VoxelPrismatic/avx-processor-list/blob/master/INTEL/AVX2.md#intel-core) `- i3, i5, i7, ...`
- - - [`PENTIUM `](https://gitlab.com/VoxelPrismatic/avx-processor-list/blob/master/INTEL/AVX2.md#intel-pentium)
- - - [`XEON    `](https://gitlab.com/VoxelPrismatic/avx-processor-list/blob/master/INTEL/AVX2.md#intel-xeon) `- Gold, Silver, Bronze, ...`
- - [`AVX-512`](https://gitlab.com/VoxelPrismatic/avx-processor-list/blob/master/INTEL/AVX-512.md)
- - - [`XEON    `](https://gitlab.com/VoxelPrismatic/avx-processor-list/blob/master/INTEL/AVX-512.md#intel-xeon) `- Gold, Silver, Bronze, ...`
- - - [`CORE    `](https://gitlab.com/VoxelPrismatic/avx-processor-list/blob/master/INTEL/AVX-512.md#intel-core) `- i3, i5, i7, ...`
- - - [`XEON PHI`](https://gitlab.com/VoxelPrismatic/avx-processor-list/blob/master/INTEL/AVX-512.md#intel-xeon-phi)
- [`AMD`]() `- SORRY, AMD IS STILL BEING FOUND`
- - `AMD does not list any of their processors in the way Intel does, this will take time to resolve`