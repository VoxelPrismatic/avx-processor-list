# Cluster F*ck of processors
| PRODUCT NAME                                                           | RELEASE | COR | TURBO MAX  | BASE FREQ  | CACHE                   |
| ---------------------------------------------------------------------- | ------- | --- | ---------- | ---------- | ----------------------- |
| Liverpool [PS4] | Q4, 2013 | 2x4 | 1.6 GHz | 1.6 GHz | 2x2 MB |
| Durango [XB1] | Q4, 2013 | 2x4 | 1.75 GHz | 1.75 GHz | 2x2 MB |
| Emdmondton [XB1S] | Q2, 2016 | 2x4 | 1.75 GHz | 1.75 GHz | 2x2 MB |
| Liverpool? [PS4S] | Q4, 2016 | 2x4 | 1.6 GHz | 1.6 GHz | 2x2 MB |
| Neo [PS4 Pro] | Q4, 2016 | 2x4 | 2.13 GHz | 2.13 GHz | 2x2 MB |
| Scorpio [XB1X] | Q4, 2017 | 2x4 | 2.3 GHz | 2.3 GHz | 2x2 MB |
| Athlom 5370 | ? | 4 | 2.2 GHz | 2.2 GHz | 2 MB |