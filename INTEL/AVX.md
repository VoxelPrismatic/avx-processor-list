# INTEL CELERON
| PRODUCT NAME                                       | STATUS          | RELEASE | COR | BASE FREQ  | CACHE      | TDP               |
| -------------------------------------------------- | --------------- | ------- | --- | ---------- | ---------- | ----------------- |
| Intel(R) Celeron(R) Processor 725C                     | Launched        | Q2'12   | 1   | 1.30 GHz   | 1.5 MB L3  | 10 W              |

# INTEL CORE
| PRODUCT NAME                                       | STATUS          | RELEASE | COR | TURBO MAX  | BASE FREQ  | CACHE             |
| -------------------------------------------------- | --------------- | ------- | --- | ---------- | ---------- | ----------------- |
| Intel(R) Core[TM] i3-4340TE Processor                   | Launched        | Q2'14   | 2   |            | 2.60 GHz   | 4 MB SmartCache   |
| Intel(R) Core[TM] i7-4960X Processor Extreme Edition    | Discontinued    | Q3'13   | 6   | 4.00 GHz   | 3.60 GHz   | 15 MB SmartCache  |
| Intel(R) Core[TM] i7-4930K Processor                    | Discontinued    | Q3'13   | 6   | 3.90 GHz   | 3.40 GHz   | 12 MB SmartCache  |
| Intel(R) Core[TM] i7-4820K Processor                    | Discontinued    | Q3'13   | 4   | 3.90 GHz   | 3.70 GHz   | 10 MB SmartCache  |
| Intel(R) Core[TM] i3-3115C Processor                    | Launched        | Q3'13   | 2   |            | 2.50 GHz   | 4 MB L3           |
| Intel(R) Core[TM] i5-3340 Processor                     | Discontinued    | Q3'13   | 4   | 3.30 GHz   | 3.10 GHz   | 6 MB SmartCache   |
| Intel(R) Core[TM] i5-3340S Processor                    | Discontinued    | Q3'13   | 4   | 3.30 GHz   | 2.80 GHz   | 6 MB SmartCache   |
| Intel(R) Core[TM] i3-4330TE Processor                   | Launched        | Q3'13   | 2   |            | 2.40 GHz   | 4 MB SmartCache   |
| Intel(R) Core[TM] i3-3250 Processor                     | Discontinued    | Q2'13   | 2   |            | 3.50 GHz   | 3 MB SmartCache   |
| Intel(R) Core[TM] i3-3250T Processor                    | Discontinued    | Q2'13   | 2   |            | 3.00 GHz   | 3 MB SmartCache   |
| Intel(R) Core[TM] i3-3245 Processor                     | Discontinued    | Q2'13   | 2   |            | 3.40 GHz   | 3 MB SmartCache   |
| Intel(R) Core[TM] i7-3540M Processor                    | Launched        | Q1'13   | 2   | 3.70 GHz   | 3.00 GHz   | 4 MB SmartCache   |
| Intel(R) Core[TM] i5-3380M Processor                    | Launched        | Q1'13   | 2   | 3.60 GHz   | 2.90 GHz   | 3 MB SmartCache   |
| Intel(R) Core[TM] i5-3340M Processor                    | Launched        | Q1'13   | 2   | 3.40 GHz   | 2.70 GHz   | 3 MB SmartCache   |
| Intel(R) Core[TM] i3-3210 Processor                     | Discontinued    | Q1'13   | 2   |            | 3.20 GHz   | 3 MB SmartCache   |
| Intel(R) Core[TM] i7-3687U Processor                    | Launched        | Q1'13   | 2   | 3.30 GHz   | 2.10 GHz   | 4 MB SmartCache   |
| Intel(R) Core[TM] i5-3437U Processor                    | Launched        | Q1'13   | 2   | 2.90 GHz   | 1.90 GHz   | 3 MB SmartCache   |
| Intel(R) Core[TM] i5-3230M Processor                    | Launched        | Q1'13   | 2   | 3.20 GHz   | 2.60 GHz   | 3 MB SmartCache   |
| Intel(R) Core[TM] i3-3227U Processor                    | Launched        | Q1'13   | 2   |            | 1.90 GHz   | 3 MB SmartCache   |
| Intel(R) Core[TM] i3-3130M Processor                    | Launched        | Q1'13   | 2   |            | 2.60 GHz   | 3 MB SmartCache   |
| Intel(R) Core[TM] i5-3230M Processor                    | Launched        | Q1'13   | 2   | 3.20 GHz   | 2.60 GHz   | 3 MB SmartCache   |
| Intel(R) Core[TM] i7-3537U Processor                    | Launched        | Q1'13   | 2   | 3.10 GHz   | 2.00 GHz   | 4 MB SmartCache   |
| Intel(R) Core[TM] i5-3337U Processor                    | Launched        | Q1'13   | 2   | 2.70 GHz   | 1.80 GHz   | 3 MB SmartCache   |
| Intel(R) Core[TM] i3-3229Y Processor                    | Launched        | Q1'13   | 2   |            | 1.40 GHz   | 3 MB              |
| Intel(R) Core[TM] i5-3339Y Processor                    | Launched        | Q1'13   | 2   | 2.00 GHz   | 1.50 GHz   | 3 MB              |
| Intel(R) Core[TM] i5-3439Y Processor                    | Launched        | Q1'13   | 2   | 2.30 GHz   | 1.50 GHz   | 3 MB              |
| Intel(R) Core[TM] i7-3689Y Processor                    | Launched        | Q1'13   | 2   | 2.60 GHz   | 1.50 GHz   | 4 MB              |
| Intel(R) Core[TM] i3-2348M Processor                    | Launched        | Q1'13   | 2   |            | 2.30 GHz   | 3 MB SmartCache   |
| Intel(R) Core[TM] i3-2375M Processor                    | Launched        | Q1'13   | 2   |            | 1.50 GHz   | 3 MB SmartCache   |
| Intel(R) Core[TM] i7-3970X Processor Extreme Edition    | Discontinued    | Q4'12   | 6   | 4.00 GHz   | 3.50 GHz   | 15 MB SmartCache  |
| Intel(R) Core[TM] i7-3840QM Processor                   | Launched        | Q3'12   | 4   | 3.80 GHz   | 2.80 GHz   | 8 MB SmartCache   |
| Intel(R) Core[TM] i7-3740QM Processor                   | Launched        | Q3'12   | 4   | 3.70 GHz   | 2.70 GHz   | 6 MB SmartCache   |
| Intel(R) Core[TM] i7-3630QM Processor                   | Launched        | Q3'12   | 4   | 3.40 GHz   | 2.40 GHz   | 6 MB SmartCache   |
| Intel(R) Core[TM] i7-3632QM Processor                   | Launched        | Q3'12   | 4   | 3.20 GHz   | 2.20 GHz   | 6 MB SmartCache   |
| Intel(R) Core[TM] i7-3635QM Processor                   | Launched        | Q3'12   | 4   | 3.40 GHz   | 2.40 GHz   | 6 MB SmartCache   |
| Intel(R) Core[TM] i7-3940XM Processor Extreme Edition   | Launched        | Q3'12   | 4   | 3.90 GHz   | 3.00 GHz   | 8 MB SmartCache   |
| Intel(R) Core[TM] i3-3120M Processor                    | Launched        | Q3'12   | 2   |            | 2.50 GHz   | 3 MB SmartCache   |
| Intel(R) Core[TM] i7-3632QM Processor                   | Launched        | Q3'12   | 4   | 3.20 GHz   | 2.20 GHz   | 6 MB SmartCache   |
| Intel(R) Core[TM] i3-3240 Processor                     | Discontinued    | Q3'12   | 2   |            | 3.40 GHz   | 3 MB SmartCache   |
| Intel(R) Core[TM] i5-3330 Processor                     | Discontinued    | Q3'12   | 4   | 3.20 GHz   | 3.00 GHz   | 6 MB SmartCache   |
| Intel(R) Core[TM] i5-3330S Processor                    | Discontinued    | Q3'12   | 4   | 3.20 GHz   | 2.70 GHz   | 6 MB SmartCache   |
| Intel(R) Core[TM] i3-3225 Processor                     | Discontinued    | Q3'12   | 2   |            | 3.30 GHz   | 3 MB SmartCache   |
| Intel(R) Core[TM] i3-3220 Processor                     | Discontinued    | Q3'12   | 2   |            | 3.30 GHz   | 3 MB SmartCache   |
| Intel(R) Core[TM] i3-3220T Processor                    | Discontinued    | Q3'12   | 2   |            | 2.80 GHz   | 3 MB SmartCache   |
| Intel(R) Core[TM] i3-3240T Processor                    | Discontinued    | Q3'12   | 2   |            | 2.90 GHz   | 3 MB SmartCache   |
| Intel(R) Core[TM] i5-3350P Processor                    | Discontinued    | Q3'12   | 4   | 3.30 GHz   | 3.10 GHz   | 6 MB SmartCache   |
| Intel(R) Core[TM] i3-2365M Processor                    | Launched        | Q3'12   | 2   |            | 1.40 GHz   | 3 MB SmartCache   |
| Intel(R) Core[TM] i3-2328M Processor                    | Launched        | Q3'12   | 2   |            | 2.20 GHz   | 3 MB              |
| Intel(R) Core[TM] i3-3120ME Processor                   | Launched        | Q3'12   | 2   |            | 2.40 GHz   | 3 MB SmartCache   |
| Intel(R) Core[TM] i3-3217UE Processor                   | Launched        | Q3'12   | 2   |            | 1.60 GHz   | 3 MB SmartCache   |
| Intel(R) Core[TM] i3-2377M Processor                    | Launched        | Q2'12   | 2   |            | 1.50 GHz   | 3 MB SmartCache   |
| Intel(R) Core[TM] i3-3217U Processor                    | Launched        | Q2'12   | 2   |            | 1.80 GHz   | 3 MB SmartCache   |
| Intel(R) Core[TM] i3-3110M Processor                    | Launched        | Q2'12   | 2   |            | 2.40 GHz   | 3 MB SmartCache   |
| Intel(R) Core[TM] i7-3555LE Processor                   | Launched        | Q2'12   | 2   | 3.20 GHz   | 2.50 GHz   | 4 MB SmartCache   |
| Intel(R) Core[TM] i7-3517UE Processor                   | Launched        | Q2'12   | 2   | 2.80 GHz   | 1.70 GHz   | 4 MB SmartCache   |
| Intel(R) Core[TM] i5-3610ME Processor                   | Launched        | Q2'12   | 2   | 3.30 GHz   | 2.70 GHz   | 3 MB SmartCache   |
| Intel(R) Core[TM] i5-3427U Processor                    | Launched        | Q2'12   | 2   | 2.80 GHz   | 1.80 GHz   | 3 MB SmartCache   |
| Intel(R) Core[TM] i7-3520M Processor                    | Launched        | Q2'12   | 2   | 3.60 GHz   | 2.90 GHz   | 4 MB SmartCache   |
| Intel(R) Core[TM] i5-3360M Processor                    | Launched        | Q2'12   | 2   | 3.50 GHz   | 2.80 GHz   | 3 MB SmartCache   |
| Intel(R) Core[TM] i5-3320M Processor                    | Launched        | Q2'12   | 2   | 3.30 GHz   | 2.60 GHz   | 3 MB SmartCache   |
| Intel(R) Core[TM] i5-3317U Processor                    | Launched        | Q2'12   | 2   | 2.60 GHz   | 1.70 GHz   | 3 MB SmartCache   |
| Intel(R) Core[TM] i7-3667U Processor                    | Launched        | Q2'12   | 2   | 3.20 GHz   | 2.00 GHz   | 4 MB SmartCache   |
| Intel(R) Core[TM] i5-3475S Processor                    | Discontinued    | Q2'12   | 4   | 3.60 GHz   | 2.90 GHz   | 6 MB SmartCache   |
| Intel(R) Core[TM] i5-3210M Processor                    | Launched        | Q2'12   | 2   | 3.10 GHz   | 2.50 GHz   | 3 MB SmartCache   |
| Intel(R) Core[TM] i5-3570S Processor                    | Discontinued    | Q2'12   | 4   | 3.80 GHz   | 3.10 GHz   | 6 MB SmartCache   |
| Intel(R) Core[TM] i5-3570 Processor                     | Discontinued    | Q2'12   | 4   | 3.80 GHz   | 3.40 GHz   | 6 MB SmartCache   |
| Intel(R) Core[TM] i5-3470T Processor                    | Discontinued    | Q2'12   | 2   | 3.60 GHz   | 2.90 GHz   | 3 MB SmartCache   |
| Intel(R) Core[TM] i7-3517U Processor                    | Launched        | Q2'12   | 2   | 3.00 GHz   | 1.90 GHz   | 4 MB SmartCache   |
| Intel(R) Core[TM] i5-3210M Processor                    | Launched        | Q2'12   | 2   | 3.10 GHz   | 2.50 GHz   | 3 MB SmartCache   |
| Intel(R) Core[TM] i5-3470S Processor                    | Discontinued    | Q2'12   | 4   | 3.60 GHz   | 2.90 GHz   | 6 MB SmartCache   |
| Intel(R) Core[TM] i5-3470 Processor                     | Discontinued    | Q2'12   | 4   | 3.60 GHz   | 3.20 GHz   | 6 MB SmartCache   |
| Intel(R) Core[TM] i3-2115C Processor                    | Launched        | Q2'12   | 2   |            | 2.00 GHz   | 3 MB SmartCache   |
| Intel(R) Core[TM] i7-3920XM Processor Extreme Edition   | Launched        | Q2'12   | 4   | 3.80 GHz   | 2.90 GHz   | 8 MB SmartCache   |
| Intel(R) Core[TM] i7-3820QM Processor                   | Launched        | Q2'12   | 4   | 3.70 GHz   | 2.70 GHz   | 8 MB SmartCache   |
| Intel(R) Core[TM] i7-3720QM Processor                   | Launched        | Q2'12   | 4   | 3.60 GHz   | 2.60 GHz   | 6 MB SmartCache   |
| Intel(R) Core[TM] i7-3770S Processor                    | Discontinued    | Q2'12   | 4   | 3.90 GHz   | 3.10 GHz   | 8 MB SmartCache   |
| Intel(R) Core[TM] i7-3770T Processor                    | Discontinued    | Q2'12   | 4   | 3.70 GHz   | 2.50 GHz   | 8 MB SmartCache   |
| Intel(R) Core[TM] i7-3770 Processor                     | Discontinued    | Q2'12   | 4   | 3.90 GHz   | 3.40 GHz   | 8 MB SmartCache   |
| Intel(R) Core[TM] i7-3610QM Processor                   | Launched        | Q2'12   | 4   | 3.30 GHz   | 2.30 GHz   | 6 MB SmartCache   |
| Intel(R) Core[TM] i7-3615QE Processor                   | Launched        | Q2'12   | 4   | 3.30 GHz   | 2.30 GHz   | 6 MB SmartCache   |
| Intel(R) Core[TM] i5-3550 Processor                     | Discontinued    | Q2'12   | 4   | 3.70 GHz   | 3.30 GHz   | 6 MB SmartCache   |
| Intel(R) Core[TM] i7-3615QM Processor                   | Launched        | Q2'12   | 4   | 3.30 GHz   | 2.30 GHz   | 6 MB SmartCache   |
| Intel(R) Core[TM] i7-3612QE Processor                   | Launched        | Q2'12   | 4   | 3.10 GHz   | 2.10 GHz   | 6 MB SmartCache   |
| Intel(R) Core[TM] i7-3612QM Processor                   | Launched        | Q2'12   | 4   | 3.10 GHz   | 2.10 GHz   | 6 MB SmartCache   |
| Intel(R) Core[TM] i7-3610QE Processor                   | Launched        | Q2'12   | 4   | 3.30 GHz   | 2.30 GHz   | 6 MB SmartCache   |
| Intel(R) Core[TM] i5-3550S Processor                    | Launched        | Q2'12   | 4   | 3.70 GHz   | 3.00 GHz   | 6 MB SmartCache   |
| Intel(R) Core[TM] i5-3450 Processor                     | Discontinued    | Q2'12   | 4   | 3.50 GHz   | 3.10 GHz   | 6 MB SmartCache   |
| Intel(R) Core[TM] i5-3570K Processor                    | Discontinued    | Q2'12   | 4   | 3.80 GHz   | 3.40 GHz   | 6 MB SmartCache   |
| Intel(R) Core[TM] i5-3450S Processor                    | Discontinued    | Q2'12   | 4   | 3.50 GHz   | 2.80 GHz   | 6 MB SmartCache   |
| Intel(R) Core[TM] i5-3570T Processor                    | Discontinued    | Q2'12   | 4   | 3.30 GHz   | 2.30 GHz   | 6 MB SmartCache   |
| Intel(R) Core[TM] i7-3770K Processor                    | Discontinued    | Q2'12   | 4   | 3.90 GHz   | 3.50 GHz   | 8 MB SmartCache   |
| Intel(R) Core[TM] i7-3612QM Processor                   | Launched        | Q2'12   | 4   | 3.10 GHz   | 2.10 GHz   | 6 MB SmartCache   |
| Intel(R) Core[TM] i7-3820 Processor                     | Discontinued    | Q1'12   | 4   | 3.80 GHz   | 3.60 GHz   | 10 MB SmartCache  |
| Intel(R) Core[TM] i5-2450P Processor                    | Discontinued    | Q1'12   | 4   | 3.50 GHz   | 3.20 GHz   | 6 MB SmartCache   |
| Intel(R) Core[TM] i5-2380P Processor                    | Discontinued    | Q1'12   | 4   | 3.40 GHz   | 3.10 GHz   | 6 MB SmartCache   |
| Intel(R) Core[TM] i5-2550K Processor                    | Discontinued    | Q1'12   | 4   | 3.80 GHz   | 3.40 GHz   | 6 MB SmartCache   |
| Intel(R) Core[TM] i5-2450M Processor                    | Launched        | Q1'12   | 2   | 3.10 GHz   | 2.50 GHz   | 3 MB SmartCache   |
| Intel(R) Core[TM] i3-2370M Processor                    | Launched        | Q1'12   | 2   |            | 2.40 GHz   | 3 MB SmartCache   |
| Intel(R) Core[TM] i7-3960X Processor Extreme Edition    | Discontinued    | Q4'11   | 6   | 3.90 GHz   | 3.30 GHz   | 15 MB SmartCache  |
| Intel(R) Core[TM] i7-3930K Processor                    | Discontinued    | Q4'11   | 6   | 3.80 GHz   | 3.20 GHz   | 12 MB SmartCache  |
| Intel(R) Core[TM] i7-2700K Processor                    | Discontinued    | Q4'11   | 4   | 3.90 GHz   | 3.50 GHz   | 8 MB SmartCache   |
| Intel(R) Core[TM] i3-2350M Processor                    | Launched        | Q4'11   | 2   |            | 2.30 GHz   | 3 MB SmartCache   |
| Intel(R) Core[TM] i5-2430M Processor                    | Launched        | Q4'11   | 2   | 3.00 GHz   | 2.40 GHz   | 3 MB SmartCache   |
| Intel(R) Core[TM] i7-2670QM Processor                   | Launched        | Q4'11   | 4   | 3.10 GHz   | 2.20 GHz   | 6 MB SmartCache   |
| Intel(R) Core[TM] i7-2675QM Processor                   | Launched        | Q4'11   | 4   | 3.10 GHz   | 2.20 GHz   | 6 MB SmartCache   |
| Intel(R) Core[TM] i3-2367M Processor                    | Launched        | Q4'11   | 2   |            | 1.40 GHz   | 3 MB SmartCache   |
| Intel(R) Core[TM] i5-2435M Processor                    | Launched        | Q4'11   | 2   | 3.00 GHz   | 2.40 GHz   | 3 MB SmartCache   |
| Intel(R) Core[TM] i5-2320 Processor                     | Discontinued    | Q3'11   | 4   | 3.30 GHz   | 3.00 GHz   | 6 MB SmartCache   |
| Intel(R) Core[TM] i3-2120T Processor                    | Discontinued    | Q3'11   | 2   |            | 2.60 GHz   | 3 MB SmartCache   |
| Intel(R) Core[TM] i3-2130 Processor                     | Discontinued    | Q3'11   | 2   |            | 3.40 GHz   | 3 MB SmartCache   |
| Intel(R) Core[TM] i7-2860QM Processor                   | Launched        | Q4'11   | 4   | 3.60 GHz   | 2.50 GHz   | 8 MB SmartCache   |
| Intel(R) Core[TM] i7-2960XM Processor Extreme Edition   | Launched        | Q4'11   | 4   | 3.70 GHz   | 2.70 GHz   | 8 MB SmartCache   |
| Intel(R) Core[TM] i7-2640M Processor                    | Launched        | Q4'11   | 2   | 3.50 GHz   | 2.80 GHz   | 4 MB SmartCache   |
| Intel(R) Core[TM] i7-2760QM Processor                   | Launched        | Q4'11   | 4   | 3.50 GHz   | 2.40 GHz   | 6 MB SmartCache   |
| Intel(R) Core[TM] i3-2125 Processor                     | Discontinued    | Q3'11   | 2   |            | 3.30 GHz   | 3 MB SmartCache   |
| Intel(R) Core[TM] i3-2330E Processor                    | Discontinued    | Q2'11   | 2   |            | 2.20 GHz   | 3 MB SmartCache   |
| Intel(R) Core[TM] i3-2330M Processor                    | Launched        | Q2'11   | 2   |            | 2.20 GHz   | 3 MB L3           |
| Intel(R) Core[TM] i3-2357M Processor                    | Launched        | Q2'11   | 2   |            | 1.30 GHz   | 3 MB L3           |
| Intel(R) Core[TM] i7-2677M Processor                    | Launched        | Q2'11   | 2   | 2.90 GHz   | 1.80 GHz   | 4 MB L3           |
| Intel(R) Core[TM] i7-2637M Processor                    | Launched        | Q2'11   | 2   | 2.80 GHz   | 1.70 GHz   | 4 MB L3           |
| Intel(R) Core[TM] i5-2557M Processor                    | Launched        | Q2'11   | 2   | 2.70 GHz   | 1.70 GHz   | 3 MB L3           |
| Intel(R) Core[TM] i3-2340UE Processor                   | Discontinued    | Q2'11   | 2   |            | 1.30 GHz   | 3 MB SmartCache   |
| Intel(R) Core[TM] i5-2467M Processor                    | Launched        | Q2'11   | 2   | 2.30 GHz   | 1.60 GHz   | 3 MB L3           |
| Intel(R) Core[TM] i5-2310 Processor                     | Discontinued    | Q2'11   | 4   | 3.20 GHz   | 2.90 GHz   | 6 MB SmartCache   |
| Intel(R) Core[TM] i5-2405S Processor                    | Discontinued    | Q2'11   | 4   | 3.30 GHz   | 2.50 GHz   | 6 MB SmartCache   |
| Intel(R) Core[TM] i3-2312M Processor                    | Launched        | Q2'11   | 2   |            | 2.10 GHz   | 3 MB SmartCache   |
| Intel(R) Core[TM] i3-2102 Processor                     | Discontinued    | Q2'11   | 2   |            | 3.10 GHz   | 3 MB SmartCache   |
| Intel(R) Core[TM] i3-2105 Processor                     | Discontinued    | Q2'11   | 2   |            | 3.10 GHz   | 3 MB SmartCache   |
| Intel(R) Core[TM] i7-2655LE Processor                   | Discontinued    | Q1'11   | 2   | 2.90 GHz   | 2.20 GHz   | 4 MB SmartCache   |
| Intel(R) Core[TM] i7-2610UE Processor                   | Discontinued    | Q1'11   | 2   | 2.40 GHz   | 1.50 GHz   | 4 MB SmartCache   |
| Intel(R) Core[TM] i5-2540M Processor                    | Launched        | Q1'11   | 2   | 3.30 GHz   | 2.60 GHz   | 3 MB SmartCache   |
| Intel(R) Core[TM] i5-2520M Processor                    | Launched        | Q1'11   | 2   | 3.20 GHz   | 2.50 GHz   | 3 MB SmartCache   |
| Intel(R) Core[TM] i7-2620M Processor                    | Launched        | Q1'11   | 2   | 3.40 GHz   | 2.70 GHz   | 4 MB SmartCache   |
| Intel(R) Core[TM] i3-2120 Processor                     | Discontinued    | Q1'11   | 2   |            | 3.30 GHz   | 3 MB SmartCache   |
| Intel(R) Core[TM] i5-2510E Processor                    | Discontinued    | Q1'11   | 2   | 3.10 GHz   | 2.50 GHz   | 3 MB SmartCache   |
| Intel(R) Core[TM] i5-2390T Processor                    | Discontinued    | Q1'11   | 2   | 3.50 GHz   | 2.70 GHz   | 3 MB SmartCache   |
| Intel(R) Core[TM] i3-2100 Processor                     | Discontinued    | Q1'11   | 2   |            | 3.10 GHz   | 3 MB SmartCache   |
| Intel(R) Core[TM] i3-2100T Processor                    | Discontinued    | Q1'11   | 2   |            | 2.50 GHz   | 3 MB SmartCache   |
| Intel(R) Core[TM] i5-2515E Processor                    | Discontinued    | Q1'11   | 2   | 3.10 GHz   | 2.50 GHz   | 3 MB SmartCache   |
| Intel(R) Core[TM] i3-2310M Processor                    | Launched        | Q1'11   | 2   |            | 2.10 GHz   | 3 MB SmartCache   |
| Intel(R) Core[TM] i5-2410M Processor                    | Launched        | Q1'11   | 2   | 2.90 GHz   | 2.30 GHz   | 3 MB SmartCache   |
| Intel(R) Core[TM] i7-2649M Processor                    | Launched        | Q1'11   | 2   | 3.20 GHz   | 2.30 GHz   | 4 MB SmartCache   |
| Intel(R) Core[TM] i7-2657M Processor                    | Launched        | Q1'11   | 2   | 2.70 GHz   | 1.60 GHz   | 4 MB SmartCache   |
| Intel(R) Core[TM] i7-2617M Processor                    | Launched        | Q1'11   | 2   | 2.60 GHz   | 1.50 GHz   | 4 MB SmartCache   |
| Intel(R) Core[TM] i5-2537M Processor                    | Launched        | Q1'11   | 2   | 2.30 GHz   | 1.40 GHz   | 3 MB SmartCache   |
| Intel(R) Core[TM] i7-2629M Processor                    | Launched        | Q1'11   | 2   | 3.00 GHz   | 2.10 GHz   | 4 MB SmartCache   |
| Intel(R) Core[TM] i7-2720QM Processor                   | Launched        | Q1'11   | 4   | 3.30 GHz   | 2.20 GHz   | 6 MB SmartCache   |
| Intel(R) Core[TM] i5-2400 Processor                     | Discontinued    | Q1'11   | 4   | 3.40 GHz   | 3.10 GHz   | 6 MB SmartCache   |
| Intel(R) Core[TM] i5-2400S Processor                    | Discontinued    | Q1'11   | 4   | 3.30 GHz   | 2.50 GHz   | 6 MB SmartCache   |
| Intel(R) Core[TM] i5-2500 Processor                     | Discontinued    | Q1'11   | 4   | 3.70 GHz   | 3.30 GHz   | 6 MB SmartCache   |
| Intel(R) Core[TM] i7-2820QM Processor                   | Launched        | Q1'11   | 4   | 3.40 GHz   | 2.30 GHz   | 8 MB SmartCache   |
| Intel(R) Core[TM] i5-2500K Processor                    | Discontinued    | Q1'11   | 4   | 3.70 GHz   | 3.30 GHz   | 6 MB SmartCache   |
| Intel(R) Core[TM] i5-2500S Processor                    | Discontinued    | Q1'11   | 4   | 3.70 GHz   | 2.70 GHz   | 6 MB SmartCache   |
| Intel(R) Core[TM] i5-2500T Processor                    | Discontinued    | Q1'11   | 4   | 3.30 GHz   | 2.30 GHz   | 6 MB SmartCache   |
| Intel(R) Core[TM] i7-2600 Processor                     | Discontinued    | Q1'11   | 4   | 3.80 GHz   | 3.40 GHz   | 8 MB SmartCache   |
| Intel(R) Core[TM] i7-2920XM Processor Extreme Edition   | Launched        | Q1'11   | 4   | 3.50 GHz   | 2.50 GHz   | 8 MB SmartCache   |
| Intel(R) Core[TM] i5-2300 Processor                     | Discontinued    | Q1'11   | 4   | 3.10 GHz   | 2.80 GHz   | 6 MB SmartCache   |
| Intel(R) Core[TM] i7-2600K Processor                    | Discontinued    | Q1'11   | 4   | 3.80 GHz   | 3.40 GHz   | 8 MB SmartCache   |
| Intel(R) Core[TM] i7-2600S Processor                    | Discontinued    | Q1'11   | 4   | 3.80 GHz   | 2.80 GHz   | 8 MB SmartCache   |
| Intel(R) Core[TM] i7-2630QM Processor                   | Launched        | Q1'11   | 4   | 2.90 GHz   | 2.00 GHz   | 6 MB SmartCache   |
| Intel(R) Core[TM] i7-2635QM Processor                   | Launched        | Q1'11   | 4   | 2.90 GHz   | 2.00 GHz   | 6 MB SmartCache   |
| Intel(R) Core[TM] i7-2710QE Processor                   | Discontinued    | Q1'11   | 4   | 3.00 GHz   | 2.10 GHz   | 6 MB SmartCache   |
| Intel(R) Core[TM] i7-2715QE Processor                   | Discontinued    | Q1'11   | 4   | 3.00 GHz   | 2.10 GHz   | 6 MB SmartCache   |
| Intel(R) Core[TM] i3-2310E Processor                    | Discontinued    | Q1'11   | 2   |            | 2.10 GHz   | 3 MB SmartCache   |

# INTEL PENTIUM
| PRODUCT NAME                                       | STATUS          | RELEASE | COR | TURBO MAX  | BASE FREQ  | CACHE             |
| -------------------------------------------------- | --------------- | ------- | --- | ---------- | ---------- | ----------------- |
| Intel(R) Pentium(R) Processor 1405 v2                  | Launched        | Q1'14   | 2   |            | 1.40 GHz   | 6 MB SmartCache   |
| Intel(R) Pentium(R) Processor B915C                    | Launched        | Q2'12   | 2   |            | 1.50 GHz   | 3 MB L3           |
| Intel(R) Pentium(R) Processor 1405                     | Discontinued    | Q2'12   | 2   | 1.80 GHz   | 1.20 GHz   | 5 MB SmartCache   |

# INTEL XEON
| PRODUCT NAME                                       | STATUS          | RELEASE | COR | TURBO MAX  | BASE FREQ  | CACHE             |
| -------------------------------------------------- | --------------- | ------- | --- | ---------- | ---------- | ----------------- |
| Intel(R) Xeon(R) Gold 6244 Processor                   | Launched        | Q2'19   | 8   | 4.40 GHz   | 3.60 GHz   | 24.75 MB          |
| Intel(R) Xeon(R) Gold 6240 Processor                   | Launched        | Q2'19   | 18  | 3.90 GHz   | 2.60 GHz   | 24.75 MB          |
| Intel(R) Xeon(R) Gold 5218 Processor                   | Launched        | Q2'19   | 16  | 3.90 GHz   | 2.30 GHz   | 22 MB             |
| Intel(R) Xeon(R) Platinum 8253 Processor               | Launched        | Q2'19   | 16  | 3.00 GHz   | 2.20 GHz   | 22 MB             |
| Intel(R) Xeon(R) Gold 6230 Processor                   | Launched        | Q2'19   | 20  | 3.90 GHz   | 2.10 GHz   | 27.5 MB           |
| Intel(R) Xeon(R) Gold 5222 Processor                   | Launched        | Q2'19   | 4   | 3.90 GHz   | 3.80 GHz   | 16.5 MB           |
| Intel(R) Xeon(R) Gold 6238T Processor                  | Launched        | Q2'19   | 22  | 3.70 GHz   | 1.90 GHz   | 30.25 MB          |
| Intel(R) Xeon(R) Gold 6248 Processor                   | Launched        | Q2'19   | 20  | 3.90 GHz   | 2.50 GHz   | 27.5 MB           |
| Intel(R) Xeon(R) Platinum 8256 Processor               | Launched        | Q2'19   | 4   | 3.90 GHz   | 3.80 GHz   | 16.5 MB           |
| Intel(R) Xeon(R) Gold 6252 Processor                   | Launched        | Q2'19   | 24  | 3.70 GHz   | 2.10 GHz   | 35.75 MB          |
| Intel(R) Xeon(R) Platinum 8260 Processor               | Launched        | Q2'19   | 24  | 3.90 GHz   | 2.40 GHz   | 35.75 MB          |
| Intel(R) Xeon(R) Gold 6242 Processor                   | Launched        | Q2'19   | 16  | 3.90 GHz   | 2.80 GHz   | 22 MB             |
| Intel(R) Xeon(R) Platinum 8276 Processor               | Launched        | Q2'19   | 28  | 4.00 GHz   | 2.20 GHz   | 38.5 MB           |
| Intel(R) Xeon(R) Platinum 8276M Processor              | Launched        | Q2'19   | 28  | 4.00 GHz   | 2.20 GHz   | 38.5 MB           |
| Intel(R) Xeon(R) Gold 6240Y Processor                  | Launched        | Q2'19   | 18  | 3.90 GHz   | 2.60 GHz   | 24.75 MB          |
| Intel(R) Xeon(R) Platinum 8276L Processor              | Launched        | Q2'19   | 28  | 4.00 GHz   | 2.20 GHz   | 38.5 MB           |
| Intel(R) Xeon(R) Silver 4209T Processor                | Launched        | Q2'19   | 8   | 3.20 GHz   | 2.20 GHz   | 11 MB             |
| Intel(R) Xeon(R) Platinum 8280L Processor              | Launched        | Q2'19   | 28  | 4.00 GHz   | 2.70 GHz   | 38.5 MB           |
| Intel(R) Xeon(R) Gold 5217 Processor                   | Launched        | Q2'19   | 8   | 3.70 GHz   | 3.00 GHz   | 11 MB             |
| Intel(R) Xeon(R) Silver 4210 Processor                 | Launched        | Q2'19   | 10  | 3.20 GHz   | 2.20 GHz   | 13.75 MB          |
| Intel(R) Xeon(R) Platinum 8260L Processor              | Launched        | Q2'19   | 24  | 3.90 GHz   | 2.40 GHz   | 35.75 MB          |
| Intel(R) Xeon(R) Gold 6238M Processor                  | Launched        | Q2'19   | 22  | 3.70 GHz   | 2.10 GHz   | 30.25 MB          |
| Intel(R) Xeon(R) Platinum 8280M Processor              | Launched        | Q2'19   | 28  | 4.00 GHz   | 2.70 GHz   | 38.5 MB           |
| Intel(R) Xeon(R) Gold 5218N Processor                  | Launched        | Q2'19   | 16  | 3.70 GHz   | 2.30 GHz   | 22 MB             |
| Intel(R) Xeon(R) Gold 6230N Processor                  | Launched        | Q2'19   | 20  | 3.90 GHz   | 2.30 GHz   | 27.5 MB           |
| Intel(R) Xeon(R) Silver 4214 Processor                 | Launched        | Q2'19   | 12  | 3.20 GHz   | 2.20 GHz   | 16.5 MB           |
| Intel(R) Xeon(R) Platinum 8260M Processor              | Launched        | Q2'19   | 24  | 3.90 GHz   | 2.40 GHz   | 35.75 MB          |
| Intel(R) Xeon(R) Gold 6226 Processor                   | Launched        | Q2'19   | 12  | 3.70 GHz   | 2.70 GHz   | 19.25 MB          |
| Intel(R) Xeon(R) Gold 6254 Processor                   | Launched        | Q2'19   | 18  | 4.00 GHz   | 3.10 GHz   | 24.75 MB          |
| Intel(R) Xeon(R) Silver 4214Y Processor                | Launched        | Q2'19   | 12  | 3.20 GHz   | 2.20 GHz   | 16.5 MB           |
| Intel(R) Xeon(R) Gold 5220 Processor                   | Launched        | Q2'19   | 18  | 3.90 GHz   | 2.20 GHz   | 24.75 MB          |
| Intel(R) Xeon(R) Platinum 8280 Processor               | Launched        | Q2'19   | 28  | 4.00 GHz   | 2.70 GHz   | 38.5 MB           |
| Intel(R) Xeon(R) Gold 6238L Processor                  | Launched        | Q2'19   | 22  | 3.70 GHz   | 2.10 GHz   | 30.25 MB          |
| Intel(R) Xeon(R) Platinum 8260Y Processor              | Launched        | Q2'19   | 24  | 3.90 GHz   | 2.40 GHz   | 35.75 MB          |
| Intel(R) Xeon(R) Gold 5218B Processor                  | Launched        | Q2'19   | 16  | 3.90 GHz   | 2.30 GHz   | 22 MB             |
| Intel(R) Xeon(R) Silver 4215 Processor                 | Launched        | Q2'19   | 8   | 3.50 GHz   | 2.50 GHz   | 11 MB             |
| Intel(R) Xeon(R) Gold 6240L Processor                  | Launched        | Q2'19   | 18  | 3.90 GHz   | 2.60 GHz   | 24.75 MB          |
| Intel(R) Xeon(R) Gold 6238 Processor                   | Launched        | Q2'19   | 22  | 3.70 GHz   | 2.10 GHz   | 30.25 MB          |
| Intel(R) Xeon(R) Platinum 8268 Processor               | Launched        | Q2'19   | 24  | 3.90 GHz   | 2.90 GHz   | 35.75 MB          |
| Intel(R) Xeon(R) Silver 4208 Processor                 | Launched        | Q2'19   | 8   | 3.20 GHz   | 2.10 GHz   | 11 MB             |
| Intel(R) Xeon(R) Gold 6240M Processor                  | Launched        | Q2'19   | 18  | 3.90 GHz   | 2.60 GHz   | 24.75 MB          |
| Intel(R) Xeon(R) Gold 5220S Processor                  | Launched        | Q2'19   | 18  | 3.90 GHz   | 2.70 GHz   | 24.75 MB          |
| Intel(R) Xeon(R) Platinum 8270 Processor               | Launched        | Q2'19   | 26  | 4.00 GHz   | 2.70 GHz   | 35.75 MB          |
| Intel(R) Xeon(R) Gold 5215 Processor                   | Launched        | Q2'19   | 10  | 3.40 GHz   | 2.50 GHz   | 13.75 MB          |
| Intel(R) Xeon(R) Gold 6222V Processor                  | Launched        | Q2'19   | 20  | 3.60 GHz   | 1.80 GHz   | 27.5 MB           |
| Intel(R) Xeon(R) Gold 6252N Processor                  | Launched        | Q2'19   | 24  | 3.60 GHz   | 2.30 GHz   | 35.75 MB          |
| Intel(R) Xeon(R) Gold 5215M Processor                  | Launched        | Q2'19   | 10  | 3.40 GHz   | 2.50 GHz   | 13.75 MB          |
| Intel(R) Xeon(R) Gold 6230T Processor                  | Launched        | Q2'19   | 20  | 3.90 GHz   | 2.10 GHz   | 27.5 MB           |
| Intel(R) Xeon(R) Gold 6246 Processor                   | Launched        | Q2'19   | 12  | 4.20 GHz   | 3.30 GHz   | 24.75 MB          |
| Intel(R) Xeon(R) Gold 5215L Processor                  | Launched        | Q2'19   | 10  | 3.40 GHz   | 2.50 GHz   | 13.75 MB          |
| Intel(R) Xeon(R) Bronze 3204 Processor                 | Launched        | Q2'19   | 6   | 1.90 GHz   | 1.90 GHz   | 8.25 MB           |
| Intel(R) Xeon(R) Gold 5218T Processor                  | Launched        | Q2'19   | 16  | 3.80 GHz   | 2.10 GHz   | 22 MB             |
| Intel(R) Xeon(R) Gold 5220T Processor                  | Launched        | Q2'19   | 18  | 3.90 GHz   | 1.90 GHz   | 24.75 MB          |
| Intel(R) Xeon(R) Silver 4216 Processor                 | Launched        | Q2'19   | 16  | 3.20 GHz   | 2.10 GHz   | 22 MB             |
| Intel(R) Xeon(R) Gold 6234 Processor                   | Launched        | Q2'19   | 8   | 4.00 GHz   | 3.30 GHz   | 24.75 MB          |
| Intel(R) Xeon(R) Gold 6262V Processor                  | Launched        | Q2'19   | 24  | 3.60 GHz   | 1.90 GHz   | 33 MB             |
| Intel(R) Xeon(R) W-3175X Processor                     | Launched        | Q4'18   | 28  | 3.80 GHz   | 3.10 GHz   | 38.5 MB           |
| Intel(R) Xeon(R) W-2175 Processor                      | Launched        | Q4'17   | 14  | 4.30 GHz   | 2.50 GHz   | 19 MB             |
| Intel(R) Xeon(R) W-2133 Processor                      | Launched        | Q3'17   | 6   | 3.90 GHz   | 3.60 GHz   | 8.25 MB           |
| Intel(R) Xeon(R) W-2155 Processor                      | Launched        | Q3'17   | 10  | 4.50 GHz   | 3.30 GHz   | 13.75 MB          |
| Intel(R) Xeon(R) W-2123 Processor                      | Launched        | Q3'17   | 4   | 3.90 GHz   | 3.60 GHz   | 8.25 MB           |
| Intel(R) Xeon(R) W-2145 Processor                      | Launched        | Q3'17   | 8   | 4.50 GHz   | 3.70 GHz   | 11 MB             |
| Intel(R) Xeon(R) W-2125 Processor                      | Launched        | Q3'17   | 4   | 4.50 GHz   | 4.00 GHz   | 8.25 MB           |
| Intel(R) Xeon(R) W-2135 Processor                      | Launched        | Q3'17   | 6   | 4.50 GHz   | 3.70 GHz   | 8.25 MB           |
| Intel(R) Xeon(R) W-2195 Processor                      | Launched        | Q3'17   | 18  | 4.30 GHz   | 2.30 GHz   | 24.75 MB          |
| Intel(R) Xeon(R) Gold 5122 Processor                   | Launched        | Q3'17   | 4   | 3.70 GHz   | 3.60 GHz   | 16.5 MB L3        |
| Intel(R) Xeon(R) Gold 6142 Processor                   | Launched        | Q3'17   | 16  | 3.70 GHz   | 2.60 GHz   | 22 MB L3          |
| Intel(R) Xeon(R) Platinum 8153 Processor               | Launched        | Q3'17   | 16  | 2.80 GHz   | 2.00 GHz   | 22 MB L3          |
| Intel(R) Xeon(R) Platinum 8156 Processor               | Launched        | Q3'17   | 4   | 3.70 GHz   | 3.60 GHz   | 16.5 MB L3        |
| Intel(R) Xeon(R) Gold 6148 Processor                   | Launched        | Q3'17   | 20  | 3.70 GHz   | 2.40 GHz   | 27.5 MB L3        |
| Intel(R) Xeon(R) Gold 5120T Processor                  | Launched        | Q3'17   | 14  | 3.20 GHz   | 2.20 GHz   | 19.25 MB L3       |
| Intel(R) Xeon(R) Platinum 8158 Processor               | Launched        | Q3'17   | 12  | 3.70 GHz   | 3.00 GHz   | 24.75 MB L3       |
| Intel(R) Xeon(R) Platinum 8176 Processor               | Launched        | Q3'17   | 28  | 3.80 GHz   | 2.10 GHz   | 38.5 MB L3        |
| Intel(R) Xeon(R) Gold 6136 Processor                   | Launched        | Q3'17   | 12  | 3.70 GHz   | 3.00 GHz   | 24.75 MB L3       |
| Intel(R) Xeon(R) Gold 6150 Processor                   | Launched        | Q3'17   | 18  | 3.70 GHz   | 2.70 GHz   | 24.75 MB L3       |
| Intel(R) Xeon(R) Platinum 8160 Processor               | Launched        | Q3'17   | 24  | 3.70 GHz   | 2.10 GHz   | 33 MB L3          |
| Intel(R) Xeon(R) Silver 4116 Processor                 | Launched        | Q3'17   | 12  | 3.00 GHz   | 2.10 GHz   | 16.5 MB L3        |
| Intel(R) Xeon(R) Gold 6152 Processor                   | Launched        | Q3'17   | 22  | 3.70 GHz   | 2.10 GHz   | 30.25 MB L3       |
| Intel(R) Xeon(R) Gold 6130 Processor                   | Launched        | Q3'17   | 16  | 3.70 GHz   | 2.10 GHz   | 22 MB L3          |
| Intel(R) Xeon(R) Gold 6128 Processor                   | Launched        | Q3'17   | 6   | 3.70 GHz   | 3.40 GHz   | 19.25 MB L3       |
| Intel(R) Xeon(R) Gold 5118 Processor                   | Launched        | Q3'17   | 12  | 3.20 GHz   | 2.30 GHz   | 16.5 MB L3        |
| Intel(R) Xeon(R) Platinum 8164 Processor               | Launched        | Q3'17   | 26  | 3.70 GHz   | 2.00 GHz   | 35.75 MB L3       |
| Intel(R) Xeon(R) Gold 6134 Processor                   | Launched        | Q3'17   | 8   | 3.70 GHz   | 3.20 GHz   | 24.75 MB L3       |
| Intel(R) Xeon(R) Gold 6126 Processor                   | Launched        | Q3'17   | 12  | 3.70 GHz   | 2.60 GHz   | 19.25 MB L3       |
| Intel(R) Xeon(R) Gold 5120 Processor                   | Launched        | Q3'17   | 14  | 3.20 GHz   | 2.20 GHz   | 19.25 MB L3       |
| Intel(R) Xeon(R) Platinum 8168 Processor               | Launched        | Q3'17   | 24  | 3.70 GHz   | 2.70 GHz   | 33 MB L3          |
| Intel(R) Xeon(R) Gold 5115 Processor                   | Launched        | Q3'17   | 10  | 3.20 GHz   | 2.40 GHz   | 13.75 MB L3       |
| Intel(R) Xeon(R) Gold 6154 Processor                   | Launched        | Q3'17   | 18  | 3.70 GHz   | 3.00 GHz   | 24.75 MB L3       |
| Intel(R) Xeon(R) Gold 6140 Processor                   | Launched        | Q3'17   | 18  | 3.70 GHz   | 2.30 GHz   | 24.75 MB L3       |
| Intel(R) Xeon(R) Platinum 8170 Processor               | Launched        | Q3'17   | 26  | 3.70 GHz   | 2.10 GHz   | 35.75 MB L3       |
| Intel(R) Xeon(R) Platinum 8180 Processor               | Launched        | Q3'17   | 28  | 3.80 GHz   | 2.50 GHz   | 38.5 MB L3        |
| Intel(R) Xeon(R) Silver 4109T Processor                | Launched        | Q3'17   | 8   | 3.00 GHz   | 2.00 GHz   | 11 MB L3          |
| Intel(R) Xeon(R) Bronze 3106 Processor                 | Launched        | Q3'17   | 8   |            | 1.70 GHz   | 11 MB L3          |
| Intel(R) Xeon(R) Silver 4114 Processor                 | Launched        | Q3'17   | 10  | 3.00 GHz   | 2.20 GHz   | 13.75 MB L3       |
| Intel(R) Xeon(R) Gold 6132 Processor                   | Launched        | Q3'17   | 14  | 3.70 GHz   | 2.60 GHz   | 19.25 MB L3       |
| Intel(R) Xeon(R) Silver 4112 Processor                 | Launched        | Q3'17   | 4   | 3.00 GHz   | 2.60 GHz   | 8.25 MB L3        |
| Intel(R) Xeon(R) Gold 6138T Processor                  | Launched        | Q3'17   | 20  | 3.70 GHz   | 2.00 GHz   | 27.5 MB L3        |
| Intel(R) Xeon(R) Platinum 8160T Processor              | Launched        | Q3'17   | 24  | 3.70 GHz   | 2.10 GHz   | 33 MB L3          |
| Intel(R) Xeon(R) Silver 4108 Processor                 | Launched        | Q3'17   | 8   | 3.00 GHz   | 1.80 GHz   | 11 MB L3          |
| Intel(R) Xeon(R) Gold 6142F Processor                  | Launched        | Q3'17   | 16  | 3.70 GHz   | 2.60 GHz   | 22 MB L3          |
| Intel(R) Xeon(R) Gold 6130T Processor                  | Launched        | Q3'17   | 16  | 3.70 GHz   | 2.10 GHz   | 22 MB L3          |
| Intel(R) Xeon(R) Gold 6138F Processor                  | Launched        | Q3'17   | 20  | 3.70 GHz   | 2.00 GHz   | 27.5 MB L3        |
| Intel(R) Xeon(R) Bronze 3104 Processor                 | Launched        | Q3'17   | 6   |            | 1.70 GHz   | 8.25 MB L3        |
| Intel(R) Xeon(R) Platinum 8160F Processor              | Launched        | Q3'17   | 24  | 3.70 GHz   | 2.10 GHz   | 33 MB L3          |
| Intel(R) Xeon(R) Silver 4110 Processor                 | Launched        | Q3'17   | 8   | 3.00 GHz   | 2.10 GHz   | 11 MB L3          |
| Intel(R) Xeon(R) Gold 6130F Processor                  | Launched        | Q3'17   | 16  | 3.70 GHz   | 2.10 GHz   | 22 MB L3          |
| Intel(R) Xeon(R) Gold 6126T Processor                  | Launched        | Q3'17   | 12  | 3.70 GHz   | 2.60 GHz   | 19.25 MB L3       |
| Intel(R) Xeon(R) Gold 6126F Processor                  | Launched        | Q3'17   | 12  | 3.70 GHz   | 2.60 GHz   | 19.25 MB L3       |
| Intel(R) Xeon(R) Gold 6148F Processor                  | Launched        | Q3'17   | 20  | 3.70 GHz   | 2.40 GHz   | 27.5 MB L3        |
| Intel(R) Xeon(R) Gold 6146 Processor                   | Launched        | Q3'17   | 12  | 4.20 GHz   | 3.20 GHz   | 24.75 MB L3       |
| Intel(R) Xeon(R) Gold 6144 Processor                   | Launched        | Q3'17   | 8   | 4.20 GHz   | 3.50 GHz   | 24.75 MB L3       |
| Intel(R) Xeon(R) Platinum 8176F Processor              | Launched        | Q3'17   | 28  | 3.80 GHz   | 2.10 GHz   | 38.5 MB L3        |
| Intel(R) Xeon(R) Silver 4116T Processor                | Launched        | Q3'17   | 12  | 3.00 GHz   | 2.10 GHz   | 16.5 MB L3        |
| Intel(R) Xeon(R) Silver 4114T Processor                | Launched        | Q3'17   | 10  | 3.00 GHz   | 2.20 GHz   | 13.75 MB L3       |
| Intel(R) Xeon(R) Gold 5119T Processor                  | Launched        | Q3'17   | 14  | 3.20 GHz   | 1.90 GHz   | 19.25 MB L3       |
| Intel(R) Xeon(R) Processor E5-2408L v3                 | Launched        | Q1'15   | 4   |            | 1.80 GHz   | 10 MB             |
| Intel(R) Xeon(R) Processor E5-2418L v3                 | Launched        | Q1'15   | 6   |            | 2.00 GHz   | 15 MB             |
| Intel(R) Xeon(R) Processor E5-2428L v3                 | Launched        | Q1'15   | 8   |            | 1.80 GHz   | 20 MB             |
| Intel(R) Xeon(R) Processor E5-2438L v3                 | Launched        | Q1'15   | 10  |            | 1.80 GHz   | 25 MB             |
| Intel(R) Xeon(R) Processor E5-1428L v3                 | Launched        | Q1'15   | 8   |            | 2.00 GHz   | 20 MB             |
| Intel(R) Xeon(R) Processor E5-4640 v2                  | Launched        | Q1'14   | 10  | 2.70 GHz   | 2.20 GHz   | 20 MB SmartCache  |
| Intel(R) Xeon(R) Processor E5-4650 v2                  | Launched        | Q1'14   | 10  | 2.90 GHz   | 2.40 GHz   | 25 MB SmartCache  |
| Intel(R) Xeon(R) Processor E5-4657L v2                 | Launched        | Q1'14   | 12  | 2.90 GHz   | 2.40 GHz   | 30 MB SmartCache  |
| Intel(R) Xeon(R) Processor E5-4607 v2                  | Launched        | Q1'14   | 6   | 2.60 GHz   | 2.60 GHz   | 15 MB SmartCache  |
| Intel(R) Xeon(R) Processor E5-4610 v2                  | Launched        | Q1'14   | 8   | 2.70 GHz   | 2.30 GHz   | 16 MB SmartCache  |
| Intel(R) Xeon(R) Processor E5-4620 v2                  | Launched        | Q1'14   | 8   | 3.00 GHz   | 2.60 GHz   | 20 MB SmartCache  |
| Intel(R) Xeon(R) Processor E5-4627 v2                  | Launched        | Q1'14   | 8   | 3.60 GHz   | 3.30 GHz   | 16 MB SmartCache  |
| Intel(R) Xeon(R) Processor E5-4603 v2                  | Launched        | Q1'14   | 4   | 2.20 GHz   | 2.20 GHz   | 10 MB SmartCache  |
| Intel(R) Xeon(R) Processor E5-4624L v2                 | Launched        | Q1'14   | 10  | 2.50 GHz   | 1.90 GHz   | 25 MB SmartCache  |
| Intel(R) Xeon(R) Processor E7-4809 v2                  | Launched        | Q1'14   | 6   |            | 1.90 GHz   | 12 MB             |
| Intel(R) Xeon(R) Processor E7-8880L v2                 | Launched        | Q1'14   | 15  | 2.80 GHz   | 2.20 GHz   | 37.5 MB           |
| Intel(R) Xeon(R) Processor E7-8880 v2                  | Launched        | Q1'14   | 15  | 3.10 GHz   | 2.50 GHz   | 37.5 MB           |
| Intel(R) Xeon(R) Processor E7-4820 v2                  | Launched        | Q1'14   | 8   | 2.50 GHz   | 2.00 GHz   | 16 MB             |
| Intel(R) Xeon(R) Processor E7-8890 v2                  | Launched        | Q1'14   | 15  | 3.40 GHz   | 2.80 GHz   | 37.5 MB           |
| Intel(R) Xeon(R) Processor E7-8891 v2                  | Launched        | Q1'14   | 10  | 3.70 GHz   | 3.20 GHz   | 37.5 MB           |
| Intel(R) Xeon(R) Processor E7-2850 v2                  | Launched        | Q1'14   | 12  | 2.80 GHz   | 2.30 GHz   | 24 MB             |
| Intel(R) Xeon(R) Processor E7-4830 v2                  | Launched        | Q1'14   | 10  | 2.70 GHz   | 2.20 GHz   | 20 MB             |
| Intel(R) Xeon(R) Processor E7-8893 v2                  | Launched        | Q1'14   | 6   | 3.70 GHz   | 3.40 GHz   | 37.5 MB           |
| Intel(R) Xeon(R) Processor E7-2870 v2                  | Launched        | Q1'14   | 15  | 2.90 GHz   | 2.30 GHz   | 30 MB             |
| Intel(R) Xeon(R) Processor E7-4850 v2                  | Launched        | Q1'14   | 12  | 2.80 GHz   | 2.30 GHz   | 24 MB             |
| Intel(R) Xeon(R) Processor E7-2880 v2                  | Launched        | Q1'14   | 15  | 3.10 GHz   | 2.50 GHz   | 37.5 MB           |
| Intel(R) Xeon(R) Processor E7-2890 v2                  | Launched        | Q1'14   | 15  | 3.40 GHz   | 2.80 GHz   | 37.5 MB           |
| Intel(R) Xeon(R) Processor E7-4860 v2                  | Launched        | Q1'14   | 12  | 3.20 GHz   | 2.60 GHz   | 30 MB             |
| Intel(R) Xeon(R) Processor E7-4870 v2                  | Launched        | Q1'14   | 15  | 2.90 GHz   | 2.30 GHz   | 30 MB             |
| Intel(R) Xeon(R) Processor E7-4890 v2                  | Launched        | Q1'14   | 15  | 3.40 GHz   | 2.80 GHz   | 37.5 MB           |
| Intel(R) Xeon(R) Processor E7-8850 v2                  | Launched        | Q1'14   | 12  | 2.80 GHz   | 2.30 GHz   | 24 MB             |
| Intel(R) Xeon(R) Processor E7-8857 v2                  | Launched        | Q1'14   | 12  | 3.60 GHz   | 3.00 GHz   | 30 MB             |
| Intel(R) Xeon(R) Processor E7-8870 v2                  | Launched        | Q1'14   | 15  | 2.90 GHz   | 2.30 GHz   | 30 MB             |
| Intel(R) Xeon(R) Processor E7-4880 v2                  | Launched        | Q1'14   | 15  | 3.10 GHz   | 2.50 GHz   | 37.5 MB           |
| Intel(R) Xeon(R) Processor E5-2420 v2                  | Launched        | Q1'14   | 6   | 2.70 GHz   | 2.20 GHz   | 15 MB SmartCache  |
| Intel(R) Xeon(R) Processor E5-2440 v2                  | Launched        | Q1'14   | 8   | 2.40 GHz   | 1.90 GHz   | 20 MB SmartCache  |
| Intel(R) Xeon(R) Processor E5-2428L v2                 | Launched        | Q1'14   | 8   | 2.30 GHz   | 1.80 GHz   | 20 MB SmartCache  |
| Intel(R) Xeon(R) Processor E5-2403 v2                  | Launched        | Q1'14   | 4   | 1.80 GHz   | 1.80 GHz   | 10 MB SmartCache  |
| Intel(R) Xeon(R) Processor E5-2430 v2                  | Launched        | Q1'14   | 6   | 3.00 GHz   | 2.50 GHz   | 15 MB SmartCache  |
| Intel(R) Xeon(R) Processor E5-2450 v2                  | Launched        | Q1'14   | 8   | 3.30 GHz   | 2.50 GHz   | 20 MB SmartCache  |
| Intel(R) Xeon(R) Processor E5-2430L v2                 | Launched        | Q1'14   | 6   | 2.80 GHz   | 2.40 GHz   | 15 MB SmartCache  |
| Intel(R) Xeon(R) Processor E5-2450L v2                 | Launched        | Q1'14   | 10  | 2.10 GHz   | 1.70 GHz   | 25 MB SmartCache  |
| Intel(R) Xeon(R) Processor E5-1428L v2                 | Launched        | Q1'14   | 6   | 2.70 GHz   | 2.20 GHz   | 15 MB SmartCache  |
| Intel(R) Xeon(R) Processor E5-2470 v2                  | Launched        | Q1'14   | 10  | 3.20 GHz   | 2.40 GHz   | 25 MB SmartCache  |
| Intel(R) Xeon(R) Processor E5-2407 v2                  | Launched        | Q1'14   | 4   | 2.40 GHz   | 2.40 GHz   | 10 MB SmartCache  |
| Intel(R) Xeon(R) Processor E5-2448L v2                 | Launched        | Q1'14   | 10  | 2.40 GHz   | 1.80 GHz   | 25 MB SmartCache  |
| Intel(R) Xeon(R) Processor E5-2418L v2                 | Launched        | Q1'14   | 6   | 2.00 GHz   | 2.00 GHz   | 15 MB SmartCache  |
| Intel(R) Xeon(R) Processor E5-2640 v2                  | Launched        | Q3'13   | 8   | 2.50 GHz   | 2.00 GHz   | 20 MB SmartCache  |
| Intel(R) Xeon(R) Processor E5-2680 v2                  | Launched        | Q3'13   | 10  | 3.60 GHz   | 2.80 GHz   | 25 MB SmartCache  |
| Intel(R) Xeon(R) Processor E5-2690 v2                  | Launched        | Q3'13   | 10  | 3.60 GHz   | 3.00 GHz   | 25 MB SmartCache  |
| Intel(R) Xeon(R) Processor E5-2643 v2                  | Launched        | Q3'13   | 6   | 3.80 GHz   | 3.50 GHz   | 25 MB SmartCache  |
| Intel(R) Xeon(R) Processor E5-2650 v2                  | Launched        | Q3'13   | 8   | 3.40 GHz   | 2.60 GHz   | 20 MB SmartCache  |
| Intel(R) Xeon(R) Processor E5-2650L v2                 | Launched        | Q3'13   | 10  | 2.10 GHz   | 1.70 GHz   | 25 MB SmartCache  |
| Intel(R) Xeon(R) Processor E5-2695 v2                  | Launched        | Q3'13   | 12  | 3.20 GHz   | 2.40 GHz   | 30 MB SmartCache  |
| Intel(R) Xeon(R) Processor E5-2660 v2                  | Launched        | Q3'13   | 10  | 3.00 GHz   | 2.20 GHz   | 25 MB SmartCache  |
| Intel(R) Xeon(R) Processor E5-2667 v2                  | Launched        | Q3'13   | 8   | 4.00 GHz   | 3.30 GHz   | 25 MB SmartCache  |
| Intel(R) Xeon(R) Processor E5-2697 v2                  | Launched        | Q3'13   | 12  | 3.50 GHz   | 2.70 GHz   | 30 MB SmartCache  |
| Intel(R) Xeon(R) Processor E5-2603 v2                  | Launched        | Q3'13   | 4   |            | 1.80 GHz   | 10 MB SmartCache  |
| Intel(R) Xeon(R) Processor E5-2670 v2                  | Launched        | Q3'13   | 10  | 3.30 GHz   | 2.50 GHz   | 25 MB SmartCache  |
| Intel(R) Xeon(R) Processor E5-2628L v2                 | Launched        | Q3'13   | 8   | 2.40 GHz   | 1.90 GHz   | 20 MB SmartCache  |
| Intel(R) Xeon(R) Processor E5-2609 v2                  | Launched        | Q3'13   | 4   | 2.50 GHz   | 2.50 GHz   | 10 MB SmartCache  |
| Intel(R) Xeon(R) Processor E5-2648L v2                 | Launched        | Q3'13   | 10  | 2.50 GHz   | 1.90 GHz   | 25 MB SmartCache  |
| Intel(R) Xeon(R) Processor E5-2658 v2                  | Launched        | Q3'13   | 10  | 3.00 GHz   | 2.40 GHz   | 25 MB SmartCache  |
| Intel(R) Xeon(R) Processor E5-1620 v2                  | Launched        | Q3'13   | 4   | 3.90 GHz   | 3.70 GHz   | 10 MB SmartCache  |
| Intel(R) Xeon(R) Processor E5-2618L v2                 | Launched        | Q3'13   | 6   | 2.00 GHz   | 2.00 GHz   | 15 MB SmartCache  |
| Intel(R) Xeon(R) Processor E5-2687W v2                 | Launched        | Q3'13   | 8   | 4.00 GHz   | 3.40 GHz   | 25 MB SmartCache  |
| Intel(R) Xeon(R) Processor E5-1650 v2                  | Launched        | Q3'13   | 6   | 3.90 GHz   | 3.50 GHz   | 12 MB SmartCache  |
| Intel(R) Xeon(R) Processor E5-2620 v2                  | Launched        | Q3'13   | 6   | 2.60 GHz   | 2.10 GHz   | 15 MB SmartCache  |
| Intel(R) Xeon(R) Processor E5-1660 v2                  | Launched        | Q3'13   | 6   | 4.00 GHz   | 3.70 GHz   | 15 MB SmartCache  |
| Intel(R) Xeon(R) Processor E5-2630 v2                  | Launched        | Q3'13   | 6   | 3.10 GHz   | 2.60 GHz   | 15 MB SmartCache  |
| Intel(R) Xeon(R) Processor E5-2630L v2                 | Launched        | Q3'13   | 6   | 2.80 GHz   | 2.40 GHz   | 15 MB SmartCache  |
| Intel(R) Xeon(R) Processor E5-2637 v2                  | Launched        | Q3'13   | 4   | 3.80 GHz   | 3.50 GHz   | 15 MB SmartCache  |
| Intel(R) Xeon(R) Processor E3-1105C v2                 | Launched        | Q3'13   | 4   |            | 1.80 GHz   | 8 MB L3           |
| Intel(R) Xeon(R) Processor E3-1125C v2                 | Launched        | Q3'13   | 4   |            | 2.50 GHz   | 8 MB L3           |
| Intel(R) Xeon(R) Processor E3-1125C                    | Launched        | Q2'12   | 4   |            | 2.00 GHz   | 8 MB L3           |
| Intel(R) Xeon(R) Processor E3-1105C                    | Launched        | Q2'12   | 4   |            | 1.00 GHz   | 6 MB L3           |
| Intel(R) Xeon(R) Processor E5-2450L                    | Discontinued    | Q2'12   | 8   | 2.30 GHz   | 1.80 GHz   | 20 MB SmartCache  |
| Intel(R) Xeon(R) Processor E5-2450                     | Discontinued    | Q2'12   | 8   | 2.90 GHz   | 2.10 GHz   | 20 MB SmartCache  |
| Intel(R) Xeon(R) Processor E5-4610                     | Discontinued    | Q2'12   | 6   | 2.90 GHz   | 2.40 GHz   | 15 MB SmartCache  |
| Intel(R) Xeon(R) Processor E5-4650                     | Discontinued    | Q2'12   | 8   | 3.30 GHz   | 2.70 GHz   | 20 MB SmartCache  |
| Intel(R) Xeon(R) Processor E5-4640                     | Discontinued    | Q2'12   | 8   | 2.80 GHz   | 2.40 GHz   | 20 MB SmartCache  |
| Intel(R) Xeon(R) Processor E5-2440                     | Discontinued    | Q2'12   | 6   | 2.90 GHz   | 2.40 GHz   | 15 MB SmartCache  |
| Intel(R) Xeon(R) Processor E5-2470                     | Discontinued    | Q2'12   | 8   | 3.10 GHz   | 2.30 GHz   | 20 MB SmartCache  |
| Intel(R) Xeon(R) Processor E5-2430L                    | Discontinued    | Q2'12   | 6   | 2.50 GHz   | 2.00 GHz   | 15 MB SmartCache  |
| Intel(R) Xeon(R) Processor E5-4607                     | Discontinued    | Q2'12   | 6   |            | 2.20 GHz   | 12 MB SmartCache  |
| Intel(R) Xeon(R) Processor E5-2407                     | Discontinued    | Q2'12   | 4   |            | 2.20 GHz   | 10 MB SmartCache  |
| Intel(R) Xeon(R) Processor E5-2403                     | Discontinued    | Q2'12   | 4   |            | 1.80 GHz   | 10 MB SmartCache  |
| Intel(R) Xeon(R) Processor E5-4650L                    | Discontinued    | Q2'12   | 8   | 3.10 GHz   | 2.60 GHz   | 20 MB SmartCache  |
| Intel(R) Xeon(R) Processor E5-2430                     | Discontinued    | Q2'12   | 6   | 2.70 GHz   | 2.20 GHz   | 15 MB SmartCache  |
| Intel(R) Xeon(R) Processor E5-4620                     | Discontinued    | Q2'12   | 8   | 2.60 GHz   | 2.20 GHz   | 16 MB SmartCache  |
| Intel(R) Xeon(R) Processor E5-4617                     | Discontinued    | Q2'12   | 6   | 3.40 GHz   | 2.90 GHz   | 15 MB SmartCache  |
| Intel(R) Xeon(R) Processor E5-2420                     | Discontinued    | Q2'12   | 6   | 2.40 GHz   | 1.90 GHz   | 15 MB SmartCache  |
| Intel(R) Xeon(R) Processor E3-1290 v2                  | Discontinued    | Q2'12   | 4   | 4.10 GHz   | 3.70 GHz   | 8 MB SmartCache   |
| Intel(R) Xeon(R) Processor E5-4603                     | Discontinued    | Q2'12   | 4   |            | 2.00 GHz   | 10 MB SmartCache  |
| Intel(R) Xeon(R) Processor E3-1230 v2                  | Discontinued    | Q2'12   | 4   | 3.70 GHz   | 3.30 GHz   | 8 MB SmartCache   |
| Intel(R) Xeon(R) Processor E3-1225 v2                  | Launched        | Q2'12   | 4   | 3.60 GHz   | 3.20 GHz   | 8 MB SmartCache   |
| Intel(R) Xeon(R) Processor E3-1280 v2                  | Discontinued    | Q2'12   | 4   | 4.00 GHz   | 3.60 GHz   | 8 MB SmartCache   |
| Intel(R) Xeon(R) Processor E3-1220 v2                  | Discontinued    | Q2'12   | 4   | 3.50 GHz   | 3.10 GHz   | 8 MB SmartCache   |
| Intel(R) Xeon(R) Processor E3-1275 v2                  | Launched        | Q2'12   | 4   | 3.90 GHz   | 3.50 GHz   | 8 MB SmartCache   |
| Intel(R) Xeon(R) Processor E3-1220L v2                 | Discontinued    | Q2'12   | 2   | 3.50 GHz   | 2.30 GHz   | 3 MB SmartCache   |
| Intel(R) Xeon(R) Processor E3-1270 v2                  | Discontinued    | Q2'12   | 4   | 3.90 GHz   | 3.50 GHz   | 8 MB SmartCache   |
| Intel(R) Xeon(R) Processor E3-1265L v2                 | Discontinued    | Q2'12   | 4   | 3.50 GHz   | 2.50 GHz   | 8 MB SmartCache   |
| Intel(R) Xeon(R) Processor E3-1245 v2                  | Discontinued    | Q2'12   | 4   | 3.80 GHz   | 3.40 GHz   | 8 MB SmartCache   |
| Intel(R) Xeon(R) Processor E3-1240 v2                  | Discontinued    | Q2'12   | 4   | 3.80 GHz   | 3.40 GHz   | 8 MB SmartCache   |
| Intel(R) Xeon(R) Processor E5-1428L                    | Discontinued    | Q2'12   | 6   |            | 1.80 GHz   | 15 MB L3          |
| Intel(R) Xeon(R) Processor E5-2650                     | Discontinued    | Q1'12   | 8   | 2.80 GHz   | 2.00 GHz   | 20 MB SmartCache  |
| Intel(R) Xeon(R) Processor E5-2687W                    | Discontinued    | Q1'12   | 8   | 3.80 GHz   | 3.10 GHz   | 20 MB SmartCache  |
| Intel(R) Xeon(R) Processor E5-1660                     | Discontinued    | Q1'12   | 6   | 3.90 GHz   | 3.30 GHz   | 15 MB SmartCache  |
| Intel(R) Xeon(R) Processor E5-1650                     | Discontinued    | Q1'12   | 6   | 3.80 GHz   | 3.20 GHz   | 12 MB SmartCache  |
| Intel(R) Xeon(R) Processor E5-2640                     | Discontinued    | Q1'12   | 6   | 3.00 GHz   | 2.50 GHz   | 15 MB SmartCache  |
| Intel(R) Xeon(R) Processor E5-2680                     | Discontinued    | Q1'12   | 8   | 3.50 GHz   | 2.70 GHz   | 20 MB SmartCache  |
| Intel(R) Xeon(R) Processor E5-1620                     | Discontinued    | Q1'12   | 4   | 3.80 GHz   | 3.60 GHz   | 10 MB SmartCache  |
| Intel(R) Xeon(R) Processor E5-2603                     | Discontinued    | Q1'12   | 4   |            | 1.80 GHz   | 10 MB SmartCache  |
| Intel(R) Xeon(R) Processor E5-2660                     | Discontinued    | Q1'12   | 8   | 3.00 GHz   | 2.20 GHz   | 20 MB SmartCache  |
| Intel(R) Xeon(R) Processor E5-2630                     | Discontinued    | Q1'12   | 6   | 2.80 GHz   | 2.30 GHz   | 15 MB SmartCache  |
| Intel(R) Xeon(R) Processor E5-2650L                    | Discontinued    | Q1'12   | 8   | 2.30 GHz   | 1.80 GHz   | 20 MB SmartCache  |
| Intel(R) Xeon(R) Processor E5-2620                     | Launched        | Q1'12   | 6   | 2.50 GHz   | 2.00 GHz   | 15 MB SmartCache  |
| Intel(R) Xeon(R) Processor E5-2630L                    | Discontinued    | Q1'12   | 6   | 2.50 GHz   | 2.00 GHz   | 15 MB SmartCache  |
| Intel(R) Xeon(R) Processor E5-2670                     | Discontinued    | Q1'12   | 8   | 3.30 GHz   | 2.60 GHz   | 20 MB SmartCache  |
| Intel(R) Xeon(R) Processor E5-2643                     | Discontinued    | Q1'12   | 4   | 3.50 GHz   | 3.30 GHz   | 10 MB SmartCache  |
| Intel(R) Xeon(R) Processor E5-2690                     | Discontinued    | Q1'12   | 8   | 3.80 GHz   | 2.90 GHz   | 20 MB SmartCache  |
| Intel(R) Xeon(R) Processor E5-2609                     | Discontinued    | Q1'12   | 4   |            | 2.40 GHz   | 10 MB SmartCache  |
| Intel(R) Xeon(R) Processor E5-2665                     | Discontinued    | Q1'12   | 8   | 3.10 GHz   | 2.40 GHz   | 20 MB SmartCache  |
| Intel(R) Xeon(R) Processor E5-2667                     | Discontinued    | Q1'12   | 6   | 3.50 GHz   | 2.90 GHz   | 15 MB SmartCache  |
| Intel(R) Xeon(R) Processor E5-2637                     | Discontinued    | Q1'12   | 2   | 3.50 GHz   | 3.00 GHz   | 5 MB SmartCache   |
| Intel(R) Xeon(R) Processor E3-1290                     | Discontinued    | Q3'11   | 4   | 4.00 GHz   | 3.60 GHz   | 8 MB SmartCache   |
| Intel(R) Xeon(R) Processor E3-1230                     | Discontinued    | Q2'11   | 4   | 3.60 GHz   | 3.20 GHz   | 8 MB SmartCache   |
| Intel(R) Xeon(R) Processor E3-1235                     | Discontinued    | Q2'11   | 4   | 3.60 GHz   | 3.20 GHz   | 8 MB SmartCache   |
| Intel(R) Xeon(R) Processor E3-1240                     | Discontinued    | Q2'11   | 4   | 3.70 GHz   | 3.30 GHz   | 8 MB SmartCache   |
| Intel(R) Xeon(R) Processor E3-1245                     | Discontinued    | Q2'11   | 4   | 3.70 GHz   | 3.30 GHz   | 8 MB SmartCache   |
| Intel(R) Xeon(R) Processor E3-1260L                    | Discontinued    | Q2'11   | 4   | 3.30 GHz   | 2.40 GHz   | 8 MB SmartCache   |
| Intel(R) Xeon(R) Processor E3-1270                     | Discontinued    | Q2'11   | 4   | 3.80 GHz   | 3.40 GHz   | 8 MB SmartCache   |
| Intel(R) Xeon(R) Processor E3-1275                     | Discontinued    | Q2'11   | 4   | 3.80 GHz   | 3.40 GHz   | 8 MB SmartCache   |
| Intel(R) Xeon(R) Processor E3-1220                     | Discontinued    | Q2'11   | 4   | 3.40 GHz   | 3.10 GHz   | 8 MB SmartCache   |
| Intel(R) Xeon(R) Processor E3-1280                     | Discontinued    | Q2'11   | 4   | 3.90 GHz   | 3.50 GHz   | 8 MB SmartCache   |
| Intel(R) Xeon(R) Processor E3-1225                     | Discontinued    | Q2'11   | 4   | 3.40 GHz   | 3.10 GHz   | 6 MB SmartCache   |
| Intel(R) Xeon(R) Processor E3-1220L                    | Discontinued    | Q2'11   | 2   | 3.40 GHz   | 2.20 GHz   | 3 MB SmartCache   |